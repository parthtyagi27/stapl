#!/bin/bash

# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

run_command=$1

reg="[1,2,4]"
nonparallel="[1]"

if [[ $run_command =~ $reg ]]
then
  eval $run_command ./test_communication_skeletons 4 10
  if test $? != 0
  then
      echo "ERROR:: while testing test_communication_skeletons"
  fi
fi

if [[ $run_command =~ $nonparallel ]]
then
  eval $run_command ./test_consumer_count
  if test $? != 0
  then
      echo "ERROR:: while testing test_consumer_count"
  fi

  eval $run_command ./test_spans
  if test $? != 0
  then
      echo "ERROR:: while testing test_spans"
  fi

  eval $run_command ./test_domain_ports 4
  if test $? != 0
  then
    echo "ERROR:: while testing test_domain_ports"
  fi
fi

eval $run_command ./test_serial
if test $? != 0
then
  echo "ERROR:: while testing test_serial"
fi

eval $run_command ./test_skeletons 256
if test $? != 0
then
    echo "ERROR:: while testing test_skeletons"
fi

eval $run_command ./test_coarse_skeletons 2048
if test $? != 0
then
    echo "ERROR:: while testing test_coarse_skeletons"
fi

eval $run_command ./test_nested 10 10
if test $? != 0
then
    echo "ERROR:: while testing test_nested"
fi

eval $run_command ./test_notify_map 1000
if test $? != 0
then
    echo "ERROR:: while testing test_notify_map"
fi

eval $run_command ./test_inline 32
if test $? != 0
then
    echo "ERROR:: while testing test_inline"
fi

grep -i 'static_assert' test_inline_cycle_check > /dev/null 2>&1
if test $? != 0
then
    echo "ERROR:: test_inline_cycle_check.cc compiled without error"
else
    echo "Testing cycle detection in inline_flows : PASSED"
fi
