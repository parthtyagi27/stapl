/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test calling MPI functions through @ref stapl::external_call().
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include "../test_utils.h"

#ifndef STAPL_DONT_USE_MPI
# include <mpi.h>
#endif

int allreduce(int i)
{
#ifndef STAPL_DONT_USE_MPI
  MPI_Allreduce(MPI_IN_PLACE, &i, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#endif
  return i;
}

void barrier(void)
{
#ifndef STAPL_DONT_USE_MPI
  MPI_Barrier(MPI_COMM_WORLD);
#endif
}

void allreduce_p(int* i)
{
#ifndef STAPL_DONT_USE_MPI
  MPI_Allreduce(MPI_IN_PLACE, i, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#endif
}


stapl::exit_code stapl_main(int, char*[])
{
  using namespace stapl;
  std::set<unsigned int> c = external_callers();

  boost::optional<int> p = external_call(&allreduce, 1);
  if (c.find(get_location_id())!=c.end()) {
    STAPL_RUNTIME_TEST_REQUIRE(bool(p));
    STAPL_RUNTIME_TEST_CHECK(*p, get_num_processes());
  }
  else {
    STAPL_RUNTIME_TEST_REQUIRE(!bool(p));
  }

  bool f = stapl::external_call(&barrier);
  if (c.find(get_location_id())!=c.end()) {
    STAPL_RUNTIME_TEST_REQUIRE(f);
  }
  else {
    STAPL_RUNTIME_TEST_REQUIRE(!f);
  }

  int  i = 1;
  bool b = stapl::external_call(&allreduce_p, &i);
  if (c.find(get_location_id())!=c.end()) {
    STAPL_RUNTIME_TEST_REQUIRE(b);
    STAPL_RUNTIME_TEST_CHECK(i, get_num_processes());
  }
  else {
    STAPL_RUNTIME_TEST_REQUIRE(!b);
    STAPL_RUNTIME_TEST_CHECK(i, 1);
  }

#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
