/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test deleting location metadata through deleting its last
/// @ref stapl::p_object.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <iostream>
#include <cmath>
#include "test_utils.h"

using namespace stapl;

struct simple_p_object
: public p_object
{ };

struct map_wf
{
  typedef unsigned int result_type;

  unsigned int operator()(unsigned int n) const
  { return (n/2); }
};

struct reverse_map_wf
{
  typedef unsigned int result_type;

  unsigned int operator()(unsigned int n) const
  { return (2*n); }
};

// even locations only
rmi_handle::reference object_in_gang(void)
{
  const unsigned int num_locs = get_num_locations();
  gang g(std::ceil(num_locs/2.0), map_wf(), reverse_map_wf());
  simple_p_object* p = new simple_p_object;
  return p->get_rmi_handle();
}

class p_test
: public p_object
{
private:
  unsigned int right;

  void kill(rmi_handle::reference r)
  {
    p_object_delete<simple_p_object> d;
    d(r);
  }

public:
  p_test(void)
  {
    const unsigned int lid = this->get_location_id();
    const unsigned int nlocs = this->get_num_locations();
    right = ( lid == nlocs - 1 ) ? 0 : lid + 1;
    this->advance_epoch();
  }

  // Creates and kills an object in a gang
  void test_gang_object(void)
  {
    if (this->get_location_id()%2==0) {
      rmi_handle::reference ref = object_in_gang();
      if (this->get_location_id()==0) {
        kill(ref);
      }
    }

    rmi_fence(); // quiescence before next test
  }

  // Creates an object which is killed by a location outside the initial gang
  void test_gang_object_3rd_party(void)
  {
    if (this->get_location_id()%2==0) {
      rmi_handle::reference ref = object_in_gang();
      if (this->get_location_id()==0)
        async_rmi(right, this->get_rmi_handle(), &p_test::kill, ref);
    }
  }

  void execute(void)
  {
    test_gang_object();
    test_gang_object_3rd_party(); // it must be the last test to detect if gangs
                                  // are correctly garbage collected
  }
};


exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
