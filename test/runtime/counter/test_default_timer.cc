/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE default_timer
#include "utility.h"
#include <chrono>
#include <thread>
#include <stapl/runtime/counter/default_counters.hpp>

using namespace stapl;

BOOST_AUTO_TEST_CASE( test_default_timer )
{
  counter<default_timer> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  // test one time event
  c.start();
  std::this_thread::sleep_for(std::chrono::seconds{1});
  const counter<default_timer>::value_type time1 = c.stop();

  // test reset
  c.reset();
  c.start();
  std::this_thread::sleep_for(std::chrono::seconds{1});
  const counter<default_timer>::value_type time2 = c.stop();

  // test accumulation
  c.start();
  std::this_thread::sleep_for(std::chrono::seconds{1});
  const counter<default_timer>::value_type time3 = c.stop();

  std::cout << time1 << ' ' << time2 << ' ' << time3 << std::endl;
}

BOOST_AUTO_TEST_CASE( test_scoped_counter )
{
  counter<default_timer> c;
  std::cout << c.native_name() << ": " << c.read() << ' ';

  // test one time event
  {
    scoped_counter<counter<default_timer> > s(c);
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  const counter<default_timer>::value_type time1 = c.value();

  // test reset
  {
    c.reset();
    scoped_counter<counter<default_timer> > s(c);
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  const counter<default_timer>::value_type time2 = c.value();

  // test accumulation
  {
    scoped_counter<counter<default_timer> > s(c);
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  const counter<default_timer>::value_type time3 = c.value();

  std::cout << time1 << ' ' << time2 << ' ' << time3 << std::endl;
}


namespace stapl {

template<>
struct disable_group_counter<1>
: public std::true_type
{ };

} // namespace stapl

BOOST_AUTO_TEST_CASE( test_disabled )
{
  counter<default_timer, 1> c;

  {
    scoped_counter<counter<default_timer, 1> > s(c);
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  const counter<default_timer, 1>::value_type time = c.value();

  BOOST_CHECK_EQUAL(time, 0.0);
}
