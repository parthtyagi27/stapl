/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE test_probe
#include "utility.h"
#include <chrono>
#include <thread>
#include <stapl/runtime/counter/probe.hpp>
#include <stapl/runtime/counter/default_counters.hpp>

using namespace stapl;

BOOST_AUTO_TEST_CASE( test_probe_clear )
{
  {
    auto p1 = probe<counter<default_timer>>::get("first");
    auto p2 = probe<counter<default_timer>>::get("second");
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  probe<counter<default_timer>>::clear();
}


BOOST_AUTO_TEST_CASE( test_probe_1 )
{
  auto t = std::thread([]{
    auto p1 = probe<counter<default_timer>>::get("first");
    auto p2 = probe<counter<default_timer>>::get("second");
    std::this_thread::sleep_for(std::chrono::seconds{1});
  });

  auto p1 = probe<counter<default_timer>>::get("first");
  auto p2 = probe<counter<default_timer>>::get("second");
  std::this_thread::sleep_for(std::chrono::seconds{1});

  t.join();
}


BOOST_AUTO_TEST_CASE( test_probe_dump )
{
  {
    auto p1 = probe<counter<default_timer>>::get("first");
    auto p2 = probe<counter<default_timer>>::get("second");
    std::this_thread::sleep_for(std::chrono::seconds{1});
  }
  probe<counter<default_timer>>::dump(std::cout);
}
