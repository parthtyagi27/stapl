/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE timer
#include "utility.h"
#include <stapl/runtime/counter/default_counters.hpp>
#include <stapl/runtime/utility/timer.hpp>

using namespace stapl;

#ifdef STAPL_USE_PAPI
# include <stapl/runtime/utility/papi_cycle_clock.hpp>

using timer_type = runtime::timer<runtime::papi_cycle_clock>;

static int initialize(void)
{
  if (PAPI_is_initialized()!=PAPI_LOW_LEVEL_INITED &&
      PAPI_library_init(PAPI_VER_CURRENT)!=PAPI_VER_CURRENT)
    std::abort();
  return 0;
}

int init = initialize();

#else

using timer_type = runtime::timer<>;

#endif


BOOST_AUTO_TEST_CASE( test_timer_remaining )
{
  counter<default_timer> c;

  const unsigned long timeout = 200;
  timer_type t{std::chrono::milliseconds(timeout)};
  t.reset();

  c.start();
  while (t.remaining() > std::chrono::milliseconds::zero());
  c.stop();

  const double time = c.value() * 1000.0;
  const double lower_bound = timeout * 0.9;
  const double upper_bound = timeout * 1.1;
  BOOST_CHECK( (lower_bound<time) && (time<upper_bound) );
}


BOOST_AUTO_TEST_CASE( test_timer_expired )
{
  counter<default_timer> c;

  const unsigned long timeout = 200;
  timer_type t{std::chrono::milliseconds(timeout)};
  t.reset();

  c.start();
  auto tt = t.now();
  while (!t.expired(tt))
    tt = t.now();
  c.stop();

  const double time = c.value()*1000.0;
  const double lower_bound = timeout * 0.9;
  const double upper_bound = timeout * 1.1;
  BOOST_CHECK( (lower_bound<time) && (time<upper_bound) );
}
