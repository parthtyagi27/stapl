/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE option
#include "utility.h"
#include <stapl/runtime/utility/option.hpp>
#include <cstdlib>

using stapl::option;


void fake_main(int argc, char* argv[])
{
  option opts;
  BOOST_CHECK_EQUAL( opts.has_argv(), false );

  opts = opts & option(argc, argv);

  BOOST_CHECK_EQUAL( opts.has_argv(), true );
  BOOST_REQUIRE_EQUAL( opts.get_argc(), 2 );
  BOOST_CHECK_EQUAL( opts.get_argv()[0], argv[0] );
  BOOST_CHECK_EQUAL( opts.get_argv()[1], argv[1] );
}


BOOST_AUTO_TEST_CASE( test_option_main )
{
  int argc = 2;
  char a0[6] = "hello";
  char a1[6] = "world";
  char* argv[] = { a0, a1 };
  fake_main(argc, argv);
}


BOOST_AUTO_TEST_CASE( test_option_1 )
{
  option opts;

  int i = 0;
  bool r1 = opts.try_get("i", i);
  BOOST_CHECK( !r1 );
  BOOST_CHECK_EQUAL( i, 0);

  double d = 0.0;
  bool r2 = opts.try_get("d", d);
  BOOST_CHECK( !r2 );
  BOOST_CHECK_EQUAL( d, 0.0);

  opts = opts & option("i", 42) & option("d", 43.5);

  bool r3 = opts.try_get("i", i);
  BOOST_CHECK( r3 );
  BOOST_CHECK_EQUAL( i, 42);

  bool r4 = opts.try_get("d", d);
  BOOST_CHECK( r4 );
  BOOST_CHECK_EQUAL( d, 43.5);
}


BOOST_AUTO_TEST_CASE( test_option_2 )
{
  option opts;

  int r1 = opts.get("i", 10);
  BOOST_CHECK_EQUAL( r1, 10);

  opts = opts & option("i", 42);
  int r2 = opts.get("i", 10);
  BOOST_CHECK_EQUAL( r2, 42);

  std::string r3 = opts.get("PATH", std::string("empty"));
  BOOST_CHECK( r3!=std::string("empty") );

  std::string r4 = opts.get("WHYONEARTHWOULDYOUDEFINETHAT",
                            std::string("awesome"));
  BOOST_CHECK( r4==std::string("awesome") );
}
