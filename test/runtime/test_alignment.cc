/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test for correct alignment in STAPL-RTS internal structures.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <stapl/runtime/request/header.hpp>
#include <algorithm>
#include <cstddef>
#include <iostream>
#include "test_utils.h"

using namespace stapl::runtime;

const std::size_t alignment = STAPL_RUNTIME_DEFAULT_ALIGNMENT;


void check_alignment(message_ptr m)
{
  STAPL_RUNTIME_TEST_CHECK(m->header_size() % alignment, 0);
  STAPL_RUNTIME_TEST_CHECK(std::uintptr_t(m->data()) % alignment, 0);
  STAPL_RUNTIME_TEST_CHECK(std::uintptr_t(&(m->header())) % alignment, 0);
  auto p = m->payload();
  STAPL_RUNTIME_TEST_CHECK(std::uintptr_t(&(*p.begin())) % alignment, 0);
}


struct unaligned_header
{
  char c;
};

stapl::exit_code stapl_main(int, char*[])
{
  check_alignment(message_ptr{message::construct()});
  check_alignment(message::create(3));
  check_alignment(message::create(header::INVALID, 3));
  check_alignment(message::create(header::INVALID, 0, unaligned_header{}));


#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
