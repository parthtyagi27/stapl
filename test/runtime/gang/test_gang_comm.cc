/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


//////////////////////////////////////////////////////////////////////
/// @file
/// Test intragang communication.
//////////////////////////////////////////////////////////////////////

#include <stapl/runtime.hpp>
#include <stapl/runtime/utility/functional.hpp>
#include <iostream>
#include <numeric>
#include <cmath>
#include "../test_utils.h"

struct p_test
: public p_test_object
{
  // creates a new gang with all locations
  void test_one(void)
  {
    typedef stapl::runtime::identity<unsigned int, unsigned int> map_fun_type;

    const unsigned int num_locs = stapl::get_num_locations();

    // do async_rmi
    if (stapl::get_location_id()!=0) {
      delay(1);
    }
    stapl::gang g(num_locs, map_fun_type(), map_fun_type());
    const unsigned int num_locs_inner = stapl::get_num_locations();
    p_test_object o;
    if (stapl::get_location_id()==0) {
      // send to last location
      stapl::async_rmi(num_locs_inner-1, o.get_rmi_handle(),
                       &p_test_object::set, 1, 1);
      STAPL_RUNTIME_TEST_CHECK(1, stapl::sync_rmi(num_locs_inner-1,
                                                  o.get_rmi_handle(),
                                                  &p_test_object::get, 1));
    }
    stapl::rmi_fence(); // wait for async_rmi calls to finish
    STAPL_RUNTIME_TEST_CHECK(1, stapl::sync_rmi(num_locs_inner-1,
                                                o.get_rmi_handle(),
                                                &p_test_object::get, 1));
    stapl::rmi_fence(); // wait for sync_rmi calls to finish
  }

  // creates a new gang with all locations
  void test_all(void)
  {
    const unsigned int num_locs = stapl::get_num_locations();

    typedef stapl::runtime::identity<unsigned int, unsigned int> map_fun_type;

    if (stapl::get_location_id()!=0 && stapl::get_location_id()!=num_locs/2) {
      delay(1);
    }
    stapl::gang g(num_locs, map_fun_type(), map_fun_type());
    p_test_object o;
    if (stapl::get_location_id()==num_locs/2) {
      // send to all locations
      stapl::unordered::async_rmi(stapl::all_locations, o.get_rmi_handle(),
                                  &p_test_object::set, 1, 1);
    }
    stapl::rmi_fence(); // wait for async_rmi calls to finish
    STAPL_RUNTIME_TEST_CHECK(1, o.get(1));
  }

  void execute(void)
  {
    test_one();
    test_all();
  }
};


stapl::exit_code stapl_main(int, char*[])
{
  p_test pt;
  pt.execute();
#ifndef _TEST_QUIET
  std::cout << stapl::get_location_id() << " successfully passed!" << std::endl;
#endif
  return EXIT_SUCCESS;
}
