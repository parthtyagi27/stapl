/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#define STAPL_RUNTIME_TEST_MODULE type_id
#include "utility.h"
#include <stapl/runtime/type_traits/type_id.hpp>
#include <stapl/runtime/type_traits/polymorphic.hpp>

struct A
{ };

struct B
{ };

class Base
{
public:
  virtual ~Base(void)
  { }

  STAPL_POLYMORPHIC_TYPE()
};

class Derived1
: public Base
{
public:
  STAPL_POLYMORPHIC_TYPE()
};

class Derived2
: public Base
{
public:
  STAPL_POLYMORPHIC_TYPE()
};


using namespace stapl;

BOOST_AUTO_TEST_CASE( test_equal )
{
  A a, b;
  BOOST_CHECK_EQUAL(get_type_id<A>(), get_type_id(a));
  BOOST_CHECK_EQUAL(get_type_id(a),   get_type_id(b));
}

BOOST_AUTO_TEST_CASE( test_not_equal )
{
  A a;
  B b;
  BOOST_CHECK_NE(get_type_id<A>(), get_type_id<B>());
  BOOST_CHECK_NE(get_type_id(a),   get_type_id(b));
}

BOOST_AUTO_TEST_CASE( test_polymorphic )
{
  BOOST_CHECK_NE(get_type_id<Derived1>(), get_type_id<Base>());
  BOOST_CHECK_NE(get_type_id<Derived2>(), get_type_id<Base>());
  BOOST_CHECK_NE(get_type_id<Derived1>(), get_type_id<Derived2>());
}

BOOST_AUTO_TEST_CASE( test_polymorphic_equal )
{
  Derived1 a, b;
  BOOST_CHECK_NE(get_type_id<Base>(),        get_type_id(a));
  BOOST_CHECK_EQUAL(get_type_id<Derived1>(), get_type_id(a));
  BOOST_CHECK_EQUAL(get_type_id(a),          get_type_id(b));
}

BOOST_AUTO_TEST_CASE( test_polymorphic_not_equal )
{
  Derived1 a;
  Derived2 b;
  BOOST_CHECK_NE(get_type_id<Base>(),        get_type_id(a));
  BOOST_CHECK_NE(get_type_id<Base>(),        get_type_id(b));
  BOOST_CHECK_EQUAL(get_type_id<Derived1>(), get_type_id(a));
  BOOST_CHECK_EQUAL(get_type_id<Derived2>(), get_type_id(b));
  BOOST_CHECK_NE(get_type_id<Derived1>(),    get_type_id<Derived2>());
  BOOST_CHECK_NE(get_type_id(a),             get_type_id(b));
}
