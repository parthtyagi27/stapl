/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_P_CONTAINER_PROFILER_UTIL_HPP
#define STAPL_P_CONTAINER_PROFILER_UTIL_HPP

#include "p_container_profiler.hpp"
#include "value_type_util.h"

#include <stapl/runtime.hpp>

////////////////////////////////////////////////////////////////////////////////
/// @brief Simple print function that limits output to location 0
///
/// @param s A string of characters to print
////////////////////////////////////////////////////////////////////////////////
void stapl_print(const char* s)
{
  if (stapl::get_location_id() == 0)
    std::cout << s << std::flush;
}

// simple timer
typedef stapl::counter<stapl::default_timer> counter_type;

typedef my_variable<int,1,10> MVT;

////////////////////////////////////////////////////////////////////////////////
/// @brief Returns the sum of two MVT objects.
///
/// @param v1 The first operand
/// @param v2 The second operand
////////////////////////////////////////////////////////////////////////////////
MVT operator+(MVT const& v1, MVT const& v2)
{
  return v1 + v2;
}

////////////////////////////////////////////////////////////////////////////////
/// @brief Takes an operand and increases it by 1234.
///
/// @tparam T An arbitrary type
////////////////////////////////////////////////////////////////////////////////
template <class T>
class sum_op
{
public:
  template<typename Reference>
  void operator()(Reference elem) const
  {
    elem += 1234;
  }
};

////////////////////////////////////////////////////////////////////////////////
/// @brief Returns the sum of an operand and the value 1234 without modifying
/// the operand.
///
/// @tparam T An arbitrary type
////////////////////////////////////////////////////////////////////////////////
template <class T>
class get_sum
{
public:
  typedef T result_type;

  template<typename Reference>
  T operator()(Reference elem) const
  {
    return elem + 1234;
  }
};

#endif
