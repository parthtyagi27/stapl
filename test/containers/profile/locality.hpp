/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PROFILING_LOCALITY_HPP
#define STAPL_PROFILING_LOCALITY_HPP

#include <algorithm>
#include "adt.hpp"

namespace stapl {

namespace profiling {

////////////////////////////////////////////////////////////////////////////////
/// @brief Profiler for the locality() method.
///
/// @tparam ADT The container/view type
/// @tparam Counter Counter for the profile metric
///
/// @ingroup profiling
////////////////////////////////////////////////////////////////////////////////
template<typename ADT, typename Counter = counter<default_timer>>
class locality_profiler
  : public adt_profiler<ADT, Counter>
{
  using base_type = adt_profiler<ADT, Counter>;
  using value_type = typename ADT::value_type;
  using index_type = typename view_traits<ADT>::index_type;

  /// The indices to be testd for locality
  std::vector<index_type> const& indices;

  /// Expected number of remote indices
  size_t m_expected_remote;

  /// Vector of locality information obtained for #indices.
  std::vector<locality_info> m_result;

public:
  locality_profiler(std::string name, ADT* adt,
                    std::vector<index_type> const& idx,
                    size_t expected_remote,
                    int argc = 0, char** argv = nullptr)
    : base_type(adt, name+"::locality", idx.size(), argc, argv),
      indices(idx), m_expected_remote(expected_remote), m_result(idx.size())
  { }

  void check_validity()
  {
    size_t nremote = std::count_if(m_result.begin(), m_result.end(),
      [this](locality_info const& loc) {
        return loc.location() != this->get_location_id();
      });

    this->m_passed = nremote == m_expected_remote;
  }

  void run()
  {
    for (size_t i=0; i<this->m_test_size; ++i)
      m_result[i] = this->m_adt->locality(indices[i]);
  }
};

template<typename ADT>
void add_locality_profilers(prof_cont_t<ADT>& p,
                            std::string const& name, ADT& adt,
                            std::vector<size_t> const& indices,
                            size_t expected_remote,
                            int argc, char** argv)
{
  static_assert(is_container<ADT>::value || is_view<ADT>::value,
    "A STAPL container or view must be supplied to add_locality_profilers.");

  p.push_back(new locality_profiler<ADT>(name, &adt, indices, expected_remote,
                                         argc, argv));
}

} // namespace stapl

} // namespace profiling

#endif // STAPL_PROFILING_LOCALITY_HPP
