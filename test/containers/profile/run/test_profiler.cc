/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include "../p_container_profiler.hpp"
#include <stapl/containers/array/array.hpp>

using namespace stapl;

#include "../profiler_util.h"

stapl::exit_code stapl_main(int argc, char* argv[])
{
  size_t N;
  if (get_location_id() == 0)
    std::cout<<"pContainer Performance Evaluation\n";
  if (argc > 2) {
    N = atoi(argv[1]);
    srand(atoi(argv[2])+get_location_id());
  } else {
    std::cout<<"Input size N required; Using 10 by default\n";
    N=10;
  }

  constructor_size_profiler<array<int>, counter_type> cep("p_array", NULL, N,
                                                          argc, argv);
  for (int i=1; i<argc; i++) {
    if ( !strcmp("--help", argv[i]) )
      cep.print_help_options();
  }
  cep.collect_profile();
  cep.report();

  array<int> p(N);

  std::vector<size_t> indices(p.size()/get_num_locations());
  for (size_t i=0; i<indices.size(); ++i) {
    indices[i] = lrand48() % N;
  }

  set_element_profiler<array<int>, counter_type> sep("p_array", &p, indices,
                                                     argc, argv);
  sep.collect_profile();
  sep.report();

  get_element_profiler<array<int>, counter_type > gep("p_array", &p, indices,
                                                      argc, argv);
  gep.collect_profile();
  gep.report();

  operator_square_bracket_lhs_profiler<array<int>,
    counter_type > oplep("p_array", &p, indices, argc, argv);
  oplep.collect_profile();
  oplep.report();

  operator_square_bracket_rhs_profiler<array<int>,
    counter_type > oprep("p_array", &p, indices, argc, argv);
  oprep.collect_profile();
  oprep.report();

  return EXIT_SUCCESS;
}
