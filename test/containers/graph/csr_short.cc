/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/containers/graph/csr_graph.hpp>
#include <stapl/containers/graph/short_csr_graph.hpp>

#include "test_util.h"

using namespace stapl;

void test_normal_csr()
{
  using graph_t = csr_graph<DIRECTED, MULTIEDGES>;
  using edge_type = graph_t::value_type::adj_edges_type::value_type;

  constexpr std::size_t edge_size = sizeof(edge_type);

  static_assert(edge_size == 24, "Normal edge is 24 bytes");
}

template<typename CSRGraph, int Size>
void test_short_csr()
{
  using vertex_type = typename CSRGraph::value_type;
  using descriptor_type = typename CSRGraph::vertex_descriptor;
  using edge_type = typename vertex_type::adj_edges_type::value_type;

  constexpr std::size_t edge_size = sizeof(edge_type);
  constexpr std::size_t descriptor_size = sizeof(descriptor_type);

  static_assert(edge_size == Size, "Short edge is 4/8 bytes");
  static_assert(descriptor_size == Size, "Descriptor is 4/8 bytes");
}

stapl::exit_code stapl_main(int argc, char* argv[])
{
  test_normal_csr();
  test_short_csr<short_csr_graph<DIRECTED>, 8>();
  test_short_csr<small_short_csr_graph<DIRECTED>, 4>();

  return EXIT_SUCCESS;
}
