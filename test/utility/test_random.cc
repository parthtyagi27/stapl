/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/utility/random.hpp>

#include "../test_report.hpp"

class random_object
 : public stapl::p_object
{
  std::mt19937 m_std_gen;
  boost::random::mt19937 m_boost_gen;

public:
  random_object(void)
    : m_std_gen{this->get_location_id()}, m_boost_gen{this->get_location_id()}
  { }

  void send_twisters()
  {
    const auto loc = this->get_location_id();

    for (std::size_t l = 0; l < this->get_num_locations(); ++l)
    {
      stapl::async_rmi(
        l, this->get_rmi_handle(),
        &random_object::receive_twister<std::mt19937>, m_std_gen, loc
      );

      stapl::async_rmi(
        l, this->get_rmi_handle(),
        &random_object::receive_twister<boost::random::mt19937>, m_boost_gen,
        loc
      );
    }
  }

  template<typename Twister>
  void receive_twister(Twister gen, stapl::location_type source)
  {
    using fn_t = void (random_object::*)(Twister);
    fn_t fn = &random_object::compare_twister;

    stapl::async_rmi(source, this->get_rmi_handle(), fn, gen);
  }

  void compare_twister(std::mt19937 gen)
  {
    if (gen != m_std_gen)
      stapl::abort("std generator has differing internal state");
  }

  void compare_twister(boost::random::mt19937 gen)
  {
    if (gen != m_boost_gen)
      stapl::abort("Boost generator has differing internal state");
  }
};

stapl::exit_code stapl_main(int argc, char* argv[])
{
  random_object r;
  r.send_twisters();

  STAPL_TEST_REPORT(true, "Generators serialized");

  return EXIT_SUCCESS;
}
