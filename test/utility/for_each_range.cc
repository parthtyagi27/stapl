/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/runtime.hpp>
#include <stapl/utility/for_each_range.hpp>

#include <vector>

#include "../test_report.hpp"

stapl::exit_code stapl_main(int, char*[])
{
  using vector_type = std::vector<int>;
  using iterator_type = vector_type::iterator;

  std::vector<int> vec{{-20,-2,4,5,6,-5,6,-7}};
  std::vector<int> out_vec;

  // Function to sum the range and add it to out_vec
  auto func = [&](iterator_type begin, iterator_type end) mutable {
    int sum = std::accumulate(begin, end, 0);
    out_vec.push_back(sum);
  };

  // Either both are negative or both are positive
  auto comp = [](int x, int y) {
    return (x < 0 && y < 0) || (x > 0 && y > 0);
  };

  stapl::utility::for_each_range(vec.begin(), vec.end(), func, comp);

  std::vector<int> expected{{-22, 15, -5, 6, -7}};

  bool passed = std::equal(out_vec.begin(), out_vec.end(), expected.begin());

  STAPL_TEST_REPORT(passed, "for_each_range sum of runs of pos/neg ints");

  ///
  /// Test for for_each_filtered_range

  auto pred = [](int x) {
    return x % 2 == 0;
  };

  out_vec.clear();
  stapl::utility::for_each_filtered_range(
    vec.begin(), vec.end(), func, comp, pred
  );

  expected = {-22, 4, 6, 6};

  passed = std::equal(out_vec.begin(), out_vec.end(), expected.begin());

  STAPL_TEST_REPORT(passed,
    "for_each_filtered_range sum of runs of even pos/neg ints");

  return EXIT_SUCCESS;
}
