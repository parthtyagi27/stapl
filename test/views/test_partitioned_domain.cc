/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>

#include <stapl/domains/indexed.hpp>
#include <stapl/domains/partitioned_domain.hpp>

#include "../test_report.hpp"

using namespace stapl;

stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef indexed_domain<int> domain_type;

  domain_type dom(0, 99);

  partitioned_domain<domain_type> pdom(dom);

  printf("%d: lower = %d / upper = %d\n",
         stapl::get_location_id(),
         pdom.local_subdomains()[0].first(),
         pdom.local_subdomains()[0].last());

///  size_t res = accumulate(stapl::domain_view(view1),0);
///
///  size_t m = n*(n-1)/2;
///  STAPL_TEST_REPORT(res == m,"Testing domain_view [0..99]");
///
///
///  view_t view4(pa1,vec_dom_t(10,n-11));
///
///  res = accumulate(stapl::domain_view(view4),0);
///
///  m = ((n-10)*(n-11)/2)-(45);
///  STAPL_TEST_REPORT(res == m,"Testing domain_view [10..89]");
///
///
///  copy(counting_view<size_t>(view4.size()),view4);
///
///  size_t idx = my_find(view1,7);
///  STAPL_TEST_REPORT(idx == 17,"Testing domain_view (find)");
///
  return EXIT_SUCCESS;
}
