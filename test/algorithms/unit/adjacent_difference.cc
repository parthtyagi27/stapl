/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <cstdlib>

#include <stapl/runtime.hpp>
#include <stapl/utility/do_once.hpp>
#include <stapl/containers/array/array.hpp>
#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/numeric.hpp>

stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef stapl::array<unsigned long>          p_array_type;
  typedef stapl::array_view<p_array_type>      parrayView;

  int nelem = 1000;

  if (argc > 1)
    nelem = atoi(argv[1]);

  p_array_type parr(nelem);
  p_array_type parrRes(nelem);
  parrayView parrv(parr);
  parrayView parrResv(parrRes);

  stapl::generate(parrv, stapl::sequence<unsigned long>(1,0));

  // Default adjacent difference

  stapl::adjacent_difference(parrv,parrResv);

  int result = stapl::count(parrResv, size_t(0));
  stapl::do_once([=](void) {
    std::cout << argv[0];
    if (result == nelem-1)
      std::cout << " test1: PASSED\n";
    else
      std::cout << " test1: FAILED\n";
  });


  // adjacent difference with plus operator

  stapl::adjacent_difference(parrv, parrResv,
    stapl::plus<parrayView::value_type>());

  result = stapl::count(parrResv, size_t(2));

  stapl::do_once([=](void) {
    std::cout << argv[0];
    if (result == nelem-1)
      std::cout << " test2: PASSED\n";
    else
      std::cout << " test2: FAILED\n";
  });

  // adjacent difference with multilies operator

  stapl::adjacent_difference(parrv, parrResv,
    std::multiplies<parrayView::value_type>());

  result = stapl::count(parrResv, size_t(1));

  stapl::do_once([=](void) {
    std::cout << argv[0];
    if (result == nelem)
      std::cout << " test3: PASSED\n";
    else
      std::cout << " test3: FAILED\n";
  });

  return EXIT_SUCCESS;
}
