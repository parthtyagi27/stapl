/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <algorithm>
#include <functional>
#include <iterator>

#include <stapl/algorithms/algorithm.hpp>
#include <stapl/algorithms/generator.hpp>

#include "test.h"

#include <boost/function.hpp>
#include <boost/bind.hpp>

using boost::bind;
using boost::function;
using stapl::sequence;
using stapl::random_sequence;


//////////////////////////////////////////////////////////////////////
/// @brief Test function for the is_sorted algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_is_sorted(const unsigned int n, const unsigned int i,
                    traits_type const& t, char const* name,
                    std::string const& ds = "none")
{
  typedef bool                             ret_type;
  typedef test_one_view<ret_type>          test_type;
  typedef stapl::less<data_type>           comp;

  typedef ret_type (*pfun1_t)(view_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, comp);
  typedef ret_type (*sfun1_t)(iterator_type, iterator_type);
  typedef ret_type (*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>              pfp_t;
  typedef function<ret_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::is_sorted<view_type>, _1));
  pfp_t pfp2(bind((pfun2_t) stapl::is_sorted<view_type, comp>, _1, comp()));

  sfp_t sfp1(bind((sfun1_t) std::is_sorted<iterator_type>, _1, _2));
  sfp_t sfp2(bind((sfun2_t)
    std::is_sorted<iterator_type, comp>, _1, _2, comp()));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres1, i, pfp1, sfp1);

  std::stringstream pred_name;
  pred_name << name << "-comp";
  timed_test_result tres2(pred_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres2, i, pfp2, sfp2);

  std::stringstream short_name;
  short_name << name << "-short";
  timed_test_result tres3(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0],
       sequence<data_type>(n_elems, -1)))
    (tres3, i, pfp1, sfp1);

  short_name << "_comp";
  timed_test_result tres4(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0],
       sequence<data_type>(n_elems, -1)))
    (tres4, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
  test_report(tres3);
  test_report(tres4);
}


//////////////////////////////////////////////////////////////////////
/// @brief Test function for the is_sorted_until algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_is_sorted_until(const unsigned int n, const unsigned int i,
                          traits_type const& t, char const* name,
                          std::string const& ds = "none")
{
  typedef view_type                  ret_type;
  typedef test_one_view<ret_type>    test_type;

  typedef stapl::less<data_type> comp;
  typedef ret_type(*pfun1_t)(view_type const&);
  typedef ret_type(*pfun2_t)(view_type const&, comp const&);

  typedef iterator_type(*sfun1_t)(iterator_type, iterator_type);
  typedef iterator_type(*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>                          pfp_t1;
  typedef function<iterator_type(iterator_type, iterator_type) >         sfp_t1;

  pfp_t1 pfp1(bind((pfun1_t)
    stapl::is_sorted_until<view_type>, _1));
  sfp_t1 sfp1(bind((sfun1_t)
    std::is_sorted_until<iterator_type>, _1, _2));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  std::stringstream short_name;
  short_name << name << "-short";
  timed_test_result tres1(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres1, i, pfp1, sfp1);

  timed_test_result tres2(name);
  test_type(data_descriptor<pcontainer_type, view_type,sequence<data_type> >
    (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
     (tres2, i, pfp1, sfp1);

  pfp_t1 pfp2(bind((pfun2_t)
    stapl::is_sorted_until<view_type, comp>, _1, comp()));
  sfp_t1 sfp2(bind((sfun2_t)
    std::is_sorted_until<iterator_type, comp>, _1, _2, comp()));

  short_name << "_comp";
  timed_test_result tres3(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres3, i, pfp2, sfp2);

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres4(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
    (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
     (tres4, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
  test_report(tres3);
  test_report(tres4);
}


template<typename T>
void test_lower_bound(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef view_type::reference       ret_type;
  typedef test_one_view<ret_type>   test_type;
  typedef stapl::less<data_type>      comp;

  typedef ret_type (*pfun1_t)(view_type const&, data_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, data_type const&, comp);
  typedef iterator_type
    (*sfun1_t)(iterator_type, iterator_type, data_type const&);
  typedef iterator_type
    (*sfun2_t)(iterator_type, iterator_type, data_type const&, comp);

  typedef function<ret_type (view_type const&)>                   pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::lower_bound<view_type, data_type>, _1, -11));
  sfp_t sfp1(bind((sfun1_t) std::lower_bound<iterator_type, data_type>, _1, _2, -11));

  const unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres1, i, pfp1, sfp1);


  pfp_t pfp2(bind((pfun1_t) stapl::lower_bound<view_type, data_type>, _1, 5));
  sfp_t sfp2(bind((sfun1_t) std::lower_bound<iterator_type, data_type>, _1, _2, 5));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres2, i, pfp2, sfp2);


  pfp_t pfp3(bind((pfun1_t) stapl::lower_bound<view_type, data_type>, _1, n/2));
  sfp_t sfp3(bind((sfun1_t) std::lower_bound<iterator_type, data_type>, _1, _2, n/2));

  std::stringstream short_name;
  short_name << name << "-short";
  timed_test_result tres3(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres3, i, pfp3, sfp3);

  pfp_t pfp4(bind((pfun1_t) stapl::lower_bound<view_type, data_type>, _1, n));
  sfp_t sfp4(bind((sfun1_t) std::lower_bound<iterator_type, data_type>, _1, _2, n));

  short_name << "_comp";
  timed_test_result tres4(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres4, i, pfp4, sfp4);

  test_report(tres1);
  test_report(tres2);
  test_report(tres3);
  test_report(tres4);
}


template<typename T>
void test_upper_bound(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef view_type::reference       ret_type;
  typedef test_one_view<ret_type>   test_type;
  typedef stapl::less<data_type>      comp;

  typedef ret_type (*pfun1_t)(view_type const&, data_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, data_type const&, comp);
  typedef iterator_type
    (*sfun1_t)(iterator_type, iterator_type, data_type const&);
  typedef iterator_type
    (*sfun2_t)(iterator_type, iterator_type, data_type const&, comp);

  typedef function<ret_type (view_type const&)>                         pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::upper_bound<view_type, data_type>, _1, -11));
  sfp_t sfp1(bind((sfun1_t) std::upper_bound<iterator_type, data_type>, _1, _2, -11));

  const unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres1, i, pfp1, sfp1);


  pfp_t pfp2(bind((pfun1_t) stapl::upper_bound<view_type, data_type>, _1, 5));
  sfp_t sfp2(bind((sfun1_t) std::upper_bound<iterator_type, data_type>, _1, _2, 5));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres2, i, pfp2, sfp2);


  pfp_t pfp3(bind((pfun1_t) stapl::upper_bound<view_type, data_type>, _1, n/2));
  sfp_t sfp3(bind((sfun1_t) std::upper_bound<iterator_type, data_type>, _1, _2, n/2));

  std::stringstream short_name;
  short_name << name << "-short";
  timed_test_result tres3(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres3, i, pfp3, sfp3);

  pfp_t pfp4(bind((pfun1_t) stapl::upper_bound<view_type, data_type>, _1, n));
  sfp_t sfp4(bind((sfun1_t) std::upper_bound<iterator_type, data_type>, _1, _2, n));

  short_name << "_comp";
  timed_test_result tres4(short_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1,1)))
    (tres4, i, pfp4, sfp4);

  test_report(tres1);
  test_report(tres2);
  test_report(tres3);
  test_report(tres4);
}


template<typename T>
void test_binary_search(const unsigned int n, const unsigned int i,
                        traits_type const& t, char const* name,
                        std::string const& ds = "none")
{
  using std::binary_search;
  using stapl::binary_search;

  typedef bool                       ret_type;
  typedef test_one_view<ret_type>    test_type;
  typedef stapl::greater<data_type>    comp;

  typedef ret_type (*pfun1_t)(view_type const&, data_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, data_type, comp);
  typedef ret_type (*sfun1_t)(iterator_type, iterator_type, data_type const&);
  typedef ret_type
    (*sfun2_t)(iterator_type, iterator_type, data_type const&, comp);

  typedef function<ret_type (view_type const&)>              pfp_t;
  typedef function<ret_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t)stapl::binary_search<view_type>, _1, 5));
  sfp_t sfp1(bind((sfun1_t)std::binary_search<iterator_type, data_type>, _1, _2, 5));

  const unsigned long int n_elems = set_data_size(n, ds, "sorting");

  std::stringstream found1_name;
  found1_name << name << "-found_case1";
  timed_test_result tres1(found1_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres1, i, pfp1, sfp1);

  pfp_t pfp2(bind((pfun2_t)
    stapl::binary_search<view_type, comp>, _1, 5, comp()));
  sfp_t sfp2(bind((sfun2_t)
    std::binary_search<iterator_type, data_type, comp>, _1, _2, 5, comp()));

  found1_name << "_comp";
  timed_test_result tres2(found1_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres2, i, pfp2, sfp2);

  pfp_t pfp3(bind((pfun1_t) stapl::binary_search<view_type>, _1, 11));
  sfp_t sfp3(bind((sfun1_t)
    std::binary_search<iterator_type, data_type>, _1, _2, 11));

  std::stringstream found2_name;
  found2_name << name << "-found_case2";
  timed_test_result tres3(found2_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres3, i, pfp3, sfp3);

  pfp_t pfp4(bind((pfun2_t)
    stapl::binary_search<view_type, comp>, _1, 11, comp()));
  sfp_t sfp4(bind((sfun2_t)
    std::binary_search<iterator_type, data_type, comp>, _1, _2, 11, comp()));

  found2_name << "_comp";
  timed_test_result tres4(found2_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres4, i, pfp4, sfp4);

  //search for non existing element
  pfp_t pfp5(bind((pfun1_t) stapl::binary_search<view_type>, _1, n + 1));
  sfp_t sfp5(bind((sfun1_t)
    std::binary_search<iterator_type, data_type>, _1, _2, n + 1));

  timed_test_result tres5(name);
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres5, i, pfp5, sfp5);

  pfp_t pfp6(bind((pfun2_t)
    stapl::binary_search<view_type, comp>, _1, n + 1, comp()));
  sfp_t sfp6(bind((sfun2_t)
    std::binary_search<iterator_type, data_type, comp>, _1, _2, n + 1, comp()));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres6(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, sequence<data_type> >
      (n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)))
    (tres6, i, pfp6, sfp6);

  test_report(tres1);
  test_report(tres2);
  test_report(tres3);
  test_report(tres4);
  test_report(tres5);
  test_report(tres6);
}


//////////////////////////////////////////////////////////////////////
/// @brief Test function for the min_element algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_min_element(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef view_type::reference       ret_type;
  typedef test_one_view<ret_type>    test_type;

  typedef stapl::less<data_type> comp;
  typedef ret_type(*pfun1_t)(view_type const&);
  typedef ret_type(*pfun2_t)(view_type const&, comp);
  typedef iterator_type(*sfun1_t)(iterator_type, iterator_type);
  typedef iterator_type(*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>                   pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::min_element<view_type>, _1));
   sfp_t sfp1(bind((sfun1_t) std::min_element<iterator_type>, _1, _2));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
   test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres1, i, pfp1, sfp1);

 pfp_t pfp2(bind((pfun2_t) stapl::min_element<view_type, comp>, _1, comp()));
   sfp_t sfp2(bind((sfun2_t) std::min_element<iterator_type, comp>, _1, _2,
              comp()));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
   test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres2, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
}


//////////////////////////////////////////////////////////////////////
/// @brief Test function for the min_value algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_min_value(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef data_type       ret_type;
  typedef test_one_view<ret_type, 1>    test_type;

  typedef stapl::less<data_type> comp;
  typedef ret_type(*pfun1_t)(view_type const&);
  typedef ret_type(*pfun2_t)(view_type const&, comp);
  typedef iterator_type(*sfun1_t)(iterator_type, iterator_type);
  typedef iterator_type(*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>                   pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::min_value<view_type>, _1));
   sfp_t sfp1(bind((sfun1_t) std::min_element<iterator_type>, _1, _2));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
   test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres1, i, pfp1, sfp1);

 pfp_t pfp2(bind((pfun2_t) stapl::min_value<view_type, comp>, _1, comp()));
   sfp_t sfp2(bind((sfun2_t) std::min_element<iterator_type, comp>, _1, _2,
              comp()));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
   test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
     (tres2, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
}

//////////////////////////////////////////////////////////////////////
/// @brief Test function for the max_value algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_max_value(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef data_type       ret_type;
  typedef test_one_view<ret_type, true>    test_type;

  typedef stapl::less<data_type> comp;
  typedef ret_type (*pfun1_t)(view_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, comp);
  typedef iterator_type (*sfun1_t)(iterator_type, iterator_type);
  typedef iterator_type (*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>                   pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::max_value<view_type>, _1));
  sfp_t sfp1(bind((sfun1_t) std::max_element<iterator_type>, _1, _2));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
    (tres1, i, pfp1, sfp1);

  pfp_t pfp2(bind((pfun2_t) stapl::max_value<view_type, comp>, _1, comp()));
  sfp_t sfp2(bind((sfun2_t) std::max_element<iterator_type, comp>, _1, _2,
             comp()));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
    (tres2, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
}

//////////////////////////////////////////////////////////////////////
/// @brief Test function for the max_element algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_max_element(const unsigned int n, const unsigned int i,
                      traits_type const& t, char const* name,
                      std::string const& ds = "none")
{
  typedef view_type::reference       ret_type;
  typedef test_one_view<ret_type>    test_type;

  typedef stapl::less<data_type> comp;
  typedef ret_type (*pfun1_t)(view_type const&);
  typedef ret_type (*pfun2_t)(view_type const&, comp);
  typedef iterator_type (*sfun1_t)(iterator_type, iterator_type);
  typedef iterator_type (*sfun2_t)(iterator_type, iterator_type, comp);

  typedef function<ret_type (view_type const&)>                   pfp_t;
  typedef function<iterator_type (iterator_type, iterator_type)>  sfp_t;

  pfp_t pfp1(bind((pfun1_t) stapl::max_element<view_type>, _1));
  sfp_t sfp1(bind((sfun1_t) std::max_element<iterator_type>, _1, _2));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

  timed_test_result tres1(name);
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
    (tres1, i, pfp1, sfp1);

  pfp_t pfp2(bind((pfun2_t) stapl::max_element<view_type, comp>, _1, comp()));
  sfp_t sfp2(bind((sfun2_t) std::max_element<iterator_type, comp>, _1, _2,
             comp()));

  std::stringstream comp_name;
  comp_name << name << "-comp";
  timed_test_result tres2(comp_name.str().c_str());
  test_type(data_descriptor<pcontainer_type, view_type, random_sequence>
    (n_elems, t.ct_create[0], t.vw_create[0], random_sequence(n_elems, true)))
    (tres2, i, pfp2, sfp2);

  test_report(tres1);
  test_report(tres2);
}


//////////////////////////////////////////////////////////////////////
/// @brief Test function for the lexicographical_compare algorithm.
/// @param n The number of elements to generate.
/// @param i The number of times the test will be run.
/// @param t Traits class containing the types of the containers and views to be
///   used and the function objects to instantiate them.
//////////////////////////////////////////////////////////////////////
template<typename T>
void test_lexicographical_compare(const unsigned int n, const unsigned int i,
                                  traits_type const& t, char const* name,
                                  std::string const& ds = "none")
{
  typedef bool                    ret_type;
  typedef test_two_view<ret_type> test_type;

  typedef ret_type (*pfun_t)(view_type const&, view_type const&);
  function<ret_type (view_type const&, view_type const&)>
     pfp(bind((pfun_t)stapl::lexicographical_compare<view_type, view_type>,
              _1, _2));

  typedef ret_type (*sfun_t)(iterator_type, iterator_type, iterator_type,
                             iterator_type);
  function<ret_type (iterator_type, iterator_type, iterator_type,
                     iterator_type)>
     sfp(bind((sfun_t) std::lexicographical_compare<iterator_type,
                                                    iterator_type>,
                _1, _2, _3, _4 ));

  unsigned long int n_elems = set_data_size(n, ds, "sorting");

   /*timed_test_result tres(name);
   test_type(
     data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
       n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
     data_descriptor<pcontainer_type, view_type, null_sequence<data_type> >(
       n_elems, t.ct_create[1], t.vw_create[1], null_sequence<data_type>()))
     (tres, i, pfp, sfp);*/

  timed_test_result tres1(name);
  test_type(
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[0], t.vw_create[0], sequence<data_type>(1, 1)),
    data_descriptor<pcontainer_type, view_type, sequence<data_type> >(
      n_elems, t.ct_create[1], t.vw_create[1], sequence<data_type>(1, 1)))
    (tres1, i, pfp, sfp);


   test_report(tres1);
}


stapl::exit_code stapl_main(int argc, char* argv[])
{
  typedef void (*test_func_type)(const unsigned int, const unsigned int,
                                 traits_type const&, char const*,
                                 std::string const&);

  test_pair<test_func_type> tests[] = {
    {"is_sorted",               test_is_sorted<int>},
    {"is_sorted_until",         test_is_sorted_until<int>},
    {"lower_bound",             test_lower_bound<int>},
    {"upper_bound",             test_upper_bound<int>},
    {"binary_search",           test_binary_search<int>},
    {"min_element",             test_min_element<int>},
    {"min_value",               test_min_value<int>},
    {"max_element",             test_max_element<int>},
    {"max_value",               test_max_value<int>},
    {"lexicographical_compare", test_lexicographical_compare<int>}//,
  };

  test_execute(argc, argv, tests,
               tests+(sizeof (tests)/sizeof (test_pair<test_func_type>)));

  return EXIT_SUCCESS;
}
