/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef TEST_REPORT_H
#define TEST_REPORT_H

#include <cstdlib>
#include <iostream>
#include <iterator>
#include <string>

#include <stapl/utility/do_once.hpp>

//
// Sends the supplied object to std::cerr
//
template<typename T>
class test_report_func1
{
private:
  T const& m_o;

public:
  typedef void result_type;

  test_report_func1(T const& o)
    : m_o(o)
  { }

  void operator()(void) const
  {
    std::cerr << m_o;
  }
};


template<typename T>
void test_report(T const& o)
{
  stapl::do_once(test_report_func1<T>(o));
}


//
// Prints the given view to std::cout
//
template<typename View>
class test_print_view_func
{
private:
  const char*  m_s;
  View const&  m_view;

public:
  typedef void result_type;

  test_print_view_func(const char *s, View const& view)
    : m_s(s), m_view(view)
  { }

  void operator()(void) const
  {
    std::cout << m_s << "\n";

    std::ostream_iterator<int> os(std::cout, " ");

    std::copy(m_view.begin(), m_view.end(), os);

    std::cout << "\n";
  }
};


template<typename View>
void test_print_view(const char *s, View const& v)
{
  stapl::do_once(test_print_view_func<View>(s,v));
}


//
// Prints an error message and exits
//
class test_error_func1
{
private:
  const char* m_s;

public:
  typedef void result_type;

  test_error_func1(const char* s)
    : m_s(s)
  { }

  void operator()(void) const
  {
    std::cerr << m_s << "\n";
  }
};


void test_error(const char *s)
{
  stapl::do_once(test_error_func1(s));

  std::exit(EXIT_FAILURE);
}


//
//  Prints an error message and exits.
//
class test_error_func2
{
private:
  const char* m_s1;
  const char* m_s2;

public:
  typedef void result_type;

  test_error_func2(const char* s1, const char* s2)
    : m_s1(s1), m_s2(s2)
  { }

  void operator()(void) const
  {
    std::cerr << m_s1 << " " << m_s2 << "\n";
  }
};


void test_error(const char *s1, const char *s2)
{
  stapl::do_once(test_error_func2(s1, s2));

  std::exit(EXIT_FAILURE);
}


//
// Prints an error message and exits.
//
class test_error_func3
{
private:
   std::string const& m_s;

public:
  typedef void result_type;

  test_error_func3(std::string const& s)
    : m_s(s)
  { }

  void operator()(void) const
  {
    std::cerr << m_s << "\n";
  }
};


void test_error(std::string const& s)
{
  stapl::do_once(test_error_func3(s));

  std::exit(EXIT_FAILURE);
}

#endif
