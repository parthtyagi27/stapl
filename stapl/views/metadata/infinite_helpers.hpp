/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_INFINITE_HELPERS_HPP
#define STAPL_VIEWS_METADATA_INFINITE_HELPERS_HPP

#include <type_traits>
#include <stapl/utility/tuple.hpp>
#include <stapl/domains/infinite.hpp>
#include <stapl/views/type_traits/has_domain.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Implementation metafunction for @ref has_finite_domain, which
///   guards inspection of  a domain_type typedef to views that define it.
///
/// @todo Remove when all views define domain_type (i.e., inherit from
///   @ref core_view)
//////////////////////////////////////////////////////////////////////
template<typename View, bool = has_domain_type<View>::value>
struct has_finite_domain_impl
  : public std::integral_constant<
             bool,
             !(std::is_base_of<infinite_impl::infinite_base,
                               typename View::domain_type>::value)
    >
{ };


template<typename View>
struct has_finite_domain_impl<View, false>
  : public std::true_type
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Type metafunction returning true if View parameter has a
///   domain type other than @ref infinite.
//////////////////////////////////////////////////////////////////////
template<typename View>
struct has_finite_domain
  : public has_finite_domain_impl<View>
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Type metafunction returning the index of the first element
///   (i.e., view) of the tuple parameter @p Views which has a finite
///   domain.
//////////////////////////////////////////////////////////////////////
template<typename Views>
struct first_finite_domain_index
  : public stapl::find_first_index<Views, has_finite_domain>
{ };

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_INFINITE_HELPERS_HPP
