/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

// Coarsen partition based on locality information

#ifndef STAPL_VIEWS_METADATA_EXTRACT_METADATA_HPP
#define STAPL_VIEWS_METADATA_EXTRACT_METADATA_HPP

#include <type_traits>

#include <stapl/views/type_traits/has_locality_metadata.hpp>
#include <stapl/views/metadata/locality_dist_metadata.hpp>
#include <stapl/views/metadata/coarsening_traits.hpp>
#include <stapl/views/metadata/coarsen_view_fwd.hpp>

namespace stapl {

namespace metadata {

//////////////////////////////////////////////////////////////////////
/// @brief Functor used to coarsen the data managed for the given @c
///        Container based on the data locality.
///
/// This functor is used when the given container is not a view.
//////////////////////////////////////////////////////////////////////
template<typename Container,
         bool = is_view<Container>::value,
         bool = stapl::detail::has_loc_dist_metadata<Container>::value>
class extract_metadata
  : public locality_dist_metadata<Container>::type
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Functor used to coarsen the data managed for the given @c
///        Container based on the data locality.
///
/// This functor is used when the given container is a view and
/// provides a functor to extract metadata locality.
//////////////////////////////////////////////////////////////////////
template<typename View>
class extract_metadata<View, true, true>
  : public View::loc_dist_metadata
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Functor used to coarsen the data managed for the given @c
///        Container based on the data locality.
///
/// This functor is used when the given container is a view
/// and does not provide a functor to extract metadata locality.
///
/// @todo Function operator should return a shared_ptr.
//////////////////////////////////////////////////////////////////////
template<typename View>
class extract_metadata<View, true, false>
{
private:
  using coarsening_type =
    typename select_extraction_projection_policy<View>::type;

public:
  using return_type = typename coarsening_type::return_type;

  return_type operator()(View* view)
  {
    return coarsening_type::apply(view);
  }
};

} // namespace metadata

} // namespace stapl

#endif
