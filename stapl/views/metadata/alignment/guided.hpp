/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_METADATA_COARSEN_ALIGN_GUIDED_HPP
#define STAPL_VIEWS_METADATA_COARSEN_ALIGN_GUIDED_HPP

#include <stapl/views/metadata/alignment/guided_offset.hpp>

#include <stapl/containers/type_traits/dimension_traits.hpp>
#include <stapl/utility/pack_ops.hpp>

namespace stapl {

namespace metadata {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute the alignment policy for a given
///        set of views.
//////////////////////////////////////////////////////////////////////
template<bool Multidimensional, typename ViewSet>
struct select_alignment_policy
{
  typedef guided_offset_alignment<ViewSet> type;
};

} // namespace detail

template<typename ViewSet>
struct guided_alignment;

//////////////////////////////////////////////////////////////////////
/// @brief Generates a metadata alignment for the given view set, using
///        the view at position @p k to guide the alignment.
//////////////////////////////////////////////////////////////////////
template<typename... View>
struct guided_alignment<tuple<View...>>
  : public detail::select_alignment_policy<
      pack_ops::and_<(dimension_traits<View>::type::value != 1)...>::value,
      tuple<View...>
    >::type
{ };

} // namespace metadata

} // namespace stapl

#endif // STAPL_VIEWS_METADATA_COARSEN_ALIGN_GUIDED_HPP
