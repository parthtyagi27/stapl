/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_TYPE_TRAITS_IS_SEGMENTED_VIEW_HPP
#define STAPL_VIEWS_TYPE_TRAITS_IS_SEGMENTED_VIEW_HPP

#include <stapl/views/segmented_view_base.hpp>
#include <stapl/views/view_traits.hpp>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Type checker to determine if the given @p View type is an
///        instantiation of segmented_view.
//////////////////////////////////////////////////////////////////////
template <typename View, bool = is_view<View>::value>
struct is_segmented_view
  : std::integral_constant<bool,
      std::is_base_of<segmented_view_base, View>::value ||
      std::is_base_of<view_container_base,
        typename view_traits<View>::container
      >::value
    >
{ };

template <typename View>
struct is_segmented_view<View, false>
  : std::false_type
{ };

} //namespace stapl

#endif // STAPL_VIEWS_TYPE_TRAITS_IS_SEGMENTED_VIEW_HPP
