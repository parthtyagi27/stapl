/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PARAGRAPH_REQUIREMENTS_HPP
#define STAPL_PARAGRAPH_REQUIREMENTS_HPP

namespace stapl {

namespace view_operations {

//////////////////////////////////////////////////////////////////////
/// @brief Defines the operations expected by the PARAGRAPH during
///        task creation.
//////////////////////////////////////////////////////////////////////
template<typename Derived>
class paragraph_required_operation
{
private:
  typedef typename view_traits<Derived>::value_type  subview_type;
  typedef subview_type                               value_t;

  const Derived& derived() const
  {
    return static_cast<const Derived&>(*this);
  }

public:
  typedef typename view_traits<Derived>::index_type cid_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of elements referenced by the view.
  /// @todo This method provides the same information as the @c size
  ///       method and could be replaced with it.
  //////////////////////////////////////////////////////////////////////
  size_t get_num_subviews() const
  {
    return derived().size();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the number of elements stored locally, referenced
  ///        for the view.
  //////////////////////////////////////////////////////////////////////
  size_t get_num_local_subviews() const
  {
    return derived().container().local_size();
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the element associated with the position @c i.
  /// @todo To be removed, use operator[] instead of this method.
  //////////////////////////////////////////////////////////////////////
  value_t get_subview(cid_type const& i) const
  {
    return derived().get_element(i);
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the global index of the given local @c index.
  //////////////////////////////////////////////////////////////////////
  cid_type get_local_vid(cid_type const& index) const
  {
    return derived().container().get_local_vid(index);
  }
}; // class paragraph_required_operation

} // namespace view_operations

} // namespace stapl

#endif // STAPL_PARAGRAPH_REQUIREMENTS_HPP
