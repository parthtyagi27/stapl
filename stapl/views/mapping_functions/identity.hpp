/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_MAPFUNC_IDENTITY_H
#define STAPL_MAPFUNC_IDENTITY_H


namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Defines an identity mapping function.
///
/// @tparam T Type of index
//////////////////////////////////////////////////////////////////////
template<typename T>
struct f_ident
{
  typedef T          index_type;
  typedef T          gid_type;
  typedef f_ident<T> inverse;

  gid_type operator()(index_type const& x) const
  {
    return x;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief  Applies the mapping on the input indices and calls the
  ///   @c get_reference functor with the mapped indices.
  ///
  /// @tparam RefGetter   Functor specified by an underlying container, that
  ///   returns a reference to the container's element at given position.
  /// @tparam Indices     Indices to be mapped.
  /// @return Reference to the element at the mapped position.
  //////////////////////////////////////////////////////////////////////
  template<typename RefGetter, typename... Indices>
  typename std::result_of<RefGetter(Indices...)>::type
  apply_get(RefGetter const& get_reference, Indices... indices) const
  {
    return get_reference(indices...);
  }
};

} // namespace stapl

#endif
