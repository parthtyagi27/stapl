/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_MAPPING_FUNCTION_TRAITS_HPP
#define STAPL_VIEWS_MAPPING_FUNCTION_TRAITS_HPP


namespace stapl {

BOOST_MPL_HAS_XXX_TRAIT_DEF(is_injective)
BOOST_MPL_HAS_XXX_TRAIT_DEF(is_bijective)
BOOST_MPL_HAS_XXX_TRAIT_DEF(is_surjective)

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a mapping function is injective.
//////////////////////////////////////////////////////////////////////
template<typename MF, bool b = has_is_injective<MF>::value>
struct is_injective
  : std::false_type
{ };


template<typename MF>
struct is_injective<MF, true>
  : MF::is_injective
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a mapping function is surjective.
//////////////////////////////////////////////////////////////////////
template<typename MF, bool b = has_is_surjective<MF>::value>
struct is_surjective
  : std::false_type
{ };


template<typename MF>
struct is_surjective<MF, true>
  : MF::is_surjective
{ };


//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to determine if a mapping function is bijective.
///        A mapping function is bijective if it reflects a true
///        is_bijective trait, or it is both injective and surjective.
//////////////////////////////////////////////////////////////////////
template<typename MF, bool b = has_is_bijective<MF>::value>
struct is_bijective
  : std::integral_constant<bool,
      is_injective<MF>::value && is_surjective<MF>::value
    >
{ };


template<typename MF>
struct is_bijective<MF, true>
  : MF::is_bijective
{ };

} // namespace stapl

#endif
