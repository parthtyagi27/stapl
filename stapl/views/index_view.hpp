/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_VIEWS_INDEX_VIEW_HPP
#define STAPL_VIEWS_INDEX_VIEW_HPP

#include <stapl/views/counting_view.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Helper function that creates a read-only view representing
///   the indices of the passed container, with the same logical
///   partitioning of elements on locations as that container.
///
/// @param ct The container whose domain and partition should be used
/// to create the @p index_view.
///
/// Relies on @ref counting_view implementation.
///
/// @todo Allow passing of any view. Tied to this is to adjust coarsener
/// to treat index_view and @ref counting_view as dont_care with regard
/// to locality (they can be arbitrarily aligned to any either view
/// in multiview coarsening).
//////////////////////////////////////////////////////////////////////
template<typename Container>
typename result_of::counting_view_nd<
  typename Container::gid_type,
  tuple_size<typename Container::gid_type>::value,
  view_impl::default_container_nd,
  typename Container::distribution_type
>::type
index_view(Container const& ct)
{
  using gid_type     = typename Container::gid_type;
  using dist_type    = typename Container::distribution_type;
  constexpr size_t N = tuple_size<gid_type>::value;

  return typename result_of::counting_view_nd<gid_type, N,
    view_impl::default_container_nd, dist_type>::type(
    new view_impl::counting_container<gid_type, N,
    view_impl::default_container_nd,  dist_type>(
    ct.dimensions(), ct.domain().first(), ct.distribution()));
}

} // namespace stapl

#endif // STAPL_VIEWS_INDEX_VIEW_HPP
