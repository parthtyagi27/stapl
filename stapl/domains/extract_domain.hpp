/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_DOMAINS_EXTRACT_DOMAIN_HPP
#define STAPL_DOMAINS_EXTRACT_DOMAIN_HPP

#include <stapl/domains/domains.hpp>
#include <stapl/views/type_traits/has_domain.hpp>

namespace stapl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to extract the domain associated with the
///        given @c container.
///
/// Specialization used when the container provides domain
/// information.
//////////////////////////////////////////////////////////////////////
template<typename Container,
         bool b_has_domain = has_domain_type<Container>::value>
struct extract_domain
{
  typedef typename Container::domain_type result_type;

  result_type operator()(Container* container) const
  {
    return container->domain();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to extract the domain associated with the
///        given @c container.
///
/// Specialization used when the container does not specify domain
/// information. A domain based on iterators is returned.
/// @todo Replace seqDom/dom1D with iterator_domain
//////////////////////////////////////////////////////////////////////
template<typename Container>
struct extract_domain<Container, false>
{
  typedef typename Container::iterator    iterator;
  typedef seqDom<iterator>                dom_t;
  typedef dom1D<iterator, dom_t>          result_type;

  result_type operator()(Container* container) const
  {
    if (container->size() > 0) {
      return result_type(
        dom_t(container->begin(),--(container->end()), true, container->size())
      );
    }

    // else
    return result_type(
      dom_t(container->begin(), container->begin(), true, 0)
    );
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to extract the domain associated with the
///        given @c container.
///
/// Specialization used when the container is a std::list.
/// @todo Replace seqDom/dom1D with iterator_domain
/// @todo Probably move these STL specific specializations to a
/// different file.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct extract_domain<std::list<T>, false>
{
private:
  typedef typename std::list<T>::iterator     iterator;
  typedef seqDom<iterator>                     dom_t;

public:
  typedef dom1D<iterator,dom_t>               result_type;

  result_type operator()(std::list<T>* ct)
  {
    if (ct->size() > 0)
    {
      return result_type(
        dom_t(ct->begin(), --(ct->end()), true, ct->size())
      );
    }

    // else
    return result_type(
      dom_t(ct->begin(), ct->begin(), true, 0)
    );
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to extract the domain associated with the
///        given @c container.
///
/// Specialization used when the container is a std::vector.
/// @todo Probably move these STL specific specializations to a
/// different file.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct extract_domain<std::vector<T>, false>
{
  typedef indexed_domain<size_t> result_type;

  result_type operator()(std::vector<T>* container) const
  {
    return result_type(container->size());
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper functor to extract the domain associated with the
///        given @c container.
///
/// Specialization used when the container is a std::string.
/// @todo Probably move these STL specific specializations to a
/// different file.
//////////////////////////////////////////////////////////////////////
template<>
struct extract_domain<std::string, false>
{
  typedef indexed_domain<size_t> result_type;

  result_type operator()(std::string* container) const
  {
    return result_type(container->size());
  }
};

} // namespace detail

} // namespace stapl

#endif // STAPL_DOMAINS_EXTRACT_DOMAIN_HPP
