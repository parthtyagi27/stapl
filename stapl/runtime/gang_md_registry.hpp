/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_GANG_MD_REGISTRY_HPP
#define STAPL_RUNTIME_GANG_MD_REGISTRY_HPP

#include "config.hpp"
#include <boost/smart_ptr/intrusive_ptr.hpp>

namespace stapl {

namespace runtime {

class gang_md;
class fence_md;


//////////////////////////////////////////////////////////////////////
/// @brief Gang metadata registry.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
class gang_md_registry
{
public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Initializes the registry.
  ///
  /// @param pid   Current process id.
  /// @param npids Number of processes.
  //////////////////////////////////////////////////////////////////////
  static void initialize(const process_id pid, const unsigned int npids);

  //////////////////////////////////////////////////////////////////////
  /// @brief Finalizes the registry.
  ///
  /// Waits for all @ref gang_md objects to be unregistered.
  //////////////////////////////////////////////////////////////////////
  static void finalize(void);

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the process that owns the @p gid.
  //////////////////////////////////////////////////////////////////////
  static process_id id_owner(const gang_id gid) noexcept;

  //////////////////////////////////////////////////////////////////////
  /// @brief Reserves and returns an id for a gang.
  //////////////////////////////////////////////////////////////////////
  static gang_id reserve_id(void);

  //////////////////////////////////////////////////////////////////////
  /// @brief Registers @p g with @p gid.
  //////////////////////////////////////////////////////////////////////
  static void register_gang_md(gang_md& g, const gang_id gid);

  //////////////////////////////////////////////////////////////////////
  /// @brief Unregisters @p g and releases its id.
  //////////////////////////////////////////////////////////////////////
  static void unregister_gang_md(gang_md const& g);

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the @ref gang_md associated with @p gid.
  //////////////////////////////////////////////////////////////////////
  static gang_md& get(const gang_id gid);

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns a pointer to the @ref gang_md associated with @p gid,
  ///        otherwise @p nullptr.
  //////////////////////////////////////////////////////////////////////
  static gang_md* try_get(const gang_id gid);

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns a pointer to the temporary fence metadata associated with
  ///        the gang with id @p gid.
  ///
  /// If the metadata does not exist, it will create it.
  //////////////////////////////////////////////////////////////////////
  static boost::intrusive_ptr<fence_md> get_fence_md(const gang_id gid);

  //////////////////////////////////////////////////////////////////////
  /// @brief Erases the temporary fence metadata.
  ///
  /// It will update the metadata of the @ref gang_md object associated with
  /// @p p.
  //////////////////////////////////////////////////////////////////////
  static void erase_fence_md(fence_md* const p);
};

} // namespace runtime

} // namespace stapl

#endif
