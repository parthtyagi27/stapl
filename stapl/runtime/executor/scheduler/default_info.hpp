/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_RUNTIME_EXECUTOR_SCHEDULER_DEFAULT_INFO_HPP
#define STAPL_RUNTIME_EXECUTOR_SCHEDULER_DEFAULT_INFO_HPP

#include "../../serialization_fwd.hpp"
#include "../../tags.hpp"
#include "../../stapl_assert.hpp"

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Default scheduling information.
///
/// Priority-based scheduling information based on <tt>unsigned int</tt>.
///
/// @ingroup scheduling
//////////////////////////////////////////////////////////////////////
class default_info
{
public:
  using priority_type = unsigned int;

private:
  priority_type m_priority;

public:
  constexpr default_info(none_t) noexcept
    : m_priority(0)
  { }

  constexpr explicit default_info(priority_type priority = 0) noexcept
    : m_priority(priority)
  { }

  operator none_t(void) const noexcept
  {
    stapl_assert(m_priority == 0, "converting non-zero priority to none_t");
    return none;
  }

  constexpr priority_type priority(void) const noexcept
  { return m_priority; }

  void define_type(typer& t)
  { t.member(m_priority); }

  friend constexpr bool operator==(default_info const& x,
                                   default_info const& y) noexcept
  { return (x.m_priority == y.m_priority); }

  friend constexpr bool operator<(default_info const& x,
                                  default_info const& y) noexcept
  { return (x.m_priority < y.m_priority); }
};

} // namespace stapl

#endif
