/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_WATCHDOG_TIMER_HPP
#define STAPL_RUNTIME_UTILITY_WATCHDOG_TIMER_HPP

#include <atomic>
#include <functional>
#include <thread>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Allows the creation of a watchdog timer that invokes the given
///        function every @p interval milliseconds.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
class watchdog_timer
{
private:
  typedef std::function<void(void)> function_type;

  function_type     m_f;
  std::atomic<bool> m_active;
  std::thread       m_thread;

public:
  watchdog_timer(void)
  : m_active(false)
  { }

  template<typename Function, typename Interval>
  watchdog_timer(Function&& f, Interval&& interval)
  : m_active(false)
  { start(std::forward<Function>(f), std::forward<Interval>(interval)); }

  template<typename Function, typename Interval>
  void start(Function&& f, Interval&& interval)
  {
    if (this->m_active.load(std::memory_order_relaxed))
      stop();
    m_f      = std::forward<Function>(f);
    m_active = true;
    m_thread = std::thread([this, interval]
               {
                 do {
                   std::this_thread::sleep_for(interval);
                   this->m_f();
                 } while (this->m_active.load(std::memory_order_relaxed));
               });
  }

  void stop(void)
  {
    m_active.store(false, std::memory_order_relaxed);
    m_thread.join();
  }

  ~watchdog_timer(void)
  { stop(); }
};

} // namespace stapl

#endif
