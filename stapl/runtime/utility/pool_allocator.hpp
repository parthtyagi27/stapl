/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_UTILITY_POOL_ALLOCATOR_HPP
#define STAPL_RUNTIME_UTILITY_POOL_ALLOCATOR_HPP

#include "../new.hpp"
#include <memory>

namespace stapl {

template<typename T, std::size_t N = 128>
class pool_allocator;


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref pool_allocator for @c void.
///
/// See C++11 Draft n3337 20.6.9
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<std::size_t N>
class pool_allocator<void, N>
{
public:
  typedef void*       pointer;
  typedef const void* const_pointer;
  typedef void        value_type;

  template<typename U>
  struct rebind
  {
    typedef pool_allocator<U, N> other;
  };
};


//////////////////////////////////////////////////////////////////////
/// @brief Pool backed allocator.
///
/// Allocator that uses pool allocation for small (<= @p N bytes) allocations,
/// backed by the @ref runtime::memory_allocator. For larger allocations, it
/// redirects to the standard allocator.
///
/// @warning This allocator may increase your memory consumption, as memory
///          reserved by a pool cannot be used by the rest of the program.
///
/// @warning Memory allocated from this allocator can only be freed in the same
///          thread it was allocated from.
///
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T, std::size_t N>
class pool_allocator
: private std::allocator<T>
{
private:
  template<typename, std::size_t>
  friend class pool_allocator;

public:
  typedef std::size_t    size_type;
  typedef std::ptrdiff_t difference_type;
  typedef T*             pointer;
  typedef const T*       const_pointer;
  typedef T&             reference;
  typedef T const&       const_reference;
  typedef T              value_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Provides a way to obtain an allocator for a different type.
  //////////////////////////////////////////////////////////////////////
  template<typename U>
  struct rebind
  {
    typedef pool_allocator<U, N> other;
  };

  pool_allocator(void) noexcept = default;
  pool_allocator(pool_allocator const&) = default;

  template<typename U>
  pool_allocator(pool_allocator<U, N> const& other) noexcept
  : std::allocator<T>(other)
  { }

  using std::allocator<T>::address;

  pointer allocate(size_type n,
                   typename pool_allocator<void, N>::const_pointer hint = 0)
  {
    using namespace runtime;

    const std::size_t sz = new_impl::normalize_size(n * sizeof(T));
    return static_cast<pointer>(sz <= N ? memory_allocator::allocate(sz)
                                        : std::allocator<T>::allocate(n, hint));
  }

  void deallocate(pointer p, size_type n)
  {
    using namespace runtime;

    const std::size_t sz = new_impl::normalize_size(n * sizeof(T));
    if (sz <= N)
      memory_allocator::deallocate(p, sz);
    else
      std::allocator<T>::deallocate(p, n);
  }

  using std::allocator<T>::max_size;
  using std::allocator<T>::construct;
  using std::allocator<T>::destroy;
};


template<typename T1, std::size_t N1, typename T2, std::size_t N2>
constexpr bool operator==(pool_allocator<T1> const&,
                          pool_allocator<T2> const&) noexcept
{
  return true;
}

template<typename T1, std::size_t N1, typename T2, std::size_t N2>
constexpr bool operator!=(pool_allocator<T1, N1> const&,
                          pool_allocator<T2, N2> const&) noexcept
{
  return false;
}

} // namespace stapl

#endif
