/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_IMMUTABLE_REF_HPP
#define STAPL_RUNTIME_IMMUTABLE_REF_HPP

#include "serialization.hpp"
#include "type_traits/is_basic.hpp"
#include "type_traits/is_copyable.hpp"
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Wraps a reference to a temporarily immutable object.
///
/// @tparam T Object type.
///
/// Referents of immutable references may avoid serialization when communication
/// happens in shared memory. The referent must not be deleted or mutated until
/// all @ref immutable_reference_wrapper to it have been deleted. This is
/// commonly guaranteed with synchronization.
///
/// Once all the @ref immutable_reference_wrapper objects have been destroyed,
/// then the referenced object can be mutated or deleted.
///
/// @see immutable_shared
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T, bool = (sizeof(T)<=sizeof(T*) && is_basic<T>::value)>
class immutable_reference_wrapper
{
public:
  using type = T;

private:
  T const* m_t;

public:
  immutable_reference_wrapper(T const& t) noexcept
  : m_t(std::addressof(t))
  { }

  immutable_reference_wrapper(T&&) = delete;

  immutable_reference_wrapper(immutable_reference_wrapper const&) noexcept =
    default;
  immutable_reference_wrapper(immutable_reference_wrapper&&) noexcept = default;

  operator T const&(void) const noexcept
  { return *m_t; }

  T const& get(void) const noexcept
  { return *m_t; }

  void define_type(typer& t)
  { t.member(m_t); }
};


//////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref immutable_reference_wrapper for basic types
///        for which <tt>sizeof(T)<=sizeof(T*)</tt>.
///
/// @see is_basic
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
class immutable_reference_wrapper<T, true>
{
public:
  using type = T;

private:
  T m_t;

public:
  immutable_reference_wrapper(T const& t) noexcept
  : m_t(t)
  { }

  immutable_reference_wrapper(T&&) = delete;

  immutable_reference_wrapper(immutable_reference_wrapper const&) noexcept =
    default;
  immutable_reference_wrapper(immutable_reference_wrapper&&) noexcept = default;

  operator T const&(void) const noexcept
  { return m_t; }

  T const& get(void) const noexcept
  { return m_t; }

  void define_type(typer& t)
  { t.member(m_t); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Creates an immutable reference to @p t.
///
/// An immutable reference may pass the object by reference during shared memory
/// communication, avoiding any potential copies.
///
/// @warning The sender has to guarantee that all callees have finished before
///          deleting or mutating the object.
///
/// @see make_immutable_shared
/// @related immutable_reference_wrapper
/// @ingroup ARMIUtilities
//////////////////////////////////////////////////////////////////////
template<typename T>
immutable_reference_wrapper<T> immutable_ref(T const& t)
{
  return immutable_reference_wrapper<T>{t};
}


namespace runtime {

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref is_copyable for
///        @ref immutable_reference_wrapper.
///
/// @ingroup runtimeTypeTraits
////////////////////////////////////////////////////////////////////
template<typename T>
struct is_copyable<immutable_reference_wrapper<T>>
: public std::true_type
{ };

} // namespace runtime

} // namespace stapl

#endif
