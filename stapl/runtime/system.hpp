/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_SYSTEM_HPP
#define STAPL_RUNTIME_SYSTEM_HPP

#include "utility/c_string.hpp"
#include <cstddef>

namespace stapl {

namespace runtime {

////////////////////////////////////////////////////////////////////
/// @brief Returns a demangled version of the given mangled name.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
c_string demangle(const char*);

////////////////////////////////////////////////////////////////////
/// @brief Returns a demangled version of the given mangled name.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
c_string demangle(c_string const&);


////////////////////////////////////////////////////////////////////
/// @brief Returns the process id of the calling process.
///
/// @ingroup system
////////////////////////////////////////////////////////////////////
int getpid(void) noexcept;


////////////////////////////////////////////////////////////////////
/// @brief Returns the total amount of physical memory in bytes.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
std::size_t get_total_physical_memory(void) noexcept;

////////////////////////////////////////////////////////////////////
/// @brief Returns the amount of physical memory that is being used in bytes.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
std::size_t get_used_physical_memory(void) noexcept;

////////////////////////////////////////////////////////////////////
/// @brief Returns the amount of physical memory available in bytes.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
std::size_t get_available_physical_memory(void) noexcept;

////////////////////////////////////////////////////////////////////
/// @brief Returns the percentage of physical memory that is being used (0-100).
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
std::size_t get_physical_memory_load(void) noexcept;


////////////////////////////////////////////////////////////////////
/// @brief Returns the number of hardware threads on the system.
///
/// @ingroup runtimeUtility
////////////////////////////////////////////////////////////////////
std::size_t get_num_hardware_threads(void) noexcept;

} // namespace runtime

} // namespace stapl

#endif
