/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COMMUNICATOR_BARRIER_HPP
#define STAPL_RUNTIME_COMMUNICATOR_BARRIER_HPP

#include "reduce.hpp"
#include <functional>
#include <utility>

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Performs a barrier over the processes specified by the @ref topology
///        object.
///
/// @see collective
/// @ingroup runtimeCollectives
///
/// @todo Use platform optimized barrier implementation.
//////////////////////////////////////////////////////////////////////
class barrier
{
private:
  reduce<bool, std::logical_and<bool>> m_red;

public:
  ////////////////////////////////////////////////////////////////////
  /// @brief Constructs a @ref barrier object.
  ///
  /// @param gid Id of the gang the collective executes in.
  /// @param cid Collective operation id.
  /// @param t   @ref topology object associated with the gang.
  /// @param f   Function to call when all processes have reached the barrier.
  ////////////////////////////////////////////////////////////////////
  template<typename Function>
  barrier(const gang_id gid,
          const collective_id cid,
          topology const& t,
          Function&& f)
  : m_red(gid, cid, t, [f](bool) { f(); })
  { }

  void operator()(void)
  { m_red(true); }
};

} // namespace runtime

} // namespace stapl

#endif
