 
/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_REQUEST_HEADER_DECODER_HPP
#define STAPL_RUNTIME_REQUEST_HEADER_DECODER_HPP

#include "header.hpp"
#include "../context_id.hpp"
#include "../exception.hpp"
#include "../message.hpp"
#include "../runtime_fwd.hpp"

namespace stapl {

namespace runtime {

//////////////////////////////////////////////////////////////////////
/// @brief Returns the epoch @p m was sent from.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
inline header::epoch_type get_message_epoch(message const& m) noexcept
{
  switch (m.type()) {
    case header::RMI:
      return m.get_extended_header<header::request>().get_epoch();

    case header::BCAST_RMI:
    case header::UNORDERED_BCAST_RMI:
      return m.get_extended_header<header::bcast_request>().get_epoch();

    default:
      STAPL_RUNTIME_ERROR("Unexpected request type.");
      return header::epoch_type{};
  }
}


//////////////////////////////////////////////////////////////////////
/// @brief Returns the context id, the epoch and the process id @p m was sent
///        from as a @c std::tuple.
///
/// @param lid Location id of the receiving location.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
inline std::tuple<context_id, header::epoch_type, process_id>
get_message_info(message const& m, const location_id lid) noexcept
{
  switch (m.type()) {
    case header::LOCATION_RPC: {
      auto const& h = m.get_extended_header<header::location_rpc>();
      return std::make_tuple(context_id{}, h.get_epoch(), h.get_process_id());
    }

    case header::BCAST_LOCATION_RPC: {
      auto const& h = m.get_extended_header<header::bcast_location_rpc>();
      return std::make_tuple(context_id{}, h.get_epoch(), h.get_process_id());
    }

    case header::RMI: {
      auto const& h = m.get_extended_header<header::request>();
      STAPL_RUNTIME_ASSERT(lid==h.get_context_id().current.get_location_id());
      return std::make_tuple(h.get_context_id(),
                             h.get_epoch(),
                             h.get_process_id());
    }

    case header::BCAST_RMI:
    case header::UNORDERED_BCAST_RMI: {
      auto const& h = m.get_extended_header<header::bcast_request>();
      return std::make_tuple(h.make_context_id(lid),
                             h.get_epoch(),
                             h.get_process_id());
    }

    default:
      STAPL_RUNTIME_ERROR("Unexpected request type.");
      return std::tuple<context_id, header::epoch_type, process_id>{};
  }
}

} // namespace runtime

} // namespace stapl

#endif
