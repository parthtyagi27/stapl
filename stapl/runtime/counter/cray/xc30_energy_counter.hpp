/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_COUNTER_CRAY_XC30_ENERGY_COUNTER_HPP
#define STAPL_RUNTIME_COUNTER_CRAY_XC30_ENERGY_COUNTER_HPP

#include "../../config/platform.hpp"
#include <cstddef>
#include <fstream>

#ifndef STAPL_RUNTIME_CRAY_XC30_TARGET
# error "xc30_energy_counter is only valid for Cray XC30 machines."
#endif

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Counter that measures energy consumption in Joules for the Cray XC30
///        platform.
///
/// The counter reads the <tt>/sys/cray/pm_counters/energy</tt> file by default
/// for energy consumption information per node.
///
/// @ingroup counters
/////////////////////////////////////////////////////////////////////
class xc30_energy_counter
{
public:
  typedef std::size_t raw_value_type;
  typedef std::size_t value_type;

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the name of the counter as a C string.
  //////////////////////////////////////////////////////////////////////
  static constexpr const char* name(void) noexcept
  { return "cray_xc_30_energy_counter()"; }

  //////////////////////////////////////////////////////////////////////
  /// @brief Normalizes the given raw value to Joules.
  //////////////////////////////////////////////////////////////////////
  static constexpr value_type normalize(const raw_value_type v) noexcept
  { return value_type(v); }

private:
  std::ifstream  m_is;
  raw_value_type m_v;

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief Constructs a new @ref xc30_energy_counter that reads from
  ///        @p filename.
  //////////////////////////////////////////////////////////////////////
  explicit
  xc30_energy_counter(const char* filename = "/sys/cray/pm_counters/energy")
  : m_is(filename, std::ios::in),
    m_v(0)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Returns the raw value from the counter.
  //////////////////////////////////////////////////////////////////////
  raw_value_type read(void)
  {
    m_is.seekg(0);
    raw_value_type joules = 0;
    m_is >> joules;
    return joules;
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Starts the counter.
  //////////////////////////////////////////////////////////////////////
  void start(void)
  { m_v = read(); }

  //////////////////////////////////////////////////////////////////////
  /// @brief Stops the counter and returns the difference from @ref start() in
  ///        Joules.
  //////////////////////////////////////////////////////////////////////
  value_type stop(void)
  { return normalize(read() - m_v); }
};

} // namespace stapl

#endif
