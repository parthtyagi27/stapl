/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_RUNTIME_SERIALIZATION_MEM_FN_HPP
#define STAPL_RUNTIME_SERIALIZATION_MEM_FN_HPP

#include "typer_fwd.hpp"
#include <type_traits>

#if defined(STAPL__GNUC__) && defined(STAPL__GNUC_MINOR__)
# if ((STAPL__GNUC__ == 4) && (STAPL__GNUC_MINOR__ <= 9) || \
      (STAPL__GNUC__ == 5) && (STAPL__GNUC_MINOR__ <= 4) || \
      (STAPL__GNUC__ == 6) && (STAPL__GNUC_MINOR__ <= 5) || \
      (STAPL__GNUC__ == 7) && (STAPL__GNUC_MINOR__ <= 5) || \
      (STAPL__GNUC__ == 8) && (STAPL__GNUC_MINOR__ <= 4) || \
      (STAPL__GNUC__ == 9) && (STAPL__GNUC_MINOR__ <= 3) || \
      (STAPL__GNUC__ == 10) && (STAPL__GNUC_MINOR__ <= 3))
# else
#  error "Only select versions of gcc 4.x and 5.x are supported"
# endif
#else
# error "Unable to determine libstdc++ version... aborting"
#endif

namespace std {

template<typename, typename...>
struct _Maybe_unary_or_binary_function;


template<typename>
class _Mem_fn;

} // namespace std


namespace stapl {

////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref define_type_provider for the result of
///        @c std::mem_fn (non-const qualified pointer to members).
///
/// @warning This implementation relies on the compiler to create identical
///          layout for two classes that are written in a similar way (identical
///          members and access control). The @c static_assert calls attempt to
///          detect any possible issues if the compiler changes its behavior.
///
/// @ingroup serialization
////////////////////////////////////////////////////////////////////
template<typename _Res, typename _Class, typename... _ArgTypes>
struct define_type_provider<std::_Mem_fn<_Res (_Class::*)(_ArgTypes...)>>
{
  //////////////////////////////////////////////////////////////////////
  /// @brief @c std::_Mem_fn doppelganger that provides @c define_type().
  //////////////////////////////////////////////////////////////////////
  class wrapper
  : public std::_Maybe_unary_or_binary_function<_Res, _Class*, _ArgTypes...>
  {
    using Functor = _Res (_Class::*)(_ArgTypes...);

    Functor __pmf;

  public:
    void define_type(typer& t)
    {
      t.base<
        std::_Maybe_unary_or_binary_function<_Res, _Class*, _ArgTypes...>
      >(*this);

      t.member(__pmf);
    }
  };

  static wrapper&
  apply(std::_Mem_fn<_Res (_Class::*)(_ArgTypes...)>& t) noexcept
  {
    static_assert(
      (sizeof(wrapper) ==
         sizeof(std::_Mem_fn<_Res (_Class::*)(_ArgTypes...)>)) &&
      (std::alignment_of<wrapper>::value ==
         std::alignment_of<std::_Mem_fn<_Res(_Class::*)(_ArgTypes...)>>::value),
      "Incompatible types.");
    return reinterpret_cast<wrapper&>(t);
  }
};


////////////////////////////////////////////////////////////////////
/// @internal
/// @brief Specialization of @ref define_type_provider for the result of
///        @c std::mem_fn (const qualified pointer to members).
///
/// @warning This implementation relies on the compiler to create identical
///          layout for two classes that are written in a similar way (identical
///          members and access control). The @c static_assert calls attempt to
///          detect any possible issues if the compiler changes its behavior.
///
/// @ingroup serialization
////////////////////////////////////////////////////////////////////
template<typename _Res, typename _Class, typename... _ArgTypes>
struct define_type_provider<std::_Mem_fn<_Res (_Class::*)(_ArgTypes...) const>>
{
  //////////////////////////////////////////////////////////////////////
  /// @brief @c std::_Mem_fn doppelganger that provides @c define_type().
  //////////////////////////////////////////////////////////////////////
  class wrapper
  : public std::_Maybe_unary_or_binary_function<
      _Res, const _Class*, _ArgTypes...>
  {
    using Functor = _Res (_Class::*)(_ArgTypes...) const;

    Functor __pmf;

  public:
    void define_type(typer& t)
    {
      t.base<
        std::_Maybe_unary_or_binary_function<_Res, const _Class*, _ArgTypes...>
      >(*this);

      t.member(__pmf);
    }
  };

  static wrapper&
  apply(std::_Mem_fn<_Res (_Class::*)(_ArgTypes...) const>& t) noexcept
  {
    static_assert(
      (sizeof(wrapper) ==
         sizeof(std::_Mem_fn<_Res (_Class::*)(_ArgTypes...) const>)) &&
      (std::alignment_of<wrapper>::value ==
         std::alignment_of<std::_Mem_fn<
           _Res(_Class::*)(_ArgTypes...) const>>::value),
      "Incompatible types.");
    return reinterpret_cast<wrapper&>(t);
  }
};

} // namespace stapl

#endif
