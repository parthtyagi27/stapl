/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_MANAGER_UTIL_HPP
#define STAPL_CONTAINERS_MANAGER_UTIL_HPP

#include <stapl/utility/tuple.hpp>

namespace stapl {

namespace cm_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Work function implementing the traversal of a tuple to compare
/// each element against known min and max values.
///
/// @tparam N current element of the tuple being examined
/// @tparam Index type of the full tuple being compared.
///
/// The work function is used to determine the bounds of the domains for
/// multiarray base containers.  This is needed because we currently find
/// the GIDs in each domain in their linear form and delinearize them to
/// get the necessary GID.  The multidimensional domain constructor
/// requires we pass the extrema of the domain to its constructor.
//////////////////////////////////////////////////////////////////////
template<int N, typename Index>
struct bc_min_max
{
private:
  Index& m_min;
  Index& m_max;

public:
  bc_min_max(Index& min, Index& max)
    : m_min(min), m_max(max)
  { }

  void operator()(Index const& val)
  {
    get<N>(m_min) = get<N>(val) < get<N>(m_min) ? get<N>(val) : get<N>(m_min);
    get<N>(m_max) = get<N>(val) > get<N>(m_max) ? get<N>(val) : get<N>(m_max);
    bc_min_max<N-1, Index>(m_min, m_max)(val);
  }
};


template<typename Index>
struct bc_min_max<0, Index>
{
private:
  Index& m_min;
  Index& m_max;

public:
  bc_min_max(Index& min, Index& max)
    : m_min(min), m_max(max)
  { }

  void operator()(Index const& val)
  {
    get<0>(m_min) = get<0>(val) < get<0>(m_min) ? get<0>(val) : get<0>(m_min);
    get<0>(m_max) = get<0>(val) > get<0>(m_max) ? get<0>(val) : get<0>(m_max);
  }
};

} // namespace cm_impl

} // namespace stapl

#endif // ifndef STAPL_CONTAINERS_MANAGER_UTIL_HPP
