/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_ARRAY_FWD_HPP
#define STAPL_CONTAINERS_ARRAY_FWD_HPP

namespace stapl {

#ifdef STAPL_DOCUMENTATION_ONLY

//////////////////////////////////////////////////////////////////////
/// @brief Parallel array container.
/// @ingroup parray
///
/// Parallel sequence container with fixed size. By default, its
/// GID type is a std::size_t, which represents the position of values
/// in the array.
///
/// @tparam T Type of the stored elements in the container. T must be
///   default assignable, copyable and assignable.
/// @tparam PS Partition strategy that defines how to partition
///   the original domain into subdomains. The default partition is
/// @ref balanced_partition.
/// @tparam M Mapper that defines how to map the subdomains produced
///   by the partition to locations. The default mapper is @ref mapper.
/// @tparam Traits A traits class that defines customizable components
///   of array, such as the domain type and base container type. The
///   default traits class is @ref array_traits.
////////////////////////////////////////////////////////////////////////
template<typename T,
         typename PS     = use_default,
         typename M      = use_default,
         typename Traits = use_default>
class array;

#else

template<typename T, typename ...OptionalParams>
class array;

#endif // STAPL_DOCUMENTATION_ONLY

} // namespace stapl

#endif // STAPL_CONTAINERS_ARRAY_FWD_HPP
