/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_DEFAULT_TRAVERSAL_HPP
#define STAPL_CONTAINERS_DEFAULT_TRAVERSAL_HPP

#include <stapl/utility/tuple.hpp>
#include <stapl/utility/integer_sequence.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute a the default traversal for an
/// n-dimensional space.
///
/// Returns a tuple type that defines the importance of each element
/// of the coordinate. The dimension holding 0 will be traversed first,
/// followed by the dimension holding 1, and so on.
///
/// For example, the default traversal for a 3-dimensional space is <2, 1, 0>,
/// which means that for a 3D coordinate (x, y, z), the z dimension will
/// be traversed first, followed by y and ending with x.
///
/// @tparam N The number of dimensions in the space.
///
/// @note icc-16 fails to expand an expression if the @c Indices shows up
/// twice in the expansion.
//////////////////////////////////////////////////////////////////////
template<int N, typename = make_index_sequence<N>>
struct default_traversal;


template<int N, std::size_t... Indices>
struct default_traversal<N, index_sequence<Indices...>>
{
  using size = std::integral_constant<std::size_t, sizeof...(Indices)>;
  using type = tuple<boost::mpl::int_<(size::value - Indices - 1)>...>;
};

} // namespace stapl

#endif
