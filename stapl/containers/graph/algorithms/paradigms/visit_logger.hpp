/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_VISIT_LOGGER_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_PARADIGMS_VISIT_LOGGER_HPP

#include <sstream>

namespace stapl {

namespace sgl {

struct visit_logger
{
#if defined(STAPL_SGL_ENABLE_VISIT_LOGGER)
  template <typename Vertex, typename NeighborOp>
  static void visit_from(Vertex&& v,
                         NeighborOp&& neighbor_op,
                         std::size_t level,
                         std::size_t max_level)
  {
    std::stringstream ss;
    ss << "sgl visit-from " << v.descriptor() << " " << level << " "
       << max_level << " (" << v.property() << ")"
       << " (" << neighbor_op << ")";

    printf("%s\n", ss.str().c_str());
  }

  template <typename Vertex>
  static void
  pre_vertex_op(Vertex&& v, std::size_t level, std::size_t max_level)
  {
    std::stringstream ss;
    ss << "sgl pre-vertex-op " << v.descriptor() << " " << level << " "
       << max_level << " (" << v.property() << ")";

    printf("%s\n", ss.str().c_str());
  }

  template <typename Vertex>
  static void post_vertex_op(Vertex&& v,
                             bool should_continue,
                             std::size_t level,
                             std::size_t max_level)
  {
    std::stringstream ss;
    ss << "sgl post-vertex-op " << v.descriptor() << " " << level << " "
       << max_level << " [" << should_continue << "]"
       << " (" << v.property() << ")";

    printf("%s\n", ss.str().c_str());
  }

  template<typename Vertex, typename NeighborOp>
  static void pre_neighbor_op(Vertex&& v, NeighborOp&& neighbor_op)
  {
    std::stringstream ss;
    ss << "sgl pre-neighbor-op " << v.descriptor()
       << " (" << v.property() << ")"
       << " (" << neighbor_op << ")";

    printf("%s\n", ss.str().c_str());
  }

  template <typename Vertex, typename NeighborOp>
  static void post_neighbor_op(Vertex&& v,
                               NeighborOp&& neighbor_op,
                               bool reinvoke,
                               bool can_repropagate)
  {
    std::stringstream ss;
    ss << "sgl post-neighbor-op " << v.descriptor()
       << " [" << reinvoke << "]"
       << " [" << can_repropagate << "]"
       << " (" << v.property() << ")"
       << " (" << neighbor_op << ")";

    printf("%s\n", ss.str().c_str());
  }

#else
  template<typename Vertex, typename NeighborOp>
  static void visit_from(Vertex&&, NeighborOp&&, std::size_t, std::size_t)
  { }

  template<typename Vertex>
  static void pre_vertex_op(Vertex&&, std::size_t, std::size_t)
  { }

  template<typename Vertex>
  static void post_vertex_op(Vertex&&, bool, std::size_t, std::size_t)
  { }

  template<typename Vertex, typename NeighborOp>
  static void pre_neighbor_op(Vertex&&, NeighborOp&&)
  { }

  template<typename Vertex, typename NeighborOp>
  static void post_neighbor_op(Vertex&&, NeighborOp&&, bool, bool)
  { }
#endif
};

} // namespace sgl


} // namespace stapl

#endif
