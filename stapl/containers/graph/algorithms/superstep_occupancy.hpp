/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_SS_OCCUPANCY_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_SS_OCCUPANCY_HPP

#include <tuple>

namespace stapl {

namespace sgl {


//////////////////////////////////////////////////////////////////////
/// @brief Superstep occupancy allows the algorithm writer to describe the
/// number of vertices for specific supersteps -- the first superstep, a
/// middle supperstep and the last superstep.
///
/// For example, in breadth-first search, the first superstep has a single
/// vertex, a middle superstep has some vertices and the last superstep has
/// some vertices.
//////////////////////////////////////////////////////////////////////
enum class superstep_occupancy
{
  none,
  single,
  some,
  all
};

template <superstep_occupancy Occupancy>
using occupancy_c = std::integral_constant<superstep_occupancy, Occupancy>;

using default_superstep_occupancy_type
  = std::tuple<occupancy_c<superstep_occupancy::some>,
               occupancy_c<superstep_occupancy::some>,
               occupancy_c<superstep_occupancy::some>>;

using always_active_superstep_occupancy_type
  = std::tuple<occupancy_c<superstep_occupancy::all>,
               occupancy_c<superstep_occupancy::all>,
               occupancy_c<superstep_occupancy::all>>;

constexpr default_superstep_occupancy_type default_superstep_occupancy = {};
constexpr always_active_superstep_occupancy_type
  always_active_superstep_occupancy
  = {};

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction to compute whether the given occupancy specification
/// denotes that the first superstep contains all vertices.
///
/// @tparam OccupancyType The tuple of occupancy information
//////////////////////////////////////////////////////////////////////
template <typename OccupancyType>
struct first_superstep_has_all_vertices
  : public std::integral_constant<bool,
                                  tuple_element<0, OccupancyType>::type::value
                                    == superstep_occupancy::all>
{ };

} // namespace sgl

} // stapl namespace

#endif
