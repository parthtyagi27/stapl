/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_GRAPH_ALGORITHMS_EXECUTION_POLICY_PROMOTION_HPP
#define STAPL_CONTAINERS_GRAPH_ALGORITHMS_EXECUTION_POLICY_PROMOTION_HPP

#include <stapl/containers/graph/algorithms/execution_policy.hpp>
#include <stapl/containers/graph/algorithms/execution_policy_builder.hpp>

namespace stapl {

namespace sgl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Helper class used to promote an execution policy
/// @tparam InputView The type of the original view of the policy
/// @tparam OutputView The type of the desired output view
//////////////////////////////////////////////////////////////////////
template<typename InputView,
         typename OutputView>
struct promotion_visitor
  : public boost::static_visitor<sgl::execution_policy<OutputView>>
{
private:
  using type = sgl::execution_policy<OutputView>;

  OutputView& m_view;

  void display_warning() const
  {
    stapl::do_once([](){
      std::cerr << "WARNING: " <<
        "Promotion of execution policy may result in worse performance\n";
    });
  }

public:
  promotion_visitor(OutputView& view)
    : m_view(view)
  { }

  type operator()(sgl::kla_policy& policy) const
  {
    return policy;
  }

  type operator()(sgl::level_sync_policy& policy) const
  {
    return policy;
  }

  type operator()(sgl::async_policy& policy) const
  {
    return policy;
  }

  type operator()(sgl::hierarchical_policy<InputView>& policy) const
  {
    this->display_warning();
    return sgl::build_execution_policy(m_view)
      .active_ratio(policy.active_ratio()).hierarchical().build();
  }

  type operator()(sgl::hierarchical_hubs_policy<InputView>& policy) const
  {
    this->display_warning();
    return sgl::build_execution_policy(m_view)
      .active_ratio(policy.active_ratio()).hubs(policy.hub_degree()).build();
  }

  type operator()(sgl::hierarchical2_policy<InputView>& policy) const
  {
    this->display_warning();
    return sgl::build_execution_policy(m_view)
      .active_ratio(policy.active_ratio()).hierarchical2().build();
  }

  template<typename T>
  type operator()(T&&) const
  {
    stapl::abort("Promotion of execution policy not possible");
    return 0;
  }
};

} // namespace detail

//////////////////////////////////////////////////////////////////////
/// @brief Create a new policy for sgl::execute with a different view
/// based on an already existing policy.
///
/// For example, if one has a hierarchical policy for some graph g and they
/// would like to execute an algorithm on a different graph g' but still using
/// the hierarchical method, they can promote the policy with this other graph
/// g', which will create the new hierarchical view of g' and retain the same
/// parameters of the original policy.
///
/// @param policy The policy to promote
/// @param input_view The original view for the policy
/// @param output_view The view for the new policy
//////////////////////////////////////////////////////////////////////
template<typename Policy, typename InputView, typename OutputView>
execution_policy<OutputView>
promote_policy(Policy& policy, InputView& input_view, OutputView& output_view)
{
  return boost::apply_visitor(
    detail::promotion_visitor<InputView, OutputView>{output_view}, policy
  );
}

} // namespace sgl

} // namespace stapl

#endif
