/*
// Copyright (c) 2000-2009, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// The information and source code contained herein is the exclusive
// property of TEES and may not be disclosed, examined or reproduced
// in whole or in part without explicit written authorization from TEES.
*/

#ifndef STAPL_CONTAINERS_EXECUTION_POLICY_BUILDER_HPP
#define STAPL_CONTAINERS_EXECUTION_POLICY_BUILDER_HPP

#include <stapl/containers/graph/algorithms/execution_policy.hpp>

namespace stapl {

namespace sgl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Enum for the type of graph execution policy
//////////////////////////////////////////////////////////////////////
enum class execution_policy_tag
{
  none,
  level_sync,
  kla,
  async,
};
};

//////////////////////////////////////////////////////////////////////
/// @brief Builder class to build an execution policy instance
/// @tparam View The graph view
/// @tparam policy_tag The execution policy to build
//////////////////////////////////////////////////////////////////////
template<typename View, detail::execution_policy_tag policy_tag>
class execution_policy_builder
{
  View m_view;
  std::size_t m_k;
  std::size_t m_hubs_degree;
  double m_active_ratio;

public:
  execution_policy_builder(View view,
                           std::size_t k,
                           double active_ratio)
    : m_view(std::move(view)),
      m_k(k), m_active_ratio(active_ratio)
  { }

  //////////////////////////////////////////////////////////////////////
  /// @brief Set the active vertex ratio, which is a rough estimate of how
  /// many vertices will be active in a given superstep during the traversal.
  //////////////////////////////////////////////////////////////////////
  execution_policy_builder active_ratio(double active_ratio)
  {
    return { std::move(m_view),
             m_k,
             active_ratio };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Build a KLA execution policy
  /// @param k The level of asynchrony
  //////////////////////////////////////////////////////////////////////
  execution_policy_builder<View,
                           detail::execution_policy_tag::kla>
  kla(std::size_t k)
  {
    return { std::move(m_view),
             k,
             m_active_ratio };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Build a level-synchronous execution policy
  //////////////////////////////////////////////////////////////////////
  execution_policy_builder<View,
                           detail::execution_policy_tag::level_sync>
  level_sync()
  {
    return { std::move(m_view),
             m_k,
             m_active_ratio };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Build an asynchronous execution policy
  //////////////////////////////////////////////////////////////////////
  execution_policy_builder<View,
                           detail::execution_policy_tag::async>
  async()
  {
    return { std::move(m_view),
             m_k,
             m_active_ratio };
  }

  //////////////////////////////////////////////////////////////////////
  /// @brief Build The currently running execution policy
  //////////////////////////////////////////////////////////////////////
  execution_policy<View> build() const
  {
    switch (policy_tag)
    {
      case detail::execution_policy_tag::kla:
      {
        kla_policy policy{ m_k };
        policy.active_ratio(m_active_ratio);
        return policy;
      }

      case detail::execution_policy_tag::async:
      {
        async_policy policy;
        policy.active_ratio(m_active_ratio);
        return policy;
      }

      case detail::execution_policy_tag::level_sync:
      {
        level_sync_policy policy;
        policy.active_ratio(m_active_ratio);
        return policy;
      }

      default:
        stapl::abort("Need to specify a policy for builder");
        return async_policy{};
    }
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Make an execution policy builder
/// @param view The graph view
//////////////////////////////////////////////////////////////////////
template<typename V>
execution_policy_builder<V, detail::execution_policy_tag::none>
build_execution_policy(V view)
{
  return {std::move(view), 0, 0.5};
}

} // namespace sgl

} // stapl namespace

#endif
