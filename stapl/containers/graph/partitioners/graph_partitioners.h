/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef STAPL_GRAPH_PARTITIONERS_H
#define STAPL_GRAPH_PARTITIONERS_H

#include <stapl/containers/graph/partitioners/gpartition.h>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Partition a graph with a collapser functor and a refiner functor
/// @ingroup pgraphPartitioner
/// @param gview graph view to be partitioned
/// @param collapse collapser functor
/// @param refine refiner functor
/// @return gpartition object representing the partition
//////////////////////////////////////////////////////////////////////
template<class GView, class Collapser, class Refiner>
gpartition<GView> graph_partition(GView& gview, Collapser const& collapse,
                                  Refiner const& refine)
{
  return refine(collapse(gview));
}


//////////////////////////////////////////////////////////////////////
/// @brief Partition a graph with a collapser functor
/// @ingroup pgraphPartitioner
/// @param gview graph view to be partitioned
/// @param collapse collapser functor
/// @return gpartition object representing the partition
//////////////////////////////////////////////////////////////////////
template<class GView, class Collapser>
gpartition<GView> graph_partition(GView& gview, Collapser const& collapse)
{
  return collapse(gview);
}


//////////////////////////////////////////////////////////////////////
/// @brief Partition a partition with a collapser functor
/// @ingroup pgraphPartitioner
/// @param gpart partition object to be partitioned
/// @param collapse collapser functor
/// @return gpartition object representing the partition
//////////////////////////////////////////////////////////////////////
template<class GView, class Collapser>
gpartition<typename gpartition<GView>::partition_view_t>
graph_partition(gpartition<GView>& gpart, Collapser const& collapse)
{
  return collapse(gpart.partition());
}


//////////////////////////////////////////////////////////////////////
/// @brief Refine a partition with a refiner functor
/// @ingroup pgraphPartitioner
/// @param gpart partition object to be partitioned
/// @param refine refiner functor
/// @return gpartition object representing the partition
//////////////////////////////////////////////////////////////////////
template<class GView, class Refiner>
gpartition<GView>
graph_repartition(gpartition<GView>& gpart, Refiner const& refine)
{
  return refine(gpart);
}


} //end namespace stapl

#endif
