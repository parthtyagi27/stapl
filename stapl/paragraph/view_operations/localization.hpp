/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PARAGRAPH_LOCALIZATION_HPP
#define STAPL_PARAGRAPH_LOCALIZATION_HPP

#include <stapl/views/proxy/accessor.hpp>
#include <stapl/views/proxy/proxy.hpp>
#include <stapl/views/type_traits/is_view.hpp>

namespace stapl {

namespace detail {

//////////////////////////////////////////////////////////////////////
/// @brief Function object that returns true if localization type
///   transformation (i.e., fast_view_type) is safe to apply.
/// @ingroup pgViewOps
/// @sa create_task
///
/// Primary template matches any view parameter and calls the @p is_local
/// member if the specifier is not ready only.
//////////////////////////////////////////////////////////////////////
template<typename T, bool = is_view<T>::value>
struct localizer
{
  template<typename View>
  static bool apply(View const& view)
  { return view.is_local(); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization matches proxies and redirects @p is_local
/// queries to the underlying accessor.
/// @ingroup pgViewOps
//////////////////////////////////////////////////////////////////////
template<typename T, typename Accessor>
struct localizer<proxy<T, Accessor>, false>
{
  static bool apply(proxy<T, Accessor> const& p)
  { return accessor_core_access::is_local(proxy_core_access::accessor(p)); }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization matches non proxies or view types.  Return true.
/// @ingroup pgViewOps
//////////////////////////////////////////////////////////////////////
template<typename T>
struct localizer<T, false>
{
  template<typename View>
  static bool apply(View const& view)
  { return true; }
};

} // namespace detail

} // namespace stapl

#endif // STAPL_PARAGRAPH_LOCALIZATION_HPP
