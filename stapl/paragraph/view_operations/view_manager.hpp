/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PARAGRAPH_VIEW_MANAGER_HPP
#define STAPL_PARAGRAPH_VIEW_MANAGER_HPP

#include <stapl/paragraph/utility.hpp>
#include <stapl/utility/tuple.hpp>

namespace stapl {

namespace paragraph_impl {

//////////////////////////////////////////////////////////////////////
/// @brief This function object is responsible for applying the coarsener
///   functor to the views passed to the PARAGRAPH constructor.  The returned
///   tuple is the set of views stored by the PARAGRAPH and passed to the
///   factory.
/// @ingroup pgViewOps
/// @todo Codebase may be simple enough now to remove this and call coarsener
/// directly on initializer line of PARAGRAPH ctor.
//////////////////////////////////////////////////////////////////////
template<typename Coarsener, typename... Views>
struct view_manager
{
private:
  using views_t     = tuple<Views...>;

public:
  using result_type = typename std::result_of<Coarsener(views_t)>::type;

  static result_type apply(Coarsener const& coarsener, Views const&... views)
  {
    // RAII to set initializing flag inspected in result_view copy ctor.
    tg_initializer t;

    // Empty call, avoid compiler warnings...
    t.foo();

    pack_ops::functional::and_(views.validate()...);

    return coarsener(tuple<Views...>(views...));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Specialization for PARAGRAPH with no input views.  Returns an empty
///   tuple.
/// @ingroup pgViewOps
//////////////////////////////////////////////////////////////////////
template<typename Coarsener>
struct view_manager<Coarsener>
{
  using result_type = tuple<>;

  template<typename CoarseWF>
  static result_type apply(CoarseWF const&)
  {
    return result_type();
  }
};

} // namespace paragraph_impl

} // namespace stapl

#endif // ifndef STAPL_PARAGRAPH_VIEW_MANAGER_HPP
