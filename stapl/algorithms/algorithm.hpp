/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_ALGORITHMS_ALGORITHM_HPP
#define STAPL_ALGORITHMS_ALGORITHM_HPP

#include <numeric>
#include <stapl/paragraph/paragraph.hpp>
#include "generator.hpp"
#include "functional.hpp"
#include "numeric.hpp"
#include <boost/random/uniform_int_distribution.hpp>
#include <stapl/utility/hash.hpp>
#include <stapl/utility/random.hpp>
#include <stapl/views/proxy/proxy.hpp>
#include <stapl/views/overlap_view.hpp>
#include <stapl/views/balance_view.hpp>
#include <stapl/views/native_view.hpp>
#include <stapl/views/repeated_view.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/views/reverse_view.hpp>
#include <stapl/views/transform_view.hpp>
#include <stapl/views/type_traits/strip_fast_view.hpp>
#include <stapl/containers/partitions/splitter.hpp>
#include <stapl/views/functor_view.hpp>
#include <stapl/containers/generators/functor.hpp>
#include <stapl/containers/array/static_array.hpp>
#include <stapl/skeletons/executors/execute.hpp>
#include "algorithm_fwd.hpp"
#include "non_modifying.hpp"
#include "binary_search.hpp"
#include "minmax.hpp"

namespace stapl {

namespace algo_details {

//////////////////////////////////////////////////////////////////////
/// @brief Work function for @ref replace_if(), which assigns the stored value
///   to the argument if the predicate returns true.
/// @tparam Predicate Unary functor which is called on the argument.
//////////////////////////////////////////////////////////////////////
template<typename Predicate, typename T>
struct assign_if
{
  Predicate m_pred;
  T m_new_value;

  typedef void result_type;

  assign_if(Predicate const& pred,  T const& new_value)
    : m_pred(pred), m_new_value(new_value)
  { }

  void define_type(typer& t)
  {
    t.member(m_pred);
    t.member(m_new_value);
  }

  template<typename Reference1>
  void operator()(Reference1 x)
  {
    if (m_pred(x))
      x = m_new_value;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function which takes an input map and sets the given key equal
///   to the given value.
/// @tparam Value Type of the value to set.
/// @see merge
//////////////////////////////////////////////////////////////////////
template<typename Value>
struct merge_send_leader
{
  size_t m_sender;
  Value  m_val;

  merge_send_leader() : m_sender(0), m_val() {}

  merge_send_leader(size_t sender, Value val)
    : m_sender(sender)
    , m_val(val) {}

  template<typename Leaders>
  void operator()(Leaders& leaders) const {
    leaders[m_sender] = m_val;
  }

  void define_type(typer& t)
  {
    t.member(m_sender);
    t.member(m_val);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function which merges two local splitters into the global
///   set of splitters.
/// @see merge
//////////////////////////////////////////////////////////////////////
class merge_splitters
{
private:
  location_type m_me;

public:
  typedef void result_type;

  merge_splitters(location_type me)
    : m_me(me)
  { }

  template<typename LocalView, typename LocalSplitsA, typename LocalSplitsB,
           typename GlobalIndices>
  void operator()(LocalView A, LocalSplitsA locA, LocalSplitsB locB,
                  GlobalIndices globAix)
  {
    //note that locA/locB are assumed to be std::vectors
    // and globAix is assumed to be a static_array
    typedef typename LocalSplitsA::iterator     locAIter;
    typedef typename LocalSplitsB::iterator     locBIter;

    //find the corresponding bounds in the splitters of B
    locAIter mystart = locA.begin() + m_me;
    locBIter c = std::lower_bound(locB.begin(), locB.end(), *(mystart));
    locBIter d = (mystart+1 != locA.end())
                ? std::lower_bound(locB.begin(), locB.end(), *(mystart+1))
                : locB.end();

    //there are me splitters before this (from other partitions)
    // there are dist(c) splitters before this (from locB)
    size_t ix = std::distance(locB.begin(), c) + m_me;
    globAix[ix] = A.domain().first();

    for (++ix; c != d; ++c, ++ix) {
      globAix[ix] =
        A.domain().advance(A.domain().first(),
                           std::distance(A.begin(),
                                         std::upper_bound(A.begin(), A.end(),
                                                          *c)));
    }
  }

  void define_type(typer& t)
  {
    t.member(m_me);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function which sets the third argument equal to the sums of the
///   sizes of the two input views.
/// @see merge
//////////////////////////////////////////////////////////////////////
struct merge_output_sizes
{
  typedef void result_type;

  template<typename View1, typename View2, typename SizeType>
  void operator()(View1 const& view1, View2 const& view2, SizeType size)
  {
    size = view1.size() + view2.size();
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Compute the index of the nth element of the provided view.
/// @see merge
//////////////////////////////////////////////////////////////////////
struct merge_output_indices
{
  typedef void result_type;

  template<typename Size, typename Index, typename View>
  void operator()(Size const& size, Index ix, View const& view)
  {
    ix = view.domain().advance(view.domain().first(), size);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function which merges the two input views into the output view.
/// @see merge
//////////////////////////////////////////////////////////////////////
struct merge_map
{
  typedef void result_type;

  template<typename View1, typename View2, typename MergeView>
  void operator()(View1 const& view1, View2 const& view2, MergeView merged)
  {
    std::merge(view1.begin(), view1.end(), view2.begin(), view2.end(),
               merged.begin());
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Helper function to invoke @ref coarse_assign on source and
/// destination view.  The source view's domain is restricted with the
/// @p first_read and @p last_read offsets.  The destination view's
/// domain is restricted to begin with @p write_offset and span enough
/// elements to match the size of the read domain
/// (ie., last_read - first_read).
//////////////////////////////////////////////////////////////////////
template<typename SrcView, typename DstView>
void
set_elements(SrcView&& src_vw, DstView&& dst_vw,
             size_t first_read, size_t last_read, size_t write_offset)
{
  // SrcView is expected to be a stapl::mix_view instance. We need the
  // underlying type in order to construct the view for the set_elements call.
  using src_view_t =
    typename detail::strip_mix_view<
      typename detail::strip_fast_view<
        typename std::decay<SrcView>::type>::type
    >::type;

  using src_dom_t  = typename src_view_t::domain_type;

  using dst_view_t =
    typename detail::strip_repeat_view<
      typename detail::strip_mix_view<
        typename detail::strip_fast_view<
          typename std::decay<DstView>::type
        >::type
      >::type
    >::type;

  using dst_dom_t = typename dst_view_t::domain_type;

  auto src_first =
    src_vw.domain().advance(src_vw.domain().first(), first_read);
  auto src_last  =
    src_vw.domain().advance(src_first, last_read - first_read);

  auto dst_first =
    dst_vw.domain().advance(dst_vw.domain().first(), write_offset);
  auto dst_last  =
     dst_vw.domain().advance(dst_first, last_read - first_read);

  // Views to use for coarse assignment, domains must be set before use.
  src_view_t src_view(src_vw.container(), src_dom_t(src_first, src_last));
  dst_view_t dst_view(dst_vw.container(), dst_dom_t(dst_first, dst_last));

  coarse_assign()(src_view, dst_view);
}


//////////////////////////////////////////////////////////////////////
/// @brief Work function which implements a lexicographical ordering based on
///   the provided functor.
/// @tparam Pred Binary functor implementing the less operation.
//////////////////////////////////////////////////////////////////////
template<typename Pred>
class lexico_compare
{
private:
  Pred m_pred;

public:
  lexico_compare(Pred const& pred)
    : m_pred(pred) {}

  void define_type(typer& t)
  {
    t.member(m_pred);
  }

  typedef int result_type;

  template <typename Elm1, typename Elm2>
  int operator()(Elm1 element1, Elm2 element2) const
  {
    if (m_pred(element1,element2))
      return 1;
    else if (m_pred(element2,element1))
      return -1;

    return 0;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function which returns the second argument if the first
///   compares equal to 0, and the first otherwise.
/// @tparam T The type of the arguments.
//////////////////////////////////////////////////////////////////////
template <typename T>
struct lexico_reduce
  : public ro_binary_function<T, T, T>
{
  template <typename Elm>
  T operator()(Elm elem1, Elm elem2){
    if (elem1 == 0){
      return (T)elem2;
    } else {
      return (T)elem1;
    }
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Copies the first argument to the
///   second unless the first meets the predicate provided, in which case
///   the given value is copied to the second argument.
/// @tparam Predicate Unary functor to test for the need for replacement.
/// @tparam T Type of the value to replace.
//////////////////////////////////////////////////////////////////////
template<typename Predicate, typename T>
struct copy_with_replacement
{
  Predicate m_pred;
  T m_new_value;

  typedef void result_type;

  copy_with_replacement(Predicate const& pred, T const& new_value)
    : m_pred(pred), m_new_value(new_value)
  { }

  template<typename Reference1, typename Reference2>
  void operator()(Reference1 x, Reference2 y)
  {
    if (m_pred(x))
      y = m_new_value;
    else
      y = x;
  }

  void define_type(typer& t)
  {
    t.member(m_pred);
    t.member(m_new_value);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief Work function for @ref is_sorted() which takes a reference to
///   two elements, and returns true if the given functor returns false when
///   called with (second, first).
/// @tparam Functor Binary functor which is used to compare the elements.
//////////////////////////////////////////////////////////////////////
template<typename Functor>
class gen_comp_func
  : public ro_unary_function<typename Functor::first_argument_type,
                             typename Functor::result_type>
{
private:
  Functor m_f;

public:
  gen_comp_func(Functor const& f)
    : m_f(f)
  { }

  template<typename Ref1, typename Ref2>
  typename Functor::result_type
  operator()(Ref1&& r1, Ref2&& r2) const
  {
    return (!m_f(r2, r1));
  }

  void define_type(typer &t)
  {
    t.member(m_f);
  }

};


//////////////////////////////////////////////////////////////////////
/// @brief Helper function for @ref copy implementation that enables
/// use of coarsened assigment function using aggregate set_elements
/// when iterators are available (denoted by std::true_type parameter).
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1>
void copy_impl(std::true_type, View0 const& vw0, View1 const& vw1)
{
  map_func<skeletons::tags::with_coarsened_wf>(coarse_assign(), vw0, vw1);
}


//////////////////////////////////////////////////////////////////////
/// @brief Helper function for @ref copy implementation that uses
/// default, per element assignment function when iterators are not
/// available (denoted by std::false_type parameter).
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1>
void copy_impl(std::false_type, View0 const& vw0, View1 const& vw1)
{
  map_func(assign<typename View1::value_type>(), vw0, vw1);
}

} //namespace algo_details


//////////////////////////////////////////////////////////////////////
/// @brief Copy the elements of the input view to the output view.
/// @param vw0 One-dimensional view of the input.
/// @param vw1 One-dimensional view of the output.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1>
void copy(View0 const& vw0, View1 const& vw1)
{
  algo_details::copy_impl(
    skeletons::optimizers::helpers::pack_has_iterator<View0, View1>(),
    vw0, vw1);
}


//////////////////////////////////////////////////////////////////////
/// @brief Copy the first n elements from the input view to the output view.
/// @param vw0 One-dimensional view of the input.
/// @param vw1 One-dimensional view of the output.
/// @param n Number of elements to copy.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1, typename Size>
void copy_n(View0 const& vw0, View1 const& vw1, Size n)
{
  stapl_assert(n >= 0, "requested copy of negative number of elements");

  if (n == 0)
    return;

  typedef typename View0::domain_type dom_t;
  typedef typename View1::domain_type dom_t2;
  copy(
    View0(vw0.container(),
          dom_t(vw0.domain().first(),
                vw0.domain().advance(vw0.domain().first(),n-1))),
    View1(vw1.container(),
          dom_t2(vw1.domain().first(),
                 vw1.domain().advance(vw1.domain().first(),n-1))));
}


//////////////////////////////////////////////////////////////////////
/// @brief Determines if the first view is lexicographically less than the
///   second view, using the given functor.
/// @param pview1 One-dimensional view of the first input.
/// @param pview2 One-dimensional view of the second input.
/// @param pred Binary functor which implements the less operation.
/// @return True if the first input is lexicographically less than the second.
/// @ingroup sortrelatedAlgorithms
///
/// This algorithm is non-mutating.
////////////////////////////////////////////////////////////////////
template <typename View, typename View2, typename Pred>
bool lexicographical_compare(View const& pview1, View2 const& pview2,
                             Pred const& pred)
{
  int minelem = 0;
  int result;
  int size1 = pview1.size();
  int size2 = pview2.size();
  if (size1 > size2){
    minelem = size2;
    View viewtrunc(pview1.container(), typename View::domain_type(0,minelem-1));
    result = stapl::map_reduce(algo_details::lexico_compare<Pred>(pred),
                               algo_details::lexico_reduce<int>(),
                               viewtrunc, pview2);
  }
  else if (size1 < size2) {
    minelem = size1;
    View2 viewtrunc(pview2.container(),typename View::domain_type(0,minelem-1));
    result = stapl::map_reduce(algo_details::lexico_compare<Pred>(pred),
                               algo_details::lexico_reduce<int>(),
                               pview1, viewtrunc);
  }
  else {
    result = stapl::map_reduce(algo_details::lexico_compare<Pred>(pred),
                               algo_details::lexico_reduce<int>(),
                               pview1, pview2);
  }

  if (result == 1){
    return true;
  }
  else if (result == -1){
    return false;
  }
  return size1<size2;
}


//////////////////////////////////////////////////////////////////////
/// @brief Determines if the first view is lexicographically less than the
///   second view.
/// @param pview1 One-dimensional view of the first input.
/// @param pview2 One-dimensional view of the second input.
/// @return True if the first input is lexicographically less than the second.
/// @ingroup sortrelatedAlgorithms
///
/// This version calls the predicated version with a default predicate of
/// stapl::less.
////////////////////////////////////////////////////////////////////
template <typename View, typename View2>
bool lexicographical_compare(View const& pview1, View2 const& pview2)
{
  return lexicographical_compare(pview1, pview2,
                                 less<typename View::value_type>());
}


//////////////////////////////////////////////////////////////////////
/// @brief Assign each value of the input view to the result of calling the
///   provided functor.
/// @param view One-dimensional view of the input.
/// @param gen Nullary functor which is called to generate elements.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view.
//////////////////////////////////////////////////////////////////////
template<typename View, typename Generator>
void generate(View const& view, Generator gen)
{
  typedef typename View::value_type             value_t;
  copy(functor_view(view.size(),
                    offset_gen<std::size_t,value_t,Generator>(gen)),
       view);
}


//////////////////////////////////////////////////////////////////////
/// @brief Assign the n values of the input view starting at the given element
///   to the result of calling the provided functor.
/// @param view One-dimensional view of the input.
/// @param first_elem First element to fill with a generated value.
/// @param n Number of elements to fill with generated values.
/// @param gen Nullary functor which is called to generate elements.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view.
//////////////////////////////////////////////////////////////////////
template <typename View, typename Generator>
void generate_n(View const& view, size_t first_elem, size_t n, Generator gen)
{
  typedef typename View::domain_type dom_t;
  dom_t dom = view.domain();
  dom_t new_dom(dom.advance(dom.first(),first_elem),
                dom.advance(dom.first(),first_elem + n - 1), dom);
  View view2(view.container(), new_dom);
  generate(view2, gen);
}


//////////////////////////////////////////////////////////////////////
/// @brief Replace the values from the input view for which the given predicate
///   returns true with the new value.
/// @param vw One-dimensional view of the input.
/// @param pred Unary functor which returns true for replaced elements.
/// @param new_value Value used to replace elements.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view.
//////////////////////////////////////////////////////////////////////
template<typename View, typename Predicate>
void replace_if(View& vw, Predicate pred,
                typename View::value_type const& new_value)
{
  stapl::map_func(algo_details::assign_if<Predicate, typename View::value_type>
                   (pred, new_value), vw);
}


//////////////////////////////////////////////////////////////////////
/// @brief Replace the given value in the input with the new value.
/// @param vw One-dimensional view of the input.
/// @param old_value Value replaced in the input.
/// @param new_value Value used to replace old values.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view. The comparison is done with
/// stapl::equal_to.
///
/// @todo Track down why std::bind1st seems to be seeping into stapl namespace
/// with icc compiler and requiring explicit qualification of stapl::bind1st.
//////////////////////////////////////////////////////////////////////
template<typename View>
void replace(View& vw, typename View::value_type const& old_value,
             typename View::value_type const& new_value)
{
  replace_if(vw,
             stapl::bind2nd(equal_to<typename View::value_type>(),old_value),
             new_value);
}


//////////////////////////////////////////////////////////////////////
/// @brief Copy the values from the input view to the output, except for those
///   elements for which the given predicate returns true, which are replaced
///   with the given value.
/// @param vw0 One-dimensional view of the input.
/// @param vw1 One-dimensional view of the output.
/// @param pred Unary functor which returns true for replaced elements.
/// @param new_value Value used to replace elements for which the functor
///   returns true.
/// @return Iterator pointing to the end of the output view.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output view. The input and output views must be
/// the same size.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1, typename Predicate>
typename View1::iterator
replace_copy_if(View0 const& vw0, View1 const& vw1, Predicate pred,
                typename View0::value_type new_value)
{
  stapl::map_func(
      algo_details::copy_with_replacement<Predicate, typename View0::value_type>
        (pred, new_value), vw0, vw1);
  return vw1.end();
}


//////////////////////////////////////////////////////////////////////
/// @brief Assigns the given value to the first n elements of the input view.
/// @param vw One-dimensional view of the input.
/// @param value The value to fill into the input.
/// @param n Number of elements to fill.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view. The input must be at least n in size.
///
/// @todo Track down why std::bind1st seems to be seeping into stapl namespace
/// with icc compiler and requiring explicit qualification of stapl::bind1st.
//////////////////////////////////////////////////////////////////////
template<typename View, typename Size>
void fill_n(View& vw, typename View::value_type value, Size n)
{
  View nview(vw.container(),
             typename View::domain_type(vw.domain().first(),
                                        vw.domain().first() + n - 1));
  stapl::map_func(stapl::bind1st(assign<typename View::value_type>(), value),
                  nview);
}

//////////////////////////////////////////////////////////////////////
/// @brief Assigns the given value to the elements of the input view.
/// @param vw One-dimensional view of the input.
/// @param value The value to fill into the input.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the input view.
///
/// @todo Track down why std::bind1st seems to be seeping into stapl namespace
/// with icc compiler and requiring explicit qualification of stapl::bind1st.
//////////////////////////////////////////////////////////////////////
template<typename View>
void fill(View const& vw, typename View::value_type value)
{
  stapl::map_func(stapl::bind1st(assign<typename View::value_type>(), value),
                  vw);
}


//////////////////////////////////////////////////////////////////////
/// @brief Merges the two sorted input views into the output view in sorted
///   order.
/// @param view1 One-dimensional view of the first sorted input.
/// @param view2 One-dimensional view of the second sorted input.
/// @param merged One-dimensional view of the output.
/// @ingroup sortrelatedAlgorithms
///
/// This algorithm mutates the output.
//////////////////////////////////////////////////////////////////////
template<typename View1, typename View2, typename MergeView>
void merge(View1 const& view1, View2 const& view2, MergeView& merged)
{
  //TYPEDEFS:

  //Balanced Partitionings
  typedef typename View1::domain_type               dom1_t;
  typedef balanced_partition<dom1_t>                balanced_part1_t;
  typedef segmented_view<View1, balanced_part1_t> balanced1_t;

  typedef typename View2::domain_type               dom2_t;
  typedef balanced_partition<dom2_t>                balanced_part2_t;
  typedef segmented_view<View2, balanced_part2_t> balanced2_t;

  typedef typename MergeView::domain_type           domM_t;

  //Distribution of Local Splitters
  typedef typename View1::value_type          val1_t;
  typedef std::vector<val1_t>                 vals1_t;
  typedef static_array<vals1_t>               vals_array1_t;
  typedef array_view<vals_array1_t>           vals_array_view1_t;

  typedef typename View2::value_type          val2_t;
  typedef std::vector<val2_t>                 vals2_t;
  typedef static_array<vals2_t>               vals_array2_t;
  typedef array_view<vals_array2_t>           vals_array_view2_t;

  //Local Local Splitters
  typedef algo_details::merge_send_leader<val1_t>   send_leader1;

  typedef algo_details::merge_send_leader<val2_t>   send_leader2;

  //Splitters (Indices)
  typedef typename View1::index_type                  index1_t;
  typedef static_array<index1_t>                      indices1_t;
  typedef array_view<indices1_t>                      indices1_view_t;

  typedef typename View2::index_type                  index2_t;
  typedef static_array<index2_t>                      indices2_t;
  typedef array_view<indices2_t>                      indices2_view_t;

  typedef typename MergeView::index_type              indexM_t;
  typedef static_array<indexM_t>                      indicesM_t;
  typedef array_view<indicesM_t>                      indicesM_view_t;

  //Segmented Partitionings
  typedef splitter_partition<dom1_t, indices1_view_t> split_part1_t;
  typedef segmented_view<View1, split_part1_t>      split1_t;

  typedef splitter_partition<dom2_t, indices2_view_t> split_part2_t;
  typedef segmented_view<View2, split_part2_t>      split2_t;

  typedef splitter_partition<domM_t, indicesM_view_t> split_partM_t;
  typedef segmented_view<MergeView, split_partM_t>  splitM_t;

  //Sizes of Each Partition
  typedef static_array<size_t>                        sizes_t;
  typedef array_view<sizes_t>                         sizes_view_t;
  typedef typename sizes_view_t::domain_type          sizes_domain_t;

  //ALGORITHM:

  size_t num_locs = get_num_locations();
  size_t my_part = get_location_id();
  size_t num_split = num_locs*2;

  //partition views naturally
  balanced_part1_t    balanced_part1(view1.domain(), num_locs);
  balanced1_t         balanced1(view1, balanced_part1);
  balanced_part2_t    balanced_part2(view2.domain(), num_locs);
  balanced2_t         balanced2(view2, balanced_part2);

  //copy the list of leaders to each location
  //FIXME: use a repeated view of the leaders, not a view of vectors
  vals_array1_t       loc1array(num_locs, vals1_t(num_locs));
  vals_array_view1_t  loc1view(loc1array);
  send_leader1 send_func1(my_part,
                 balanced1[my_part][balanced1[my_part].domain().first()]);

  vals_array2_t       loc2array(num_locs, vals2_t(num_locs));
  vals_array_view2_t  loc2view(loc2array);
  send_leader2 send_func2(my_part,
                 balanced2[my_part][balanced2[my_part].domain().first()]);

  for (size_t i=0; i<num_locs; ++i) {
    loc1view.apply_set(i, send_func1);
    loc2view.apply_set(i, send_func2);
  }
  rmi_fence();

  //find the indices
  indices1_t        indices1(num_split, view1.domain().first());
  indices1_view_t   indices1_view(indices1);
  indices2_t        indices2(num_split, view2.domain().first());
  indices2_view_t   indices2_view(indices2);
  map_func(algo_details::merge_splitters(get_location_id()),
           balanced1, loc1view, loc2view, make_repeat_view(indices1_view));
  map_func(algo_details::merge_splitters(get_location_id()),
           balanced2, loc2view, loc1view, make_repeat_view(indices2_view));

  //partition view2 (so that ranges are same as view1)
  split_part1_t   split_part1(view1.domain(), indices1_view, true);
  split1_t        split1(view1, split_part1);

  split_part2_t   split_part2(view2.domain(), indices2_view, true);
  split2_t        split2(view2, split_part2);

  //TODO: combining merge_output_sizes, partial_sum, and merge_output_indices
  //      could reduce the time significantly on domains where advance() does
  //      not work in constant time

  //find partition on merged by finding sizes and advancing through the domain
  sizes_t       out_sizes(num_split+1);
  sizes_view_t  out_sizev(out_sizes);

  map_func(algo_details::merge_output_sizes(), split1, split2, out_sizev);

  //remove the last size to attain splitters
  sizes_domain_t cut_dom(0, num_split-1);
  out_sizev.set_domain(cut_dom);

  partial_sum(out_sizev, out_sizev);

  indicesM_t        ixMs(num_split);
  indicesM_view_t   ixMv(ixMs);
  map_func(algo_details::merge_output_indices(),
           out_sizev, ixMv, make_repeat_view(merged));

  split_partM_t split_partM(merged.domain(), ixMv, true);
  splitM_t      splitM(merged, split_partM);

  //merge individual pieces with std::merge
  map_func(algo_details::merge_map(), split1, split2, splitM);

  rmi_fence();
}


//////////////////////////////////////////////////////////////////////
/// @brief Swaps the elements of the two input views.
/// @param vw0 One-dimensional view of the first input.
/// @param vw1 One-dimensional view of the second input.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates both input views, and requires that both views are
/// the same size.
//////////////////////////////////////////////////////////////////////
template<typename View>
void swap_ranges(View& vw0, View& vw1)
{
  stapl::map_func<skeletons::tags::with_coarsened_wf>(
       coarse_swap_ranges<typename View::value_type>(),vw0,vw1);
}


//////////////////////////////////////////////////////////////////////
/// @brief Copy the elements from the input to the output, replacing the given
///   old_value with the new_value.
/// @param vw0 One-dimensional view of the input.
/// @param vw1 One-dimensional view of the output.
/// @param old_value The old value to replace.
/// @param new_value The new value to substitute for occurrences of old_value.
/// @return Iterator to the end of the newly copied view.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output view, and requires a view with iterator
/// support. It uses stapl::equal_to for comparisons.
///
/// @todo Track down why std::bind1st seems to be seeping into stapl namespace
/// with icc compiler and requiring explicit qualification of stapl::bind1st.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1>
typename View1::iterator
replace_copy(View0& vw0, View1& vw1, typename View0::value_type old_value,
             typename View0::value_type new_value)
{
  replace_copy_if(vw0, vw1,
                  stapl::bind2nd(equal_to<typename View0::value_type>(),
                                 old_value),
                   new_value);
  return vw1.end();
}


//////////////////////////////////////////////////////////////////////
/// @brief Applies the given functor to all of the elements in the input.
/// @param vw0 One-dimensional view over the input.
/// @param func Unary functor to apply to the elements.
/// @return The functor that was passed as input.
/// @ingroup generatingAlgorithms
///
/// This algorithm will mutate the input view.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename Function>
Function
for_each(const View0& vw0, Function func)
{
  stapl::map_func(apply_functor<Function>(func), vw0);
  return func;
}


//////////////////////////////////////////////////////////////////////
/// @brief Applies the given function to the input, and stores the result in
///   the output.
/// @param vw0 One-dimensional view over the input.
/// @param vw1 One-dimensional view over the output.
/// @param func Unary function which is applied to all of the input elements.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output view only.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1, typename Function>
void transform(const View0& vw0, const View1& vw1, Function func)
{
  stapl::map_func(transform_assign<Function>(func), vw0, vw1);
}


//////////////////////////////////////////////////////////////////////
/// @brief Applies the given function to the inputs, and stores the result in
///   the output.
/// @param vw0 One-dimensional view over the first input.
/// @param vw1 One-dimensional view over the second input.
/// @param vw2 One-dimensional view over the output.
/// @param func Binary function which is applied to all of the input elements.
/// @ingroup generatingAlgorithms
///
/// This algorithm mutates the output view only.
//////////////////////////////////////////////////////////////////////
template<typename View0, typename View1, typename View2, typename Function>
void transform(View0& vw0, View1& vw1, View2& vw2, Function func)
{
  stapl::map_func(transform_assign<Function>(func), vw0, vw1, vw2);
}


//////////////////////////////////////////////////////////////////////
/// @brief Computes whether the input view is sorted.
/// @param view One-dimensional view of the input.
/// @param comp The strict weak ordering comparison functor.
/// @return True if the input is sorted, false otherwise.
/// @ingroup sortrelatedAlgorithms
///
/// This algorithm is non-mutating.
//////////////////////////////////////////////////////////////////////
template <typename View, typename Comp>
bool is_sorted(View const& view, Comp comp)
{
  using dom_t = typename View::domain_type;

  View view_ant(view.container(),
      dom_t(view.domain().first(),
        view.domain().advance(
          view.domain().first(), view.size() - 2),
        view.domain()));

  View view_post(view.container(),
      dom_t(view.domain().advance(view.domain().first(), 1),
        view.domain().last(), view.domain()));

  return stapl::map_reduce(algo_details::gen_comp_func<Comp>(comp),
                           logical_and<bool>(),
                           view_ant,
                           view_post);
}


//////////////////////////////////////////////////////////////////////
/// @brief Computes whether the input view is sorted.
/// @param view One-dimensional view of the input.
/// @return True if the input is sorted, false otherwise.
/// @ingroup sortrelatedAlgorithms
///
/// This version calls the predicated version with a default predicate of
/// stapl::less.
//////////////////////////////////////////////////////////////////////
template<typename View>
bool is_sorted(View const& view)
{
  return is_sorted(view,less<typename View::value_type>());
}


namespace algo_details {

//////////////////////////////////////////////////////////////////////
/// @brief Work function for @ref is_sorted_until(), which returns false if the
///   given elements are out of order, and true otherwise.
/// @tparam View Type of the input view.
/// @tparam Comp Binary functor implementing the less operation.
//////////////////////////////////////////////////////////////////////
template<typename View, typename Comp>
class is_sorted_until_compare
{
private:
  Comp m_comp;

public:
  is_sorted_until_compare(Comp const& c)
    : m_comp(c)
  { }

  void define_type(typer& t)
  {
    t.member(m_comp);
  }

  using pair_type   = std::pair<bool, typename View::index_type>;
  using result_type = pair_type;

  template <typename Value1, typename Value2>
  pair_type operator()(Value1&& v1, Value2&& v2) const
  {
    if (!m_comp(v2, v1))
      return pair_type(true, typename View::index_type());
    else
      // TODO: Isn't it preferred to use a counting view here?
      return pair_type(false, index_of(v1));
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Work function for @ref is_sorted_until(), which returns the first
///   argument if it is not sorted, and otherwise returns the second.
/// @tparam View Type of the input view.
//////////////////////////////////////////////////////////////////////
template <typename View>
struct is_sorted_until_reduce
{
  typedef std::pair<bool, typename View::index_type>  pair_type;
  typedef pair_type result_type;

  pair_type operator()(pair_type elem1, pair_type elem2)
  {
    if (!elem1.first)
    {
      return elem1;
    } else {
      return elem2;
    }
  }
};

}

//////////////////////////////////////////////////////////////////////
/// @brief Finds the range of elements in the input which are sorted.
/// @param v One-dimensional view of the input.
/// @param c The strict weak ordering comparison functor.
/// @return A view over the sorted range.
/// @ingroup sortrelatedAlgorithms
///
/// This algorithm is non-mutating.
//////////////////////////////////////////////////////////////////////
template <typename View, typename Comp>
View is_sorted_until(View const& v, Comp const& c)
{
  using pair_type = std::pair<bool, typename View::index_type>;
  using dom_t     = typename View::domain_type;

  View view_ant(v.container(),
      dom_t(v.domain().first(),
        v.domain().advance(
          v.domain().first(), v.size() - 2),
        v.domain()));

  View view_post(v.container(),
      dom_t(v.domain().advance(v.domain().first(), 1),
        v.domain().last(), v.domain()));

  pair_type result = stapl::map_reduce(
      algo_details::is_sorted_until_compare<View, Comp>(c),
      algo_details::is_sorted_until_reduce<View>(),
      view_ant, view_post);

  if (result.first)
    return v;

  View view(v.container(), dom_t(0, result.second));

  return view;
}

//////////////////////////////////////////////////////////////////////
/// @brief Finds the range of elements in the input which are sorted.
/// @param v One-dimensional view of the input.
/// @return A view over the sorted range.
/// @ingroup sortrelatedAlgorithms
///
/// This version calls the predicated version with a default predicate of
/// stapl::less.
//////////////////////////////////////////////////////////////////////
template <typename View>
View is_sorted_until(View const& v)
{
  less<typename View::value_type> comp;
  return is_sorted_until(v,comp);
}


namespace algo_details {

//////////////////////////////////////////////////////////////////////
/// @brief Convert boolean return value of partition predicate passed to
///   @ref is_partitioned() to int, so that it is in the tribool form
///   expected by the @ref is_partitioned_reduce reduction operator
///   of the @ref stapl::map_reduce.
//////////////////////////////////////////////////////////////////////
template<typename WF>
class convert_to_int
{
private:
  WF m_wf;

public:
  convert_to_int(WF const& wf)
    : m_wf(wf)
  { }

  typedef int result_type;

  template<typename View>
  int operator()(View&& elem) const
  {
    auto result = std::is_partitioned(elem.begin(), elem.end(), m_wf);

    // If this set of elements is partitioned then the result of the predicate
    // on the last element is converted for the reduction operator.
    // Otherwise, return 2 to indicate that a non-partitioned set of elements
    // has been found.
    if (result)
      return m_wf(*std::prev(elem.end()));
    else
      return 2;
  }

  void define_type(typer& t)
  {
    t.member(m_wf);
  }
}; // class convert_to_int


//////////////////////////////////////////////////////////////////////
/// @brief Work function used in @ref is_partitioned() to determine whether the
///   input is partitioned.
//////////////////////////////////////////////////////////////////////
template<typename T>
struct is_partitioned_reduce
{
  typedef int result_type;

  template<typename Reference1, typename Reference2>
  int operator()(Reference1&& elem1, Reference2&& elem2) const
  {
    if (elem1 == 1 && elem2 == 1)
      return 1;

    if (elem1 == 1 && elem2 == 0)
      return 0;

    if (elem1 == 0 && elem2 == 0)
      return 0;

    if (elem1 == 0 && elem2 == 1)
      return 2;

    if (elem1 == 2 || elem2 == 2)
      return 2;

    // else
    return 2;
  }
}; // struct is_partitioned_reduce

} // namespace algo_details


//////////////////////////////////////////////////////////////////////
/// @brief Decides if the input view is partitioned according to the given
///   functor, in that all elements which return true precede all those that do
///   not.
/// @param pview One-dimensional view of the input.
/// @param predicate Unary functor used to check the partitioning.
/// @return True if the input is partitioned, false otherwise.
/// @ingroup summaryAlgorithms
///
/// This algorithm is non-mutating.
//////////////////////////////////////////////////////////////////////
template <typename View, typename Predicate>
bool
is_partitioned(View const& pview, Predicate predicate)
{
  const int result = map_reduce<skeletons::tags::with_coarsened_wf>(
    algo_details::convert_to_int<Predicate>(predicate),
    algo_details::is_partitioned_reduce<int>(),
    pview
  );

  if (result == 0 || result == 1)
    return true;

  // else
 return false;
}

} //namespace stapl

#endif

