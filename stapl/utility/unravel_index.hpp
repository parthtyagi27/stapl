/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_UNRAVEL_INDEX_HPP
#define STAPL_UTILITY_UNRAVEL_INDEX_HPP

#include <algorithm>
#include <numeric>
#include <deque>
#include <vector>

//////////////////////////////////////////////////////////////////////
/// @brief Convert a linear index to a multidimensional index where the
///        dimensionality is not known statically. This is based on
///        the unravel_index function in the NumPy package.
///
/// @param index Linear index in the space
/// @param shape Sizes of each dimension
/// @return An n-dimensional index
//////////////////////////////////////////////////////////////////////
std::vector<std::size_t> unravel_index(std::size_t index,
                                       std::vector<std::size_t> const& shape)
{
  std::deque<std::size_t> sizes(shape.begin(), shape.end());

  // Drop first element and reverse
  sizes.pop_front();
  std::reverse(std::begin(sizes), std::end(sizes));

  // Prepend 1
  sizes.push_front(1);

  // Calculate cummulative product
  std::partial_sum(std::begin(sizes), std::end(sizes), std::begin(sizes),
    std::multiplies<std::size_t>());

  // Reverse
  std::reverse(std::begin(sizes), std::end(sizes));

  // For each dim, divide by plane size and mod by dim size
  std::vector<std::size_t> unravelled(sizes.begin(), sizes.end());
  std::transform(
    std::begin(unravelled), std::end(unravelled), std::begin(shape),
    std::begin(unravelled), [&](std::size_t size, std::size_t shp) {
      return (index/size) % shp;
  });

  return unravelled;
}

#endif
