/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_TUPLE_SIZE_HPP
#define STAPL_UTILITY_TUPLE_TUPLE_SIZE_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <type_traits>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Wrap std::tuple_size and fix deficiency in libstdc++, which
///   doesn't remove cv qualifications.
/// @ingroup Tuple
///
/// This wrapper also reflects value_type and type, and implements
/// value_type().  All of these interfaces are specified in the standard,
/// but are not yet provided by libstd++.
///
/// @todo Monitor https://svn.boost.org/trac/boost/ticket/7192 to
/// determine when we can use std::integral_constant.
//////////////////////////////////////////////////////////////////////
template<typename Tuple>
struct tuple_size
  : public std::tuple_size<
      typename std::remove_cv<Tuple>::type
    >
{
  using value_type = std::size_t;
  using type = std::integral_constant<
                 std::size_t,
                 std::tuple_size<typename std::remove_cv<Tuple>::type>::value>;

  operator std::size_t()
  {
    return std::tuple_size<typename std::remove_cv<Tuple>::type>::value;
  }
};

} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_TUPLE_SIZE_HPP
