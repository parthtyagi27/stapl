/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_TUPLE_CONTAINS_HPP
#define STAPL_UTILITY_TUPLE_TUPLE_CONTAINS_HPP

#include <type_traits>
#include <stapl/utility/tuple/tuple.hpp>

namespace stapl {
namespace tuple_ops {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that computes whether an integral type
///        appears in a tuple of integral types
///
/// @tparam T The type to search for
/// @tparam Tuple The tuple to search in
//////////////////////////////////////////////////////////////////////
template <typename T, typename Tuple>
struct tuple_contains;

template <typename T, typename Head, typename... Tail>
struct tuple_contains<T, tuple<Head, Tail...>>
 : public std::integral_constant<bool, T::value == Head::value ||
            tuple_contains<T, tuple<Tail...>>::value>
{ };

template<typename T>
struct tuple_contains<T, tuple<>>
 : public std::false_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that computes if a type appears in a tuple
///
/// @tparam T The type to search for
/// @tparam Tuple The tuple to search in
//////////////////////////////////////////////////////////////////////
template <typename T, typename Tuple>
struct tuple_contains_type;

template <typename T, typename... Tail>
struct tuple_contains_type<T, tuple<T, Tail...>>
  : public std::true_type
{ };

template <typename T, typename Head, typename... Tail>
struct tuple_contains_type<T, tuple<Head, Tail...>>
  : public tuple_contains_type<T, tuple<Tail...>>
{ };

template<typename T>
struct tuple_contains_type<T, tuple<>>
  : public std::false_type
{ };

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that computes if a type does not appear in a tuple
///
/// @tparam T The type to search for
/// @tparam Tuple The tuple to search in
//////////////////////////////////////////////////////////////////////
template<typename T, typename Tuple>
struct not_tuple_contains_type :
  std::integral_constant<bool,
    not tuple_contains_type<T, Tuple>::value>
{ };

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_TUPLE_CONTAINS_HPP
