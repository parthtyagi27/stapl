/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_TUPLE_FILTER_HPP
#define STAPL_UTILITY_TUPLE_FILTER_HPP

#include <type_traits>
#include <stapl/utility/tuple/tuple_size.hpp>
#include <stapl/utility/tuple/tuple_element.hpp>
#include <stapl/utility/tuple/discard.hpp>
#include <stapl/utility/tuple/homogeneous_tuple.hpp>

namespace stapl {
namespace tuple_ops {

namespace result_of {

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that reflects the type of the @see stapl::filter
///        function.
//////////////////////////////////////////////////////////////////////
template<typename Slices, typename Tuple>
struct filter
{
  using result_size_t = typename stapl::tuple_size<Slices>::type ;
  using scalar_t      = typename stapl::tuple_element<
                          0, typename std::decay<Tuple>::type>::type;

  // the result type should be a tuple of size |Slices|.
  // but if this value is 1, we should just use a scalar instead
  // of a tuple of size 1
  using type = typename std::conditional<
    result_size_t::value == 1,          // if
    scalar_t,                           // then
    typename homogeneous_tuple_type<    // else
     result_size_t::value, scalar_t
    >::type
  >::type;
};

//////////////////////////////////////////////////////////////////////
/// @brief Metafunction that filters a d-dimensional tuple by selecting out
///        only the indices specified in a tuple of compile-time integer
///        constants.
///
/// For example, heterogeneous_filter<<0,2>, <4,5,6>> should return <4,6>.
///
/// @tparam Index The tuple of indices
/// @tparam Tuple The tuple of elements from which to select.
//////////////////////////////////////////////////////////////////////
template<typename Index, typename Tuple>
struct heterogeneous_filter;

template<std::size_t... IndexIndices, std::size_t... TupleIndices>
struct heterogeneous_filter<
  tuple<std::integral_constant<std::size_t, IndexIndices>...>,
  tuple<std::integral_constant<std::size_t, TupleIndices>...>>
{
  using as_tuple = tuple<std::integral_constant<std::size_t, TupleIndices>...>;

  using type = tuple<
    typename tuple_element<IndexIndices, as_tuple>::type...
  >;
};

} // namespace result_of


//////////////////////////////////////////////////////////////////////
/// @brief Filter a d-dimensional tuple by selecting out only the indices
///        specified in a tuple of compile-time integer constants. This
///        function provides the inverse behavior of @see discard
///
///        Note that this function currently only supports tuples
///        of homogeneous types.
///
/// @tparam Slices A tuple of std::integral_constants which specifies
///                which indices in the tuple to keep.
/// @param  t The tuple to filter
/// @return A tuple of size |Slices| where the rest of the elements are
///         filtered out
//////////////////////////////////////////////////////////////////////
template<typename Slices, typename Tuple>
typename result_of::filter<Slices, typename std::decay<Tuple>::type>::type
filter(Tuple&& t)
{
  using tuple_t  = typename std::decay<Tuple>::type;

  using result_t = typename result_of::filter<Slices, tuple_t>::type;

  using filter_t = detail::discard_impl<
                     0,
                     stapl::tuple_size<tuple_t>::type::value,
                     tuple_t, result_t, Slices, true>;

  return filter_t::apply(std::forward<Tuple>(t));
}

} // namespace tuple_ops
} // namespace stapl

#endif // STAPL_UTILITY_TUPLE_FILTER_HPP
