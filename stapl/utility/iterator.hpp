/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_UTILITY_ITERATOR_HPP
#define STAPL_UTILITY_ITERATOR_HPP

namespace stapl {
//////////////////////////////////////////////////////////////////////
/// @defgroup iteratorSpecializations Specializations for stand-alone begin.
/// @brief The specializations for stand-alone iterator operations such as
/// begin and end.
/// @{
/// @brief General implementation of stand-alone begin that redirects
/// it to type.begin().
//////////////////////////////////////////////////////////////////////
template <typename T>
auto begin(T&& t) -> decltype(t.begin())
{
  return t.begin();
}

//////////////////////////////////////////////////////////////////////
/// @brief General implementation of end that redirects it to type.end().
//////////////////////////////////////////////////////////////////////
template <typename T>
auto end(T&& t) -> decltype(t.end())
{
  return t.end();
}

/// @}

} // namespace stapl

#endif // STAPL_UTILITY_ITERATOR_HPP
