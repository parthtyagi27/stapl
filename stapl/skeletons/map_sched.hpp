/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_MAP_SCHED_HPP
#define STAPL_SKELETONS_MAP_SCHED_HPP

#include <stapl/utility/use_default.hpp>
#include <stapl/skeletons/functional/zip.hpp>
#include <stapl/skeletons/transformations/coarse/zip.hpp>
#include <stapl/skeletons/executors/execute.hpp>
#include <stapl/views/metadata/coarseners/default.hpp>
#include <stapl/views/metadata/coarseners/multiview.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @brief Construct and execute a PARAGRAPH that will perform a map
/// operation, applying the fine-grain work function to the element of
/// the views provided.  The scheduler provided will be used by the
/// executor processing the tasks of the PARAGRAPH as it is executed.
///
/// @param scheduler Scheduler to employ in the executor processing the
///                  PARAGRAPH.
/// @param map_op    Fine-grain map work function.
/// @param views     One or more views to process with the map work function.
///
/// @ingroup skeletonsExecutable
//////////////////////////////////////////////////////////////////////
template<typename Scheduler, typename MapOp, typename ...V>
inline void
map_func_sched(Scheduler const& scheduler, MapOp const& map_op,
               V&&... views)
{
  skeletons::execute(
    skeletons::execution_params(
      default_coarsener(), stapl::use_default(), scheduler),
    skeletons::coarse(skeletons::zip<sizeof...(V)>(map_op)),
    std::forward<V>(views)...);
}

} // namespace stapl

#endif // STAPL_SKELETONS_MAP_SCHED_HPP
