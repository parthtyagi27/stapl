/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_NEAREST_POW_TWO_HPP
#define STAPL_SKELETONS_SPANS_NEAREST_POW_TWO_HPP

#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/views/counting_view.hpp>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/front.hpp>

namespace stapl {
namespace skeletons {
namespace spans {

/////////////////////////////////////////////////////////////////////////
/// @brief In some skeletons the number of elements to be spawned is less
/// than the elements in the input size. One good example is in the reduction
/// skeleton where the size of the leaves of the tree is less than or
/// equal to the number of elements in the input.
///
/// Typically, a skeleton with this span comes after a skeleton with
/// @c reduce_to_pow_two. The main difference between the two is that in
/// this span all the elements are spawned (no should_spawn is defined).
/// The reason behind this is that the size of the span is important to
/// the elementary skeleton using it, and hence needs to be stored.
///
/// @tparam OnSpan the span on which this tree is defined
///
/// @ingroup skeletonsSpans
/////////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct nearest_pow_two
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Spawner, typename... Views>
  void set_size(Spawner const& spawner, Views const&... views)
  {
    // we need to do this just to make sure we get the correct size for
    // the next step if something like spans::per_location is used.
    OnSpan::set_size(spawner, views...);
    std::size_t n = OnSpan::size();
    std::size_t nearest_pow_2 = 1;
    while (n != 1)
    {
      n >>= 1;
      nearest_pow_2 <<= 1;
    }
    //now we adjust the size
    //for now this is the only place that we are forcing the changes
    OnSpan::template set_size<true>(
      spawner, stapl::counting_view<int>(nearest_pow_2));
  }
};

} // namespace spans
} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_SPANS_NEAREST_POW_TWO_HPP
