/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_SPANS_TREE_HPP
#define STAPL_SKELETONS_SPANS_TREE_HPP

#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/front.hpp>
#include <stapl/skeletons/utility/tags.hpp>

namespace stapl {
namespace skeletons {
namespace spans {

template <typename OnSpan, typename Alignment>
struct tree;

template <typename OnSpan, typename Alignment>
struct reverse_tree;

//////////////////////////////////////////////////////////////////////
/// @brief This is a span for a left-skewed binary tree that looks
/// like the following:
/// @code
/// O O O O O O O O
/// |/ _|/  |/  |/
/// | |  ___|   |
/// | | |  _____|
/// | | | |
/// O O O O
/// |/ _|/
/// | |
/// O O
/// |/
/// O
///
/// @endcode
///
/// @tparam OnSpan the span on which this tree is defined
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct tree<OnSpan, tags::left_skewed>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (tuple_ops::front(skeleton_size) == 1) ||
      (1ul << stapl::get<1>(skeleton_size) == tuple_ops::front(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t tree_depth = stapl::get<1>(skeleton_size);
    const std::size_t current_depth = stapl::get<1>(coord);

    return stapl::get<0>(coord) <
             (std::size_t(1) << (tree_depth - current_depth - 1));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A span for right-aligned binary tree that looks like the
/// following:
/// @code
/// O O O O O O O O
///  \|  \|  \|  \|
///   O__ O   O__ O
///      \|      \|
///       O______ O
///              \|
///               O
/// @endcode
///
/// @tparam OnSpan the span on which this right tree is defined
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct tree<OnSpan, tags::right_aligned>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (stapl::get<0>(skeleton_size) == 1) ||
      (1ul << stapl::get<1>(skeleton_size) == stapl::get<0>(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t current_depth = stapl::get<1>(coord);

    return (stapl::get<0>(skeleton_size) == 1) ||
           ((stapl::get<0>(coord) + 1) %
            (std::size_t(1) << (current_depth + 1)) == 0);
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A span for a left-aligned reverse binary tree that looks
/// like the following:
/// @code
/// O O O O O O O O
/// |/  |/  |/  |/
/// O __O   O __O
/// |/      |/
/// O ______O
/// |/
/// O
/// @endcode
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct tree<OnSpan, tags::left_aligned>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (stapl::get<0>(skeleton_size) == 1) ||
      (1ul << stapl::get<1>(skeleton_size) == stapl::get<0>(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t current_depth = stapl::get<1>(coord);

    return (stapl::get<0>(coord) %
             (std::size_t(1) << (current_depth + 1))) == 0;
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A span for a left-skewed reverse binary tree that looks
/// like the following:
/// @code
/// O
/// |\.
/// O O
/// | |_
/// |\  |\.
/// O O O O
/// | | | |_____
/// | | |___    |
/// | |_    |   |
/// |\  |\  |\  |\.
/// O O O O O O O O
/// @endcode
///
/// @tparam OnSpan the span on which this tree is defined
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct reverse_tree<OnSpan, tags::left_skewed>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (stapl::get<0>(skeleton_size) == 1) ||
        (1ul << stapl::get<1>(skeleton_size) == stapl::get<0>(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t current_depth = stapl::get<1>(coord);
    //remember that the first level of the tree is provided by the previous
    //skeleton that this one is connected to
    return stapl::get<0>(coord) < (1ul << (current_depth + 1));
  }
};


//////////////////////////////////////////////////////////////////////
/// @brief A span for a right-aligned reverse binary tree that looks
/// like the following:
/// @code
///               O
///        ______/|
///       O       O
///    __/|    __/|
///   O   O   O   O
///  /|  /|  /|  /|
/// O O O O O O O O
/// @endcode
///
/// @tparam OnSpan the span on which this tree is defined
///
/// @ingroup skeletonsSpans
//////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct reverse_tree<OnSpan, tags::right_aligned>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (stapl::get<0>(skeleton_size) == 1) ||
      (1ul << stapl::get<1>(skeleton_size) == stapl::get<0>(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t current_depth = stapl::get<1>(coord);
    const std::size_t depth = stapl::get<1>(skeleton_size);
    //remember that the first level of the tree is provided by the previous
    //skeleton that this one is connected to
    return (stapl::get<0>(skeleton_size) == 1) ||
           ((stapl::get<0>(coord) + 1) %
            (std::size_t(1) << (depth - current_depth - 1)) == 0);
  }
};


/////////////////////////////////////////////////////////////////////////
/// @brief A span for a left-aligned reverse binary tree that look like
/// the following:
/// @code
/// O
/// |\______
/// O       O
/// |\__    |\__
/// O   O   O   O
/// |\  |\  |\  |\.
/// O O O O O O O O
/// @endcode
///
/// @tparam OnSpan the span on which this tree is defined
///
/// @ingroup skeletonsSpans
/////////////////////////////////////////////////////////////////////////
template <typename OnSpan>
struct reverse_tree<OnSpan, tags::left_aligned>
  : public OnSpan
{
  using size_type      = typename OnSpan::size_type;
  using dimension_type = typename OnSpan::dimension_type;

  template <typename Coord>
  bool should_spawn (Coord const& skeleton_size, Coord const& coord) const
  {
    stapl_assert(
      (stapl::get<0>(skeleton_size) == 1) ||
      (1ul << stapl::get<1>(skeleton_size) == stapl::get<0>(skeleton_size)),
      "Skeleton size should be power of two");
    const std::size_t current_depth = stapl::get<1>(coord);
    const std::size_t depth = stapl::get<1>(skeleton_size);
    //remember that the first level of the tree is provided by the previous
    //skeleton that this one is connected to
    return (stapl::get<0>(coord) %
              (std::size_t(1) << (depth - current_depth - 1))) == 0;
  }
};

} // namespace spans
} // namespace skeletons
} // namepsace stapl

#endif // STAPL_SKELETONS_SPANS_TREE_HPP
