/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_PARAM_DEPS_PRE_BROADCAST_PD_HPP
#define STAPL_SKELETONS_PARAM_DEPS_PRE_BROADCAST_PD_HPP

#include <type_traits>
#include <stapl/utility/tuple/tuple.hpp>
#include <stapl/utility/tuple/front.hpp>
#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/operators/elem_helpers.hpp>
#include <stapl/skeletons/param_deps/utility.hpp>
#include <stapl/utility/integer_sequence.hpp>

namespace stapl {
namespace skeletons {
namespace skeletons_impl {

//////////////////////////////////////////////////////////////////////
/// @brief Creates a set_result parametric dependency given a @c op.
///        Basically this parametric dependency is only used for
///        sinking values of skeletons which couldn't currently
///        set their results directly without using sink_value skeleton.
///
/// @tparam Arity      the arity of set_result
/// @tparam  Op         the workfunction to be used in each set_result
///                    parametric dependency
/// @tparam Span       the span for the previous skeleton that is passed
///                    to set result for detecting if the corresponding task
///                    is spanned or not
/// @tparam SetResult  whether put the task result on the result container
///                    or not

/// @ingroup skeletonsParamDeps
//////////////////////////////////////////////////////////////////////
template <std::size_t Arity, typename Op, typename Span, bool SetResult>
class pre_broadcast_pd
{
  Op        m_op;
public:
  static constexpr std::size_t in_port_size = Arity;
  static constexpr std::size_t op_arity     = Arity;

  using op_type      = Op;

  explicit pre_broadcast_pd(Op op)
    : m_op(std::move(op))
  { }

private:
  template <typename Coord, typename Visitor, typename In,
            std::size_t... Indices>
  void apply_case_of(Coord const& skeleton_size, Visitor& visitor, In&& in_flow,
                     index_sequence<Indices...>&&) const
  {
    visitor.template operator()<SetResult>(
      m_op,
      no_mapper(),
      stapl::get<Indices>(in_flow).consume_from(
        make_tuple(skeleton_size - 1))...
    );
  }

public:
  //////////////////////////////////////////////////////////////////////
  /// @brief If coord is <idx, ...> it wraps the @c Op with the
  /// following inputs and sends it to the visitor along with the
  /// @c m_op
  /// @li in<0>[idx]
  /// @li in<1>[idx]
  /// @li ...
  ///
  /// @param coord        <i, j, k, ...> where i < n, j < m, k < p
  /// @param visitor      the information about Op and input is passed
  ///                     so that later this information can be converted
  ///                     to a node in the dependence graph
  /// @param in_flow      a tuple of input flows to consume from
  //////////////////////////////////////////////////////////////////////
  template <typename Coord, typename Visitor, typename In>
  void case_of(Coord const& skeleton_size, Coord const&/* coord*/,
               Visitor& visitor, In&& in_flow) const
  {
    apply_case_of(
      tuple_ops::front(skeleton_size),
      visitor, std::forward<In>(in_flow),
      stapl::make_index_sequence<Arity>());
  }


  //////////////////////////////////////////////////////////////////////
  /// @brief determines how many of the instances of this parametric
  /// dependency will be consuming from a producer with a given coordinate.
  /// This is a reverse query as compared to case_of
  ///
  /// @param  skeleton_size  the size of skeleton dimension
  /// @param  coord          the producer coordination
  /// @tparam FlowIndex      the flow index to which this request is sent
  //////////////////////////////////////////////////////////////////////
  template <typename Size, typename Coord, typename FlowIndex>
  std::size_t consumer_count(Size const&  skeleton_size,
                             Coord const& coord,
                             FlowIndex const& /*flow_idx*/) const
  {
    return tuple_ops::front(coord) == tuple_ops::front(skeleton_size) - 1 ? 1
                                                                          : 0;
  }

  template <typename Coord>
  int get_result_id(Coord const&, Coord const&) const
  {
    return -1;
  }

  Op get_op() const
  {
    return m_op;
  }

  template <typename Coord, typename SpanType>
  void configure(Coord&&, SpanType&&)
  { }

  void define_type(typer& t)
  {
    t.member(m_op);
  }
};

} // namespace skeletons_impl

//////////////////////////////////////////////////////////////////////
/// @brief Creates a set_result parametric dependency given a @c op.
///        Basically this parametric dependency is only used for
///        sinking values of skeletons which couldn't currently
///        set their results directly without using sink_value skeleton.
///
/// @tparam Arity      the arity of set_result
/// @tparam Span       the span for the previous skeleton that is passed
///                    to set result for detecting if the corresponding task
///                    is spanned or not
/// @tparam SetResult  whether put the task result on the result container
///                    or not
/// @param  op         the workfunction to be used in each set_result
///                    parametric dependency
///
/// @ingroup skeletonsParamDeps
//////////////////////////////////////////////////////////////////////
template <std::size_t Arity, typename Span, bool SetResult, typename Op>
skeletons_impl::pre_broadcast_pd<Arity, Op, Span, SetResult>
pre_broadcast_pd(Op const& op)
{
  return skeletons_impl::pre_broadcast_pd<Arity, Op, Span, SetResult>(op);
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_PARAM_DEPS_PRE_BROADCAST_PD_HPP
