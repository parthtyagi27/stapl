/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COARSE_HPP
#define STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COARSE_HPP

#include <type_traits>
#include <stapl/utility/utility.hpp>
#include <stapl/utility/use_default.hpp>
#include <stapl/skeletons/utility/tags.hpp>
#include <stapl/skeletons/transformations/transform.hpp>

namespace stapl {
namespace skeletons {

//////////////////////////////////////////////////////////////////////
/// @brief An indirect call to a specialized coarsening for a specific
/// skeleton.
///
/// When @c coarse is called over a skeleton, this method redirects the
/// call to related implementation for a coarsened execution of that
/// skeleton. This indirection can be used later on for automatic
/// coarsening.
///
/// @param skeleton     the skeleton to be coarsened
/// @param CoarseTag    a tag to specify the required specialization for
///                     coarsening
/// @param ExecutionTag a tag to specify the execution method used for
///                     the coarsened chunks
///
/// @return a coarsened version of the given skeleton
///
/// @see skeletonsTagsCoarse
/// @see skeletonsTagsExecution
///
/// @ingroup skeletonsTransformations
//////////////////////////////////////////////////////////////////////
template <typename CoarseTag    = stapl::use_default,
          typename ExecutionTag = tags::sequential_execution,
          typename S,
          typename =
            typename std::enable_if<
              is_skeleton<typename std::decay<S>::type>::value>::type>
auto
coarse(S&& skeleton)
STAPL_AUTO_RETURN((
  skeletons::transform<tags::coarse<CoarseTag, ExecutionTag>>(
    std::forward<S>(skeleton))
))

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_TRANSFORMATIONS_COARSE_COARSE_HPP
