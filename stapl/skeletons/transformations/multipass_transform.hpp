/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/skeletons/utility/tags.hpp>

namespace stapl {
namespace skeletons {
namespace transformations {

template <typename S, typename SkeletonTag, typename CoarseTag>
struct transform;

template<class S, typename SkeletonTag>
struct transform<S, SkeletonTag, tags::multipass_transform<>>
{
  static S const& call(S const& skeleton)
  {
    return skeleton;
  }
};

//////////////////////////////////////////////////////////////////////
/// @brief Apply a sequence of transformations on a skeleton identified
///        by the tags in the @see multipass_transform tag.
//////////////////////////////////////////////////////////////////////
template <typename S, typename SkeletonTag, typename Pass, typename... Passes>
struct transform<S, SkeletonTag, tags::multipass_transform<Pass, Passes...>>
{
private:
  using after_pass = decltype(skeletons::transform<Pass>(std::declval<S>()));
  using base_after = typename after_pass::base_type;

public:
  static auto call(S const& skeleton)
  STAPL_AUTO_RETURN((
    skeletons::transform<tags::multipass_transform<Passes...>, base_after>(
      skeletons::transform<Pass>(skeleton))
  ))
};

} // namespace transformations
} // namespace skeletons
} // namespace stapl
