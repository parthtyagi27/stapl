/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_SKELETONS_FUNCTIONAL_SINK_HPP
#define STAPL_SKELETONS_FUNCTIONAL_SINK_HPP

#include <stapl/skeletons/utility/utility.hpp>
#include <stapl/skeletons/flows/compose_flows.hpp>
#include <stapl/skeletons/operators/compose.hpp>
#include "copy.hpp"

namespace stapl {
namespace skeletons {
namespace result_of {

template <typename ValueType,
          typename SrcSkeleton, typename DestSkeleton>
using sink = decltype(
               skeletons::compose<flows::compose_flows::last_input_to_all>(
                 std::declval<SrcSkeleton>(),
                 std::declval<DestSkeleton>()));

} // namespace result_of

//////////////////////////////////////////////////////////////////////
/// @brief This sink skeleton assumes a default span for the created
/// skeleton
///
/// @tparam ValueType    the type of elements to be copied
/// @param  skeleton      the skeleton to read the input from
/// @param  dest_skeleton a customized sink skeleton. By default this
///                      is assumed to be a copy skeleton
/// @return a sink skeleton with a customized destination skeleton
///
/// @see copy
///
/// @ingroup skeletonsFunctional
//////////////////////////////////////////////////////////////////////
template <typename ValueType,
          typename Span = stapl::use_default,
          typename SrcSkeleton,
          typename DestSkeleton =
            decltype(skeletons::copy<ValueType>(skeleton_traits<Span>()))>
result_of::sink<ValueType, SrcSkeleton, DestSkeleton>
sink(SrcSkeleton&& skeleton,
     DestSkeleton&& dest_skeleton =
       skeletons::copy<ValueType>(skeleton_traits<Span>()))
{
  using namespace flows;
  return skeletons::compose<compose_flows::last_input_to_all>(
           std::forward<SrcSkeleton>(skeleton),
           std::forward<DestSkeleton>(dest_skeleton));
}

} // namespace skeletons
} // namespace stapl

#endif // STAPL_SKELETONS_FUNCTIONAL_SINK_HPP
