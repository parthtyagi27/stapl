/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

/*
 *         ---- The Unbalanced Tree Search (UTS) Benchmark ----
 *
 *  Copyright (c) 2010 See UTS_AUTHORS file for copyright holders
 *
 *  This file is part of the unbalanced tree search benchmark.  This
 *  project is licensed under the MIT Open Source license.  See the UTS_LICENSE
 *  file for copyright and licensing information.
 *
 *  UTS is a collaborative project between researchers at the University of
 *  Maryland, the University of North Carolina at Chapel Hill, and the Ohio
 *  State University.  See UTS_AUTHORS file for more information.
 *
 */

/*
 * Unbalanced Tree Search
 *
 * alfg.h
 *   Additive Lagged Fibonacci Generator
 *    - splittable pseudorandom number generator for UTS
 *
 */
#ifndef _ALFG_H
#define _ALFG_H

/*
 * ALFG parameters
 *   L choices:  17, 55, 159, 607, 1279
 *
 */
#define UTS_ALFG_L           55          /* lag distance */

/****************************/
/* state array index names: */
/****************************/
#define J_STATE_SIZE      0
#define J_L               1
#define J_SEED            2
#define J_K               3
#define J_CBIT            4
#define J_RUNUP           5
#define J_LP              6
#define J_KP             7
#define J_ZP             8
#define N_SCALARS        9
#define NODE0            N_SCALARS
#define REG0             (NODE0+l-1)

#define POS_MASK         0x7fffffff
#define HIGH_BIT         0x80000000

/**********************************/
/* random number generator state  */
/**********************************/
struct state_t {
  int state[2*UTS_ALFG_L-1+N_SCALARS];
};

#define RNG_state int

/***************************************/
/* random number generator operations  */
/***************************************/

void rng_init(RNG_state *state, int seed);
void rng_spawn(RNG_state *mystate, RNG_state *newstate, int spawnNumber);
int rng_rand(RNG_state *mystate);
int rng_nextrand(RNG_state *mystate);
char * rng_showstate(RNG_state *state, char *s);
int rng_showtype(char *strBuf, int ind);

#endif /*alfg.h */
