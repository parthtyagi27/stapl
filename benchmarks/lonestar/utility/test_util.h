/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <stapl/containers/graph/graph.hpp>

#ifndef TEST_UTIL_H
#define TEST_UTIL_H

void stapl_print(const char* s)
{
  stapl::do_once([s](void)->void
                 { std::cout << s << std::flush; });
}

void check_reset_error(bool& err) {
  if (err)
    stapl_print("Failed\n");
  else
    stapl_print("Passed\n");
  err = false;
}

double compute_stats(std::string name, std::vector<double>& data) {
  //The predicate can be different that m_pred here if we want
  //statistics on other fields of the counter; default will be on
  //time
  double avg, max,min,variance,stddev,confidenceinterval;
  size_t iterations = data.size();
  avg=0.0;
  min=max=data[0];
  for ( size_t i = 0; i < iterations; i++) {
    double d = data[i];
    avg += d;
    if ( d > max )
      max = d;
    if ( d < min )
      min = d;
  }
  avg /= iterations;
  variance=0.0;
  for ( size_t i = 0; i < iterations; i++) {
    double d = data[i] - avg;
    variance += d*d;
  }
  variance /= iterations;
  stddev = sqrt(variance);
  // 1.96 is z-table entry for 95%
  //The formula for min number of iteration is
  // n=[(1.96*stddev)/(useconfrange*avg)]^2
  //double nit = (1.96*stddev*1.96*stddev)/(0.05 * avg * 0.05 * avg);
  //std::cout<<"Min Number of iterations"<<nit<<"\n";
  confidenceinterval = (1.96*stddev) / sqrt((double) iterations);
  stapl::do_once([name, avg, min, max, confidenceinterval](void)->void
                 { std::cout << name << "=" << avg <<"\n";
                   std::cout << name << "_min=" << min <<"\n";
                   std::cout << name << "_max=" << max <<"\n";
                   std::cout << name << "_conf=" << confidenceinterval<<"\n\n";
                 });
  return avg;
}

#endif
