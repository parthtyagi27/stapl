/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"
#include "../timer.hpp"
#include <stapl/algorithms/algorithm.hpp>
#include <vector>
#include <iostream>

using container_t = stapl::array<data_t>;
using view_t      = stapl::array_view<container_t>;

// work function used to initialize element on a location with the location id
struct assign_id
{
private:
  data_t m_lid;

public:
  assign_id(unsigned int lid)
    : m_lid(lid)
  { }

  template<typename Ref>
  void operator()(Ref&& val) const
  { val = m_lid; }

  void define_type(stapl::typer& t)
  { t.member(m_lid); }
};


stapl::exit_code stapl_main(int argc, char *argv[])
{
  const int num_samples = argc == 2 ? atoi(argv[1]) : 10;

  std::vector<double> map_samples(num_samples,0.),
    map_reduce_samples(num_samples,0.), reduce_samples(num_samples, 0.),
    scan_samples(num_samples,0.), bfly_scalar_samples(num_samples,0.),
    bfly_vector_samples(num_samples,0.);

  bool map_correct(true), map_reduce_correct(true), scan_correct(true),
    reduce_correct(true), bfly_scalar_correct(true), bfly_vector_correct(true);

  counter_t timer;

  unsigned int nlocs = stapl::get_num_locations();
  unsigned int lid   = stapl::get_location_id();

  // sum of [0, nlocs-1]
  unsigned int expected_sum = (nlocs-1)*nlocs/2;

  // sum of exclusive scan of [0, nlocs-1]
  unsigned int expected_scan_sum = 0;
  for (unsigned int i = 0; i != nlocs; i++)
    expected_scan_sum += i * (nlocs - (i + 1));


  for (int sample = 0; sample != 10; ++sample)
  {
    // Container for algorithm basic tests
    stapl::array<data_t> bcont(nlocs);
    stapl::array_view<stapl::array<data_t>> b(bcont);

    // map_func test fills elements
    timer.reset();
    timer.start();

    stapl::map_func(assign_id(lid), b);

    map_samples[sample] = timer.stop();

    // reduce call checks sum
    timer.reset();
    timer.start();

    data_t red_result = stapl::reduce(b, stapl::plus<data_t>());

    reduce_samples[sample] = timer.stop();

    // scan is the commonly used exclusive scan
    timer.reset();
    timer.start();

    stapl::scan(b, b, stapl::plus<data_t>(), true);

    scan_samples[sample] = timer.stop();

    // map_reduce checks the sum after scan
    timer.reset();
    timer.start();

    data_t mapred_result =
      stapl::map_reduce(stapl::identity<data_t>(), stapl::plus<data_t>(), b);

    map_reduce_samples[sample] = timer.stop();

    map_correct = map_correct && red_result == expected_sum;

    reduce_correct = reduce_correct && red_result == expected_sum;

    map_reduce_correct = map_reduce_correct &&
      mapred_result == expected_scan_sum;

    scan_correct = scan_correct &&
      mapred_result == expected_scan_sum;

    stapl::rmi_fence();
  }

  report_result("b_map_func","STAPL",map_correct, map_samples);
  report_result("b_map_reduce","STAPL",map_reduce_correct, map_reduce_samples);
  report_result("b_reduce","STAPL",reduce_correct, reduce_samples);
  report_result("b_scan","STAPL",scan_correct, scan_samples);

  return EXIT_SUCCESS;
}
