/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"
#include "../timer.hpp"

#include <vector>
#include <iostream>

int main(int argc, char *argv[])
{
  size_t size = atoll(argv[1]);
  int num_threads = atoi( argv[2] );

  // Force the use of parallel STL versions of the algorithms 
  __gnu_parallel::_Settings s;
  s.algorithm_strategy = __gnu_parallel::force_parallel;
  __gnu_parallel::_Settings::set(s);

  omp_set_num_threads(num_threads);

  std::vector<double> acc_samples(10,0.);
  std::vector<double> ad_samples(10,0.);
  std::vector<double> ip_samples(10,0.);
  std::vector<double> ps_samples(10,0.);

  data_t acc_result(0.), ip_result(0.), ad_result(0.);
  bool   acc_correct(true), ip_correct(true), ad_correct(true);

  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    std::vector<data_t> y(size);

    //Init
    fill_random(x);
    fill_random(y);


    auto time = start_timer();

    //Apply accumulate
    auto acc_result = __gnu_parallel::accumulate(x.begin(), x.end(), 0.);

    acc_samples[sample] = stop_timer(time);

    acc_correct =
      acc_correct && check_numeric_result(acc_result, x.size(), 0.5);


    time = start_timer();

    auto ip_result =
      __gnu_parallel::inner_product(x.begin(), x.end(), y.begin(), 0.);

    ip_samples[sample] = stop_timer(time);

    ip_correct =
      ip_correct && check_numeric_result(ip_result, x.size(), 0.3333);


    time = start_timer();

    __gnu_parallel::partial_sum(x.begin(), x.end(), y.begin());

    ps_samples[sample] = stop_timer(time);
    time = start_timer();

    __gnu_parallel::adjacent_difference(y.begin(), y.end(), x.begin());

    ad_samples[sample] = stop_timer(time);

    auto ad_result = __gnu_parallel::accumulate(x.begin(), x.end(), 0.);

    ad_correct = ad_correct && check_numeric_result(ad_result, x.size(), 0.5);

  }

  report_result("b_accumulate","gnu_parallel",acc_correct, acc_samples);
  report_result("b_inner_product","gnu_parallel",ip_correct, ip_samples);

  // partial sum and adjacent different share a correctness check because
  // the operations counter the side effects of one another and restore the
  // container to the initial set of values
  report_result("b_partial_sum","gnu_parallel",ad_correct, ps_samples);
  report_result("b_adjacent_difference","gnu_parallel",ad_correct, ad_samples);

  return 0;
}
