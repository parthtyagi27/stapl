/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include "../utilities.hpp"
#include "../timer.hpp"

#include <vector>
#include <iostream>
#include <sstream>
#include <functional>

int main(int argc, char *argv[])
{
  size_t size = 13;
  int num_threads = 1;
  double duplicate_ratio = 0.001;

  if (argc > 3)
  {
    size = atoll(argv[1]);
    num_threads = atoi( argv[2] );
    duplicate_ratio = atof(argv[3]);
  }
  else if (argc > 2)
  {
    size = atoll(argv[1]);
    num_threads = atoi( argv[2] );
  }
  else if (argc > 1)
    size = atoll(argv[1]);

  // Force the use of parallel STL versions of the algorithms 
  __gnu_parallel::_Settings s;
  s.algorithm_strategy = __gnu_parallel::force_parallel;
  __gnu_parallel::_Settings::set(s);
  
  omp_set_num_threads(num_threads);

  std::vector<double> gen_samples(10,0.), gen_n_samples(10,0.),
    shu_samples(10,0.), rep_samples(10,0.), rif_samples(10,0.),
    tra_samples(10,0.), mer_samples(10,0.), uni_samples(10,0.);

  bool gen_correct(true), gen_n_correct(true), shu_correct(true),
    rep_correct(true), rif_correct(true), tra_correct(true),
    mer_correct(true), uni_correct(true);

  // GENERATE
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);

    auto time = start_timer();

     __gnu_parallel::generate(x.begin(), x.end(), []{ return 81372.0; });

    gen_samples[sample] = stop_timer(time);

    gen_correct  = gen_correct && (x[std::rand()%size] == 81372.0);
  }
  report_result("b_generate","gnu_parallel",gen_correct, gen_samples);

  // GENERATE N
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);

    auto time = start_timer();

     __gnu_parallel::generate_n(x.begin(), size, []{ return 81372.0; });

    gen_n_samples[sample] = stop_timer(time);

    gen_n_correct  = gen_correct && (x[std::rand()%size] == 81372.0);
  }
  report_result("b_generate_n","gnu_parallel",gen_n_correct, gen_n_samples);

  // REPLACE IF and REPLACE
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    fill_random(x);

    auto time = start_timer();

    // Replace if less than 0.25
    __gnu_parallel::replace_if(x.begin(), x.end(),
      std::bind(std::less<double>(),std::placeholders::_1, 0.25), 0.5,
                               __gnu_parallel::parallel_balanced);

    rif_samples[sample] = stop_timer(time);

    rif_correct = rif_correct &&
      (std::find_if(x.begin(), x.end(), std::bind(std::less<double>(),
        std::placeholders::_1, 0.25)) == x.end());

    time = start_timer();

    // Replace 0 with 0.125
    __gnu_parallel::replace(x.begin(), x.end(), 0.5, 0.125,
                            __gnu_parallel::parallel_balanced);

    rep_samples[sample] = stop_timer(time);

    auto result = std::accumulate(x.begin(), x.end(), 0.);

    rep_correct = rep_correct && result/size > 0.49 && result/size < 0.51;
  }
  report_result("b_replace","gnu_parallel", rep_correct, rep_samples);
  report_result("b_replace_if","gnu_parallel", rif_correct, rif_samples);

  // TRANSFORM
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x(size);
    fill_random(x);
    std::vector<data_t> original(x);

    auto time = start_timer();

    __gnu_parallel::transform(x.begin(), x.end(),  x.begin(),
      [](double i) { return i+1;}); // Increase by 1

    tra_samples[sample] = stop_timer(time);

    tra_correct = tra_correct && std::equal(x.begin(), x.end(),
      original.begin(), [](double x, double y) {return x == (y+1);});
  }
  report_result("b_transform","gnu_parallel", tra_correct, tra_samples);

  // MERGE
  for (int sample = 0; sample != 10; ++sample)
  {
    std::vector<data_t> x1(size);
    fill_random(x1);
    std::vector<data_t> x2(size);
    fill_random(x2);
    std::vector<data_t> x3(size*2);

    // merge requires sorted inputs
    std::sort(x1.begin(), x1.end());
    std::sort(x2.begin(), x2.end());

    double pre_merge =  std::accumulate(x1.begin(), x1.end(), 0.0);
    pre_merge += std::accumulate(x2.begin(), x2.end(), 0.0);

    auto time = start_timer();

    __gnu_parallel::merge(x1.begin(), x1.end(), x2.begin(), x2.end(),
      x3.begin());

    mer_samples[sample] = stop_timer(time);

    double post_merge = std::accumulate(x3.begin(), x3.end(), 0.0);

    if ((post_merge * 1.05) <= pre_merge || (post_merge * 0.95) >= pre_merge )
      mer_correct = false;
  }
  report_result("b_merge","gnu_parallel",mer_correct, mer_samples);

  return 0;
}
