/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STAPL_PROXY_ARRAY_H
#define STAPL_PROXY_ARRAY_H

#include <stapl/views/proxy/proxy.hpp>
#include <stapl/views/iterator/member_iterator.h>
#include <stapl/views/proxy_macros.hpp>

namespace stapl {

//////////////////////////////////////////////////////////////////////
/// @todo Make reflected const_reference type different from reference
///   when const_member_accessor and associated helper macros exist.
//////////////////////////////////////////////////////////////////////
template <typename T, std::size_t N, typename Accessor>
class proxy<std::array<T, N>, Accessor>
  : public Accessor
{
  typedef std::array<T, N> base_target_t;

  STAPL_PROXY_DEFINES(base_target_t)

public:
  STAPL_PROXY_REFLECT_TYPE(size_type)
  STAPL_PROXY_METHOD_RETURN(size, size_type)
  STAPL_PROXY_METHOD_RETURN(max_size, size_type)
  STAPL_PROXY_METHOD_RETURN(empty, bool)

  STAPL_PROXY_REFERENCE_METHOD_0(front, T)
  STAPL_PROXY_REFERENCE_METHOD_0(back, T)
  STAPL_PROXY_REFERENCE_METHOD_1(inner, operator[], T, std::size_t)
  STAPL_PROXY_REFERENCE_METHOD_1(inner2, at, T, std::size_t)

private:
  typedef typename target_t::iterator                  iter_t;
  typedef typename target_t::const_iterator            const_iter_t;

  typedef typename target_t::reverse_iterator          r_iter_t;
  typedef typename target_t::const_reverse_iterator    const_r_iter_t;

public:
  typedef member_iterator<iter_t, Accessor>            iterator;
  typedef member_iterator<const_iter_t, Accessor>      const_iterator;

  typedef member_iterator<r_iter_t, Accessor>          r_iterator;
  typedef member_iterator<const_r_iter_t, Accessor>    const_r_iterator;

  typedef inner_reference                              reference;
  typedef inner_reference                              const_reference;

  iterator begin()
  {
    return iterator(Accessor::invoke(&target_t::begin), *this);
  }

  iterator end()
  {
    return iterator(Accessor::invoke(&target_t::end), *this);
  }

  r_iterator rbegin()
  {
    return r_iterator(Accessor::invoke(&target_t::rbegin), *this);
  }

  r_iterator rend()
  {
    return r_iterator(Accessor::invoke(&target_t::rend), *this);
  }

  const_iterator cbegin() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::cbegin), *this);
  }

  const_iterator cend() const
  {
    return const_iterator(Accessor::const_invoke(&target_t::cend), *this);
  }

  const_r_iterator crbegin() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::crbegin), *this);
  }

  const_r_iterator crend() const
  {
    return const_r_iterator(Accessor::const_invoke(&target_t::crend), *this);
  }
}; // struct proxy

} // namespace stapl

#endif

