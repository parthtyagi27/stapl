/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef ITER_IJ_TEST_H
#define ITER_IJ_TEST_H

#include "mtl/mtl_config.h"

template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			       rectangle_tag, row_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T(0);
  for (Int i = 0; i < A.nrows(); ++i)
    for (Int j = 0; j < A.ncols(); ++j) {
      c = c + T(1);
      if (A(i,j) != c || A[i][j] != c) {
	std::cerr << "**** FAILED: (iterator operator_ij) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << i << "][" << j << "] = " << A[i][j] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
  std::cout << test_name.c_str() << " passed iterator operator_ij" << std::endl;
  return true;
}


template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			       rectangle_tag, column_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T(0);
  for (Int j = 0; j < A.ncols(); ++j)
    for (Int i = 0; i < A.nrows(); ++i) {
      c = c + T(1);
      if (A(i,j) != c || A[j][i] != c) {
	std::cerr << "**** FAILED: (iterator operator_ij rect column) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << j << "][" << i << "] = " << A[j][i] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
 std::cout << test_name.c_str() << " passed iterator operator_ij (rect col)" << std::endl;
  return true;
}

template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			   banded_tag, row_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  Int i, j;
  int lo = A.sub();
  int up = A.super();
  T c = T(0);
  for (i = 0; i < A.nrows(); ++i) {
    Int first = MTL_MAX(0, int(i) - lo);
    Int last = MTL_MIN(int(A.ncols()), int(i) + up + 1);
    for (j = 0; j < A.ncols(); ++j) {
      if (j >= first && j < last) {
	c = c + T(1);
	if (A(i,j) != c || A[i][j] != c) {
		std::cerr << "**** FAILED: ( iterator_operator(i,j) banded row) "
	       << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	  std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	  std::cerr << "A[" << i << "][" << j << "] = " << A[i][j] << std::endl;
	  std::cerr << "c = " << c << std::endl;
#endif
	  return false;
	}
      }
    }
  }
  std::cout << test_name.c_str() << " passed iterator operator_ij banded row" << std::endl;
  return true;
}


template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			   banded_tag, column_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  Int i, j;
  int lo = A.sub();
  int up = A.super();
  T c = T(0);
  for (j = 0; j < A.ncols(); ++j) {
    Int first = MTL_MAX(0, int(j) - up);
    Int last = MTL_MIN(int(A.nrows()), int(j) + lo + 1);
    for (i = 0; i < A.nrows(); ++i) {
      if (i >= first && i < last) {
	c = c + T(1);
	if (A(i,j) != c || A[j][i] != c) {
	  std::cerr << "**** FAILED: ( iterator_operator(i,j) banded column) "
	       << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	  std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	  std::cerr << "A[" << j << "][" << i << "] = " << A[j][i] << std::endl;
	  std::cerr << "c = " << c << std::endl;
#endif
	  return false;
	}
      }
    }
  }
 std::cout << test_name.c_str() << " passed iterator operator_ij banded column" << std::endl;
  return true;
}


template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			   symmetric_tag, row_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  Int i, j;
  int lo = A.sub();
  int up = A.super();
  for (i = 0; i < A.nrows(); ++i) {
    Int first = MTL_MAX(0, int(i) - lo);
    Int last = MTL_MIN(int(A.ncols()), int(i) + up + 1);
    for (j = 0; j < A.ncols(); ++j) {
      if (j >= first && j < last) {
	if (A(i,j) != T(i + j)) {
	  // JGS, can't do this test because the indices
	  // can not be swapped using the [][] operators
	  // so they realy should not be used with symmetric
	  // matrices:
	  //	  A[j][i] != (i + j)
	  std::cerr << "**** FAILED: ( iterator_operator(i,j) symmetric row) "
	       << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	  std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	  std::cerr << "correct = " << i + j << std::endl;
	  mtl::print_all_banded(A, lo, up);
#endif
	  return false;
	}
      }
    }
  }
 std::cout << test_name.c_str() << " passed iterator operator_ij symmetric row" << std::endl;
  return true;
}


template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			   symmetric_tag, column_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  Int i, j;
  int lo = A.sub();
  int up = A.super();
  for (j = 0; j < A.ncols(); ++j) {
    Int first = MTL_MAX(0, int(j) - up);
    Int last = MTL_MIN(int(A.nrows()), int(j) + lo + 1);
    for (i = 0; i < A.nrows(); ++i) {
      if (i >= first && i < last)
	if (A(i,j) != T(i + j)) {
	  // JGS, can't do this test because the indices
	  // can not be swapped using the [][] operators
	  // so they realy should not be used with symmetric
	  // matrices:
	  //	  A[j][i] != (i + j)
	  std::cerr << "**** FAILED: ( iterator_operator(i,j) symmetric column) "
	       << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	  std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	  std::cerr << "correct = " << i + j << std::endl;
#endif
	  return false;
	}
    }
  }
 std::cout << test_name.c_str() << " passed iterator operator_ij symmetric column" << std::endl;
  return true;
}


template <class Matrix>
bool iterator_operator_ij_test(const Matrix& , std::string test_name, 
			   diagonal_tag, row_tag)
{
 std::cout << test_name.c_str() << " skipping iterator operator_ij" << std::endl;
  return true;
}

template <class Matrix>
bool iterator_operator_ij_test(const Matrix& , std::string test_name, 
			   diagonal_tag, column_tag)
{
 std::cout << test_name.c_str() << " skipping iterator operator_ij" << std::endl;
  return true;
}


#if 0
template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name,
			       sparse_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::orientation Orien;
  //  iterator_fill(A);
  return iterator_operator_ij_test_sparse(A, test_name, Orien());
}
#endif

template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name)
{
  typedef typename mtl::matrix_traits<Matrix>::shape Shape;
  typedef typename mtl::matrix_traits<Matrix>::orientation Orien;
  return iterator_operator_ij_test(A, test_name, Shape(), Orien());
}

#if 0
template <class Matrix>
bool iterator_operator_ij_test(const Matrix& A, std::string test_name)
{
  typedef typename mtl::matrix_traits<Matrix>::sparsity Sparsity;
  return iterator_operator_ij_test(A, test_name, Sparsity());
}
#endif
 

template <class Matrix>
bool strided_iterator_operator_ij_test(const Matrix& A, std::string test_name, 
			       rectangle_tag, row_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T();
  for (Int j = 0; j < A.ncols(); ++j)
    for (Int i = 0; i < A.nrows(); ++i) {
      c = c + T(1);
      if (A(i,j) != c || A[i][j] != c) {
	std::cerr << "**** FAILED: (strided iterator operator_ij rect row) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << i << "," << j << "] = " << A[i][j] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
 std::cout << test_name.c_str() << " passed strided iterator operator_ij rect row" << std::endl;
  return true;
}

template <class Matrix>
bool strided_iterator_operator_ij_test_sparse(const Matrix& A, 
					      std::string test_name, 
					      row_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T();
  for (Int j = 0; j < A.ncols(); ++j)
    for (Int i = 0; i < A.nrows(); ++i) {
      c = c + T(1);
      if (A(i,j) != c || A[i][j] != c) {
	std::cerr << "**** FAILED: (strided iterator operator_ij sparse row) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << i << "," << j << "] = " << A[i][j] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
 std::cout << test_name.c_str() << " passed strided iterator operator_ij sparse column" << std::endl;
  return true;
}

template <class Matrix>
bool strided_iterator_operator_ij_test_sparse(const Matrix& A,
					      std::string test_name, 
					      column_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T();
  for (Int i = 0; i < A.nrows(); ++i)
    for (Int j = 0; j < A.ncols(); ++j) {
      c = c + T(1);
      if (A(i,j) != c || A[j][i] != c) {
	std::cerr << "**** FAILED: (strided iterator operator_ij sparse column) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << j << "," << i << "] = " << A[j][i] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
 std::cout << test_name.c_str() << " passed strided iterator operator_ij sparse column" << std::endl;
  return true;
}


template <class Matrix>
bool strided_iterator_operator_ij_test(const Matrix& A, std::string test_name, 
				       rectangle_tag, column_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::size_type Int;
  typedef typename mtl::matrix_traits<Matrix>::value_type T;
  T c = T();
  for (Int i = 0; i < A.nrows(); ++i)
    for (Int j = 0; j < A.ncols(); ++j) {
      c = c + T(1);
      if (A(i,j) != c || A[j][i] != c) {
	std::cerr << "**** FAILED: (strided iterator operator_ij rect column) "
	     << test_name.c_str() << " ****" << std::endl;
#if !defined(_MSVCPP_)
	std::cerr << "A(" << i << "," << j << ") = " << A(i,j) << std::endl;
	std::cerr << "A[" << j << "," << i << "] = " << A[j][i] << std::endl;
	std::cerr << "c = " << c << std::endl;
#endif
	return false;
      }
    }
 std::cout << test_name.c_str() << " passed strided iterator operator_ij (rect col)" << std::endl;
  return true;
}

template <class Matrix>
bool strided_iterator_operator_ij_test(const Matrix& A, std::string test_name,
				       sparse_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::orientation Orien;
  return strided_iterator_operator_ij_test_sparse(A, test_name, Orien());
}

template <class Matrix>
bool strided_iterator_operator_ij_test(const Matrix& A, std::string test_name,
				       dense_tag)
{
  typedef typename mtl::matrix_traits<Matrix>::shape Shape;
  typedef typename mtl::matrix_traits<Matrix>::orientation Orien;
  return strided_iterator_operator_ij_test(A, test_name, Shape(), Orien());
}

template <class Matrix>
bool strided_iterator_operator_ij_test(const Matrix& A, std::string test_name)
{
  typedef typename mtl::matrix_traits<Matrix>::sparsity Sparsity;
  return strided_iterator_operator_ij_test(A, test_name, Sparsity());
}


#endif
