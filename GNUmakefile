# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

##############################################################################
# See GNUmakefile.STAPL_README
###############################################################################

ifndef STAPL
  export STAPL=$(PWD)
endif

include GNUmakefile.STAPLdefaults

.PHONY: base compile test clean
default: base

all: base test

BASEDIRS:= src

#
# base builds precompiled version of stapl distro
#
base:
	$(MAKE) -C src

SUBDIRS:= examples \
          docs/tutorial_guide \
          test \
          benchmarks

SUBDIRS_SERIAL:=test

TESTDIRS:=$(addsuffix .test, $(SUBDIRS))

.PHONY: $(SUBDIRS) $(TESTDIRS)

#
# Compile, then run, all tests
#

.PHONY: test runtests

test: export RUNNING_VALIDATION=1
test: base compile
	$(MAKE) -l 0.0 runtests

runtests: $(TESTDIRS)

#Force sequential test execution
$(TESTDIRS): %.test: base compile
	$(MAKE) -C $* test

#
# Compile all tests, but don't run
#

COMPILEDIRS:=$(addsuffix .compile, $(SUBDIRS))
COMPILEDIRS_SERIAL:=$(addsuffix .compile_serial, $(SUBDIRS_SERIAL))

.PHONY: compile compile_serial $(COMPILEDIRS) $(COMPILEDIRS_SERIAL)

compile: base $(COMPILEDIRS)
	$(MAKE) -l 0.0 compile_serial

$(COMPILEDIRS): base
$(COMPILEDIRS): %.compile:
	$(MAKE) -C $* compile

#Special target for memory-heavy compilations, so they are serialized
compile_serial: $(COMPILEDIRS_SERIAL)

$(COMPILEDIRS_SERIAL): base
$(COMPILEDIRS_SERIAL): %.compile_serial:
	$(MAKE) -C $* compile_serial

#
# clean related rules
#
CLEANDIRS = $(BASEDIRS) $(SUBDIRS)
SUBCLEAN  = $(addsuffix .clean,$(CLEANDIRS))
.PHONY: clean $(SUBCLEAN)

clean: $(SUBCLEAN)
	@rm -f $(THISBUILD)  # forget current platform/rts/stl

mostlyclean: $(SUBCLEAN)

$(SUBCLEAN): %.clean:
	$(MAKE) -C $* clean


showconf:
	@echo "platform        = $(platform)"
	@echo "stl             = $(stl)"
	@echo "CC              = $(CC)"
	@echo "ld              = $(LD)"
	@echo "ar              = $(AR)"
	@echo "USER_CXXFLAGS   = $(USER_CXXFLAGS)"
	@echo "STAPL_CXXFLAGS  = $(STAPL_CXXFLAGS)"
	@echo "AUTOFLAGS       = $(AUTOFLAGS)"
	@echo "CXXFLAGS        = $(CXXFLAGS)"
	@echo "USER_LIB        = $(USER_LIB)"
	@echo "STAPL_LIBRARIES = $(STAPL_LIBRARIES)"
	@echo "AUTOLIB         = $(AUTOLIB)"
	@echo "LIB             = $(LIB)"
	@echo "LIB_EPILOGUE    = $(LIB_EPILOGUE)"
	@echo "staplrun        = $(staplrun)"
	@echo "ALL_FLAGS       = $(STAPL_CXXFLAGS) $(CXXFLAGS)"
	@echo "ALL_LIBS        = $(STAPL_LIBRARIES) $(LIB) $(LIB_EPILOGUE)"
