/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/

#include <stapl/utility/hash_fwd.hpp>
#include <stapl/runtime/exception.hpp>
#include <stapl/runtime/p_object_registry.hpp>
#include <mutex>
#include <typeindex>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>


namespace stapl {

namespace runtime {

namespace {

//////////////////////////////////////////////////////////////////////
/// @brief Typed @ref p_object registry entry.
///
/// @ingroup runtimeMetadata
//////////////////////////////////////////////////////////////////////
struct registry_entry
{
  const rmi_handle* handle;
  const void*       address;
  std::type_index   type;

  registry_entry(const rmi_handle* h,
                 const void* p,
                 std::type_info const& t) noexcept
  : handle{h},
    address{p},
    type{t}
  { }
};


/// Typed @ref p_object registry.
using registry_type =
  boost::multi_index::multi_index_container<
    registry_entry,
    boost::multi_index::indexed_by<
      // index by {object address, object type}
      boost::multi_index::hashed_unique<
        boost::multi_index::composite_key<
          registry_entry,
          BOOST_MULTI_INDEX_MEMBER(registry_entry, const void*, address),
          BOOST_MULTI_INDEX_MEMBER(registry_entry, std::type_index, type)>>,
      // index by rmi_handle*
      boost::multi_index::hashed_unique<
        boost::multi_index::tag<rmi_handle>,
        BOOST_MULTI_INDEX_MEMBER(registry_entry, const rmi_handle*, handle)>>>;

registry_type typed_object_registry;
std::mutex    typed_object_registry_mtx;

} // namespace


// Registers object p with typeid t
void p_object_registry::register_object(const rmi_handle* h,
                                        const void* p,
                                        std::type_info const& t)
{
  std::lock_guard<std::mutex> lock{typed_object_registry_mtx};
  if (typed_object_registry.count(boost::make_tuple(p, std::type_index{t}))!=0)
    STAPL_RUNTIME_ERROR("p_object is already registered.");
  if (!typed_object_registry.emplace(h, p, t).second)
    STAPL_RUNTIME_ERROR("p_object failed to be registered.");
}


// Unregisters object o
void p_object_registry::unregister_object(const rmi_handle* h)
{
  std::lock_guard<std::mutex> lock{typed_object_registry_mtx};
  if (typed_object_registry.get<rmi_handle>().erase(h)!=1)
    STAPL_RUNTIME_ERROR("p_object was not registered.");
}


// Verifies that object o can be cast to type t
void p_object_registry::verify_object_type(const void* p,
                                           std::type_info const& t)
{
  if (!p)
    return;
  std::lock_guard<std::mutex> lock{typed_object_registry_mtx};
  if (typed_object_registry.count(boost::make_tuple(p, std::type_index{t}))!=1)
    STAPL_RUNTIME_ERROR("p_object is not registered with the given type.");
}

} // namespace runtime

} // namespace stapl

