/*
// Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
// component of the Texas A&M University System.

// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
*/


#include <stapl/runtime/location_md.hpp>
#include <stapl/runtime/executor/gang_executor.hpp>
#include <stapl/runtime/executor/scheduler/sched.hpp>
#include <stapl/runtime/type_traits/aligned_storage.hpp>

namespace stapl {

namespace runtime {

// Total required size in bytes for a location_md object and its runqueue::impl
const auto sizeof_location_md =
  aligned_size(sizeof(location_md), runqueue::required_alignment());


// Returns a properly aligned pointer to construct a runqueue::impl
void* location_md::get_ptr(void) noexcept
{
  return (reinterpret_cast<char*>(this) + sizeof_location_md);
}


// Allocates a new gang_executor for the location_md
std::unique_ptr<executor_base> location_md::make_executor(void)
{
  using executor_type = gang_executor<default_gang_scheduler>;
  return std::unique_ptr<executor_base>{new executor_type(*this)};
}


// Allocates space for a location_md object and its runqueue::impl
void* location_md::operator new(std::size_t)
{
  return ::operator new(sizeof_location_md + runqueue::required_size());
}


// Frees the space allocated for a location_md object and its runqueue::impl
void location_md::operator delete(void* p)
{
  ::operator delete(p);
}

} // namespace runtime

} // namespace stapl
