# Copyright (c) 2000-2017, Texas Engineering Experiment Station (TEES), a
# component of the Texas A&M University System.
#
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

##############################################################################
# This is designed for GNU make (gmake), and probably won't work
# with other make implementations because of its use of conditional if
# statements to set the appropriate flags for a given platform.
#
# To correctly compile the runtime system for different platforms, this
# makefile requires arguments specifying the desired system and compiler.  An
# empty call (e.g., '> gmake') will display the supported platforms.
#
# VARIABLE DICTIONARY (included from GNUmakefile.STAPLdefaults):
# platform        - the desired target platform (e.g., LINUX_gcc)
# stl             - the underlying ISO C++ STL to build upon
# STAPL           - the path to stapl (should be overloaded if this Makefile is
#                   is included by another Makefile in another directory)
# CC              - the compiler
# LD              - the linker and necessary flags to link
# AR              - the archiver and necessary flags to create an archive
# CXXFLAGS        - compiler and platform flags
# LIB             - compiler and platform libraries
# STAPL_CXXFLAGS  - STAPL required flags
# STAPL_LIBRARIES - STAPL required libraries
# USER_CXXFLAGS   - optimization levels and other user flags
# USER_LIB        - user libraries
# staplrun        - function to execute stapl, where $(1) is the number of
#                   locations
###############################################################################
